﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Resources;
using System.Threading;

namespace SelfUpdate
{
    class Program
    {
        private const string zipFilePath = @".\su.sp";
        private const string backupZipFilePath = @".\更新前备份.zip";
        static void Main(string[] args)
        {
            Console.WriteLine("大家别慌！你是不是看到这个很陌生？！");
            Console.WriteLine("其实我是来更新UP主助手程序的，请不要关闭我！\n\n");
            Console.WriteLine("(=・ω・=) 正在更新文件...");
            killProess();
            Thread wth = new Thread(wfile);
            wth.Start();
            Console.Read();
        }
        /// <summary>
		/// 关闭原有的应用程序 
		/// </summary>
		static void killProess()
        {
            Console.WriteLine("正在关闭程序....");
            System.Diagnostics.Process[] proc = System.Diagnostics.Process.GetProcessesByName("UPassistant");
            //关闭原有应用程序的所有进程
            foreach (System.Diagnostics.Process pro in proc)
            {
                pro.Kill();
            }
            Thread.Sleep(500);
        }
        static void wfile()
        {
 
                //Byte[] netiosf = (Byte[])resource.GetObject("Netios");
                //Byte[] upsf = (Byte[])resource.GetObject("Update");
                //System.IO.File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "netios.dll", netiosf);
                //System.IO.File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "Update.exe", upsf);
                try
                {
                if (File.Exists(backupZipFilePath))
                {
                    File.Delete(backupZipFilePath);
                }
                    ZipFile.CreateFromDirectory(".", backupZipFilePath);
                }
                catch (Exception)
                {

                }
            try
            {
                using (FileStream zipFileToOpen = new FileStream(zipFilePath, FileMode.Open))
                using (ZipArchive archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Read))
                {
                    Console.WriteLine("即将更新以下文件：");
                    foreach (var zipArchiveEntry in archive.Entries)
                    {
                        Console.WriteLine(zipArchiveEntry.FullName);
                        if (zipArchiveEntry.FullName.IndexOf("/")>0)
                        {
                            if (Directory.Exists(zipArchiveEntry.FullName))
                            {
                                Directory.Delete(zipArchiveEntry.FullName, true);
                            }
                        }
                        else
                        {
                            if (File.Exists(zipArchiveEntry.FullName))
                            {
                                File.Delete(zipArchiveEntry.FullName);
                            }
                        }
                     
                    }
                }
                ZipFile.ExtractToDirectory(zipFilePath, "./");
                
                System.IO.File.Delete("SelfUpdate.fff");
            }
            catch (Exception e)
            {
                Console.Write("错误："+e.Message);
                Console.WriteLine("\n");
                Console.WriteLine("OMG!更新遇到问题了！你可以在软件的目录里面找到备份文件！解压覆盖即可还原哦！\n");
                Console.WriteLine("按任意键退出！");
                Console.Read();
            }

            
            Console.WriteLine("更新完毕");
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo { FileName = AppDomain.CurrentDomain.BaseDirectory+ "upassistant.exe" };
            p.Start();
            Environment.Exit(0);
        }
    }
}
