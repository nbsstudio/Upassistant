﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Netios
{
    public class UpdateClass
    {
        public string OldEdition = "1001";
        public string NewEdition = "1002";
        private const string NowUpdateProgram = "Update.exe";
        public UpdateClass(string sVer)
        {
            NewEdition = sVer.Replace(".", "");
        }
        public void getNowVersion()
        {
            //获取系统中xml里面存储的版本号               
            String fileName = AppDomain.CurrentDomain.BaseDirectory + "XMLEdition.xml";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            XmlNode xn = xmlDoc.SelectSingleNode("/Content/Project");
            XmlElement xe = (XmlElement)xn;
            if (xe.GetAttribute("id") == "RSDS")
            {
                OldEdition = NewEdition;//动态数组
                XmlElement nxe = xe;
                nxe.SetAttribute("Edition", NewEdition);
                //写入程序的位元率
                nxe.SetAttribute("Is64BitProcess", Environment.Is64BitProcess.ToString());
                xmlDoc.SelectSingleNode("/Content").ReplaceChild(nxe, xe);
                xmlDoc.Save(fileName);
            }
        }
        public string getCopyRightCode()
        {
            string NewEdition = this.OldEdition;//初始化返回的版本号为现在的版本号，以免在网上找不到新的版本号产生错误。
            try
            {
                NetHTTP bes = new NetHTTP();
                string pageHtml = bes.pub_doGET("http://www.nacgame.com/index.php?s=/Version/index/versionDetail/swn/rsds", Encoding.UTF8);
                String fileName = AppDomain.CurrentDomain.BaseDirectory + @"WebGetEdition.xml";
                StreamWriter sw = new StreamWriter(fileName);
                sw.Write(pageHtml);
                sw.Close();
                //获取系统中xml里面存储的版本号 
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(fileName);
                XmlNode xn = xmlDoc.SelectSingleNode("/Content/Project");
                XmlElement xe = (XmlElement)xn;
                if (xe.GetAttribute("id") == "RSDS")
                {
                    NewEdition = xe.GetAttribute("Edition");//动态数组
                    this.NewEdition = NewEdition;
                }
            }
            catch (WebException webEx)
            {
                MessageBox.Show(webEx.Message.ToString());
            }
            return NewEdition;
        }
        public void LoadMath()
        {
            //服务器上的版本号
            string NewEdition = "1002";
            //应用程序中的版本号
            try
            {
                //本地版本号
                this.getNowVersion();
                //服务器上的版本号
                NewEdition = this.getCopyRightCode();
                double newE = double.Parse(NewEdition);
                double oldE = double.Parse(this.OldEdition);
                //比较两个版本号，判断应用程序是否要更新
                if (newE > oldE)
                {
                    //更新程序¨

                    //打开下载窗口
                    // Application.Run(new DownUpdate());

                    //启动安装程序                        
                    System.Diagnostics.Process.Start(AppDomain.CurrentDomain.BaseDirectory + NowUpdateProgram);
                    //Environment.Exit(0);//强制退出
                }

            }
            catch
            {
                //MessageBox.Show("软件获取更新失败,请使用管理员权限运行本程序并检查你的网络！");
            }
        }
    }
}
