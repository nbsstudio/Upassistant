﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UPassistant
{
    /// <summary>
    /// UserNameMapConfigWindow.xaml 的交互逻辑
    /// </summary>
    public partial class UserNameMapConfigWindow : Window
    {
        public UserNameMapConfigWindow()
        {
            InitializeComponent();
            dataGrid.ItemsSource = App.uic.UserNameMap;
        }

        private void addRowBtn_Click(object sender, RoutedEventArgs e)
        {
            bool exists = false;
            foreach (var item in App.uic.UserNameMap)
            {
                if (item.UserName.Equals("<user>"))
                {
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                App.uic.UserNameMap.Add(new Entities.UserNameMapConfigEntity { UserName= "<user>", ReplaceValue= "<replace>" });
            }
            //dataGrid.ItemsSource = App.uic.UserNameMap;
        }

        private void dataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {

        }

        private void dataGrid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.uic.SaveUserNameMapToIni();
        }
    }
}
