﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using UPassistant.Models;

namespace UPassistant
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow : Window
    {
        string CurrentPath = AppDomain.CurrentDomain.BaseDirectory;
        private const int MAX_HEIGHT = 350;
        public LoginWindow()
        {
            InitializeComponent();
            Init();
        }
        private void Init()
        {
            if (App.isLogin)
            {
                logindedPage.Visibility = Visibility.Visible;//提示用户已经登陆过
            }
            userLogin.loginedEvent += UserLogin_loginedEvent;
            userLogin.loginPageLoaded += UserLogin_loginPageLoaded;
            userLogin.changeToLocalLogPage += UserLogin_changeToLocalLogPage;
            userLogin.onlyRoomidSetted += UserLogin_onlyRoomidSetted;
            qroomid.Text = App.egc.RoomID;
            //读取是否自动登录
            oldRoomInputPanle.Visibility = Visibility.Hidden;
            Width = 560;
            Height = MAX_HEIGHT;
        }

        #region 委托
        public delegate void Logged();
        public delegate void Logout();
        public static event Logged logged_event;
        public static event Logout logout_enent;
        #endregion

        #region 事件
        private void UserLogin_onlyRoomidSetted(string roomID)
        {
            SetRoomID(roomID);
        }

        private void UserLogin_changeToLocalLogPage()
        {
            Dispatcher.Invoke(() => {
                oldRoomInputPanle.Visibility = Visibility.Visible;
                logindedPage.Visibility = lodingPage.Visibility = userLogin.Visibility = Visibility.Hidden;
                DoubleAnimation daV = new DoubleAnimation(Height, 80, new Duration(TimeSpan.FromMilliseconds(200)));
                this.BeginAnimation(Window.HeightProperty, daV);
                daV = new DoubleAnimation(Width, 240, new Duration(TimeSpan.FromMilliseconds(200)));
                this.BeginAnimation(Window.WidthProperty, daV);
            });
        }
        /// <summary>
        /// 当登录页面加载完成之后会隐藏界面
        /// </summary>
        private void UserLogin_loginPageLoaded()
        {
            if (App.isLogin)
            {
                return;
            }
            Dispatcher.Invoke(new Action(() =>
            {
                userLogin.Visibility = Visibility.Visible;
                if (oldRoomInputPanle.Visibility == Visibility.Visible)
                {
                    DoubleAnimation daV = new DoubleAnimation(Height, MAX_HEIGHT, new Duration(TimeSpan.FromMilliseconds(200)));
                    this.BeginAnimation(Window.HeightProperty, daV);
                    oldRoomInputPanle.Visibility = Visibility.Hidden;
                }
            }));
        }

        /// <summary>
        /// 当登录成功之后会触发的事件
        /// </summary>
        /// <param name="accessid"></param>
        private void UserLogin_loginedEvent(string accessid,string roomID,bool autoLoginChecked)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(accessid))
                {
                    App.nkc.AccessId = accessid;
                    SetRoomID(roomID);
                    App.nkc.AutoLogin = autoLoginChecked;
                    App.isLogin = true;
                    logged_event();
                    LogHelper.getLogger().Info("登录NCD成功");
                }
            }
            catch (Exception ex)
            {
                LogHelper.getLogger().Info("登陆终端", ex);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            SetRoomID(qroomid.Text);
            this.Close();
        }

        private void SetRoomID(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                App.DoMC.startBtn.SetRoomID(id);
                App.DoMC.startBtn.HideStartButtonMask();
                App.egc.SaveRoomId();
            }
            IniHelper.BackupSetting();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch (Exception)
            {
            }
        }
        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            App.isLogin = false;
            logout_enent?.Invoke();
            UserLogin_loginPageLoaded();
        }
        #endregion

        public void DoAutoLog()
        {
            userLogin.AutoLogin();
        }

        
    }
}
