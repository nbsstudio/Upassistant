﻿using System;
using System.Configuration;
using System.Threading;
using System.Windows;
using NAudio.Wave;
using System.Timers;
using System.Diagnostics;
using RQM;
using System.Threading.Tasks;
using System.Security.Principal;
using UPassistant.Models;
using System.Collections.Generic;
using Addons.Entities;
using UPassistant.UI;

namespace UPassistant
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        #region 属性
        /// <summary>
        /// 主界面
        /// </summary>
        public static MainController DoMC;
        public static Fade.RequestMusic RQM;
        public string path = AppDomain.CurrentDomain.BaseDirectory;
        public static bool isLogin = false;
        public static string mydocdir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\UP主助手\";
        public static int audioOutPut = 0;
        //public static int UseTray = 0;
        //public static bool downInProgress = false;
        public System.Timers.Timer tkb = new System.Timers.Timer(1500);
        //public static CodyDanMu.BiliBili.DM B_eg = new CodyDanMu.BiliBili.DM();
        public static CodyDanMu.IDanMu B_eg = new CodyDanMu.BiliBili.DM(); //接口版弹幕接收引擎，跨平台支持
        public static Addons.Controller addonsController = new Addons.Controller();//构造类的时候自动会搜索dll
        public Addons.BlackListChecker addonsBlackListChecker;
        private static int nowIndex = 0;//读取弹幕的索引
        public static bool OsdShow = true;
        public static int botStwich = 1;//机器人回复开关
        private delegate void UI_Action(CodyDanMu.DanmuInfo log);
        public const string WEBSITE = "http://www.nacgame.com";
        public static int botRate = 20;
        public static double SystemSoundVol = 100;
        System.Threading.Mutex mutex;
        public const string obsPath1 = @"C:\Program Files (x86)\OBS\OBS.exe";
        public const string obsPath2 = @"C:\Program Files\OBS\OBS.exe";
        public static MusicPlayer musicPlayer = new MusicPlayer();
        public static UIconfig uic;
        public static EGconfig egc;
        public static AIconfig rqmc;
        public static Models.Configition.NKconfig nkc;
        public static Entities.EngEntity engc = new Entities.EngEntity();
        public static int dmCount { get; private set; }
        #endregion

        #region 启动部分
        private App()
        {
            
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            string langType = ConfigurationManager.AppSettings.Get("Langs");
            App.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(langType, UriKind.RelativeOrAbsolute) });
            bool isCanCreateNew;
            mutex = new System.Threading.Mutex(true, "OnlyRunOneInstance", out isCanCreateNew);
            if (!isCanCreateNew)
            {
                System.Windows.Forms.MessageBox.Show(App.Current.FindResource("CannotCreateMutex_1").ToString() + "\n" + App.Current.FindResource("CannotCreateMutex_2").ToString());
                Environment.Exit(0);
            }
            Init();
            RQM = new Fade.RequestMusic();
            Thread tr = new Thread(RQM.WaitDL);
            tr.Start();
            
            base.OnStartup(e);
            //NewCody.Dmsystem dmsystem = new NewCody.Dmsystem();
            //string res = dmsystem.Interface("{\"cmd\":\"init\"}");
            string res = Models.Util.NewCodyLink.DoInterface("{\"cmd\":\"init\"}");
        }
        public bool IsAdministrator()
        {
            WindowsIdentity current = WindowsIdentity.GetCurrent();
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
            return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
        }
        private void Check()
        {
            
            if (!IsAdministrator())
            {
                DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext ="请你以“管理员身份”运行助手，可以避免发生一些意外！",nickname="警告", type="warning"});
                //Window tips = new Window();
                //tips.Style = (Style)App.Current.FindResource("AttentionWindow1");
                //tips.Show();
            }
        }

        public void Init()
        {
            //SelfUpdate();
            tkb.Elapsed += Tkb_Tick;
            tkb.Enabled = true;
            B_eg.AddDMLOG += B_eg_AddDMLOG;
            B_eg.AddExceptionLog += B_eg_Add_Exception_Log;
            B_eg.AddDMExcepitonLog += B_eg_AddDMExcepitonLog;
            B_eg.AddPlatformGiftInfo += B_eg_AddPlatformGiftInfo;
            //B_eg.DebugMode = true;
            //插件
            addonsController.logger = Models.LogHelper.getLogger();
            addonsController.AddAddonLOG += addons_con_AddAddonLOG;
            addonsController.AddAddonAction += addons_con_AddAddonAction;
            addonsController.AddAddonError += Addons_con_AddAddonError;
            addonsBlackListChecker = new Addons.BlackListChecker();
            Task.Run(() => {
                try
                {
                    List<Addon> bl=addonsBlackListChecker.GetBlackAddonsNames(WEBSITE);
                    if (bl!=null && bl.Count>0)
                    {
                        foreach (var item in bl)
                        {
                            addonsController.AutoUninstall(item.name,item.author);
                        }
                    }
                }
                catch (Exception e)
                {
                    LogHelper.WirteLog("卸载黑名单插件异常", e);
                }
            });
            uic = new Models.UIconfig();
            egc = new Models.EGconfig();
            rqmc = new AIconfig();
            nkc = new Models.Configition.NKconfig();
            Task.Run(new Action(()=> {
                while (DoMC==null)
                {
                    //Waite the Main Window Open
                    Thread.Sleep(20);
                }
                try
                {
                    IniHelper.RecoverSetting();
                    base.Dispatcher.Invoke(new Action(() =>
                    {
                        uic.Load();
                        egc.Load();
                        rqmc.Load();
                        nkc.Load();
                        AllConfigLoaded();
                        App.DoMC.TurnOffWellcomBoard();
                        Check();
                        AppInitComplate?.Invoke();
                    }));
                }
                catch (Exception ex)
                {
                    LogHelper.WirteLog("配置异常", ex);
                }

            }));
            
            
            try
            {
                Netios.UpdateClass uc = new Netios.UpdateClass(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
                Thread loadmath = new Thread(uc.LoadMath);
                loadmath.Start();
            }
            catch (Exception ex)
            {
                LogHelper.WirteLog("更新异常",ex);
            }
            if (addonsController!=null)
            {
                Addons.Model.loger = Models.LogHelper.getLogger();
            }
        }

       

        private void AllConfigLoaded()
        {
            App.osd = new DanmuShow();
        }
        
        //private void SelfUpdate()
        //{
        //    try
        //    {

        //        //将新文件归位
        //        if (System.IO.File.Exists("SelfUpdate.exe") )
        //        {
        //            if (System.IO.File.Exists("SelfUpdate.fff"))
        //            {
        //                Process p = new Process();
        //                p.StartInfo = new ProcessStartInfo { FileName = AppDomain.CurrentDomain.BaseDirectory + "SelfUpdate.exe" };
        //                p.Start();
        //                Environment.Exit(0);
        //            }
        //            else
        //            {
        //                System.IO.File.Delete("SelfUpdate.exe");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Models.LogHelper.WirteLog("更新异常", ex);
        //    }
        //}
        #endregion

        #region NaudioPlay
        public class MusicPlayer : Models.SoundPlayer
        {
            /// <summary>
            /// 超长自动切掉，-1是关闭，大于0是分钟数
            /// </summary>
            public int OverLimitLength = -1;
            int errtime = 0;
            TimeSpan CheckPlayerTime;
            public override void BeginPlayback(string filename)
            {
                base.BeginPlayback(filename);
                DoMC.EnableButtons(true);
                errtime = 0;
            }
            public override void wavePlayer_PlaybackStopped(object sender, StoppedEventArgs e)
            {
                base.wavePlayer_PlaybackStopped(sender, e);
                DoMC.MusicBoxCtr("stoped", new string[] { "00:00" });
                RQM.addWork(1);
                Models.LryRead lrc = new Models.LryRead();
                lrc.Clear();
                if (e.Exception != null)
                {
                    DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext= string.Format("播放器抛出错误： {0}", e.Exception.Message) });
                    //App.B_eg.AddNewLog(string.Format("播放器抛出错误： {0}", e.Exception.Message));
                }
            }
            public override void GetSoundToPlayer(string File)
            {
                base.GetSoundToPlayer(File);
                if (!string.IsNullOrEmpty(ErrMsg))
                {
                    if (ErrMsg.IndexOf("0xC00D36C4") > -1)
                    {

                        RQM.addWork(5);
                    }
                    Models.LogHelper.WirteLog(ErrMsg);
                }
            }
            public override void ProccessTimer_Elapsed(object sender, ElapsedEventArgs e)
            {
                base.ProccessTimer_Elapsed(sender, e);
                try
                {
                    if (file != null && GetPlayerStu() == 1)
                    {
                        DoMC.MusicBoxCtr("playing", new string[] { FormatTimeSpan(file.CurrentTime), FormatTimeSpan(file.TotalTime) });
                        Models.LryRead lrc = new Models.LryRead();
                        lrc.PlayTrack(new string[] { FormatTimeSpan(file.CurrentTime), FormatTimeSpan(file.TotalTime) }, RQM.songCacheStu[0].geci);
                        if (file.TotalTime - file.CurrentTime ==CheckPlayerTime)
                        {
                            //异常处理，针对于某些系统播放到结尾部分卡住
                            errtime++;
                            if (errtime>3)
                            {
                                DoStop();
                                errtime = 0;
                                //CleanUp();//主动清理，而不是等待播放器自动停止信息
                            }
                            //歌词清空
                            lrc.Clear();
                        }
                        //异常处理2，超长音乐切掉
                        if (OverLimitLength>0 && file.TotalTime.Minutes > OverLimitLength)
                        {
                            DoStop();
                            DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = App.RQM.songCacheStu[0].name + "，这首歌被狠狠地切走了，因为他超出了设定的播放时间" });
                            //App.B_eg.AddNewLog(App.RQM.songCacheStu[0].name+"，这首歌被狠狠地切走了，因为他超出了设定的播放时间");
                        }
                    }
                }
                catch (Exception me)
                {
                    Models.LogHelper.WirteLog("app_timer",me);
                }
                CheckPlayerTime = file.TotalTime - file.CurrentTime;
            }
        }
        public static void PNPplayback(bool isPauseB)
        {
            try
            {
                if (musicPlayer.file != null)
                {
                    
                    if (!isPauseB && musicPlayer.GetPlayerStu()==1)
                    {
                        musicPlayer.DoStop();
                        //wavePlayer.Stop();//切歌
                    }
                    else if (isPauseB)
                    {
                        musicPlayer.PlayOrPause();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(App.Current.FindResource("err_pausefaild").ToString());
                Models.LogHelper.WirteLog(e.Message);
            }
        }
        private static string FormatTimeSpan(TimeSpan ts)
        {
            return string.Format("{0:D2}:{1:D2}", (int)ts.TotalMinutes, ts.Seconds);
        }
        #endregion

        #region 屏幕弹幕部分
        public static DanmuShow osd;
        /// <summary>
        /// 发送标准弹幕委托
        /// </summary>
        delegate void Send_SDM(string name,string v, bool IsNewNow);
        private void PlusDMCount()
        {
            if (dmCount <= int.MaxValue)
            {
                dmCount++;
            }
        }
        #endregion

        #region 事件
        private void B_eg_AddPlatformGiftInfo(CodyDanMu.PlatformGiftInfo dmmsg)
        {
            //TODO 让用户及时抢到礼物
        }
        private void B_eg_AddDMLOG(CodyDanMu.DanmuInfo dmmsg)
        {
            B_eg_AddDMLOG_ht(dmmsg);
        }

        private void B_eg_Add_Exception_Log(Exception ex)
        {
            LogHelper.WirteLog("弹幕核心报错：", ex);
        }

        private void B_eg_AddDMExcepitonLog(Exception ex,string originstr)
        {
            LogHelper.WirteLog("弹幕解析报错,元数据：“"+ originstr+"”", ex);
        }
        private void B_eg_AddDMLOG_ht(CodyDanMu.DanmuInfo temp)
        {
            if (B_eg.TMQ.Count == 0)
            {
                nowIndex = 0;
            }
            string systemNickname = App.uic.FindReplaceName(temp.nickname);
            try
            {
                Send_SDM ssdm = new Send_SDM(osd.ShowDM);
               
                ssdm.BeginInvoke(systemNickname, temp.logtext, true, null, null);
                //发送到小弹窗
                if (uic.UseWellcomBoard)
                {
                    if (temp.member == 1)
                    {
                        try
                        {
                            new WellcomBaord(temp).Poup();
                        }
                        catch (Exception pe)
                        {
                            Models.LogHelper.WirteLog("弹出欢迎页异常", pe);
                        }
                    }
                }
                //判断是否房管
                bool isAdmin = temp.admin == 1 ? true : false;
                RQM.rqmOne(temp.logtext, temp.nickname, temp.datetime, isAdmin, nowIndex);
                nowIndex++;
                temp.nickname = systemNickname;
                if (temp.member == 1)
                {
                    temp.nickname = Langs.LangModel.L("sysinfo");
                }
                DoMC.AddDMToInterface(temp);

                //发送弹幕内容给插件
                App.addonsController.SendDMLog(temp);
                if (App.egc.SupportOldAddons)
                {
                    addonsController.DMShow(temp);
                }
            }
            catch (Exception eee)
            {
                Models.LogHelper.WirteLog("弹幕与插件异常", eee);
            }
            
            try
            {
                //发送弹幕给云
                if (App.isLogin)
                {
                    App.DoMC.SendDMToCloud(temp);
                }
                //发送弹幕到放映厅
                Models.Util.APlayerLink.GetInstance().SendDMToAPlayer(temp);
            }
            catch (Exception eee)
            {
                Models.LogHelper.WirteLog("附属功能异常", eee);
            }
            /*    
            try
            {
                //发送弹幕内容给机器人
                if (temp.type != "system")//所有系统信息不进行回复，包括机器人自己的信息
                {
                    Fade.BOT bot = new Fade.BOT(botStwich == 1 ? true : false, botRate);

                    string resLog = "";
                    if (bot.AskToBot(temp.logtext, temp.type, systemNickname, out resLog))
                    {
                        DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = resLog , uid="bot",nickname= "昵称：" + Fade.BOT.BName });
                        //B_eg.AddNewLog(resLog, "bot", "昵称：" + Fade.BOT.BName);
                        UI_Action uia = new UI_Action(addonsController.SendActionLog);
                        uia.BeginInvoke(new CodyDanMu.DanmuInfo { logtext = "机器人回复：" + resLog }, null, null);
                    }
                }
                PlusDMCount();
            }
            catch (Exception eee)
            {
                Models.LogHelper.WirteLog("机器人异常",eee);
            }
            */
            PlusDMCount();
        }

        void addons_con_AddAddonLOG(string dmmsg, bool deleteTile)
        {
            try
            {
            string title = "[插件]";
            if (deleteTile)
            {
                title = "";
            }
                DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo {logtext= title + dmmsg });
                //B_eg.AddNewLog(title + dmmsg);

            }
            catch (Exception e)
            {
                LogHelper.WirteLog("", e);
            }
        }
        void addons_con_AddAddonAction(string log,string name)
        {
            Models.ParseAddonAction paa = new Models.ParseAddonAction();
            paa.Parse(log, name);
        }
        private void Addons_con_AddAddonError(string log)
        {
            DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = log });
            //B_eg.AddNewLog(log);
        }
        private static void Tkb_Tick(object sender, EventArgs e)
        {
            if (App.uic!=null)
            {
                Thread dlm = new Thread(Subs);
                dlm.Start();
            }
            
        }

        static void Subs()
        {
            //歌单信息处理
            if (App.uic.OsdType == ViewModels.DMTypeEnum.MusicList)
            {
                try
                {
                    osd.ShowList(RQM.GetList());
                    //发送歌单
                }
                catch (Exception e)
                {
                    LogHelper.WirteLog("发送歌单失败", e);
                }
                
            }

        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }
        #endregion

        #region 委托
        public delegate void _AppInitComplate();
        public static event _AppInitComplate AppInitComplate;
        #endregion
    }
    #region 类型
    public class DownloadItem
    {
        public int index { get; set; }
        public long sif_id { get; set; }
        public MatchIDs FFR { get; set; }
    }
    #endregion
}