﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using UPassistant.ViewModels;
using System.Windows.Controls;
using UPassistant.ViewModels.DanMn;
using UPassistant.Models;

namespace UPassistant
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class DanmuShow : Window
    {
        #region 属性
        //private DMTypeEnum _cdmml = DMTypeEnum.Null;

        //public DMTypeEnum cdmml
        //{
        //    get { return _cdmml; }
        //    set
        //    {
        //        SwitchMode(value);
        //        _cdmml = value;
        //    }
        //}
        
        //public int tt_row = 9;//弹幕总行数
        //public int list_row = 5;//歌单总行数
        private DMTypeEnum NowModel = DMTypeEnum.Null;
        private double textopt = 1;
        public bool AutoClear = false;
        public SolidColorBrush FontColour = new SolidColorBrush(Colors.White);

        private FontFamily fontFamily;

        public new FontFamily FontFamily
        {
            get { return fontFamily; }
            set { fontFamily = value;
                if (osds!=null)
                {
                    osds.SetNowFont(value);
                }
            }
        }

        private SolidColorBrush BgColour = new SolidColorBrush(Colors.White);
        private IOsd osds;
        #endregion

        #region 常量
        private const int WS_EX_TRANSPARENT = 0x20;
        private const int WS_EX_TOOLWINDOW = 0x0080;
        private const uint WS_EX_LAYERED = 0x80000;
        private const int GWL_EXSTYLE = (-20);
        #endregion

        public DanmuShow()
        {
            InitializeComponent();
            Left = App.uic.OsdPosition.X;
            Top = App.uic.OsdPosition.Y;
            SetFadeTime(App.uic.FadeTime);
            SetBgColour(App.uic.OsdBgColor);
            SwitchMode(App.uic.OsdType);
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            Init();
        }

        #region 界面
        public void Init()
        {
            Show();
            IntPtr hwnd = new WindowInteropHelper(this).Handle;
            uint extendedStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
            SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_TRANSPARENT | WS_EX_LAYERED | WS_EX_TOOLWINDOW);
        }
        public void SwitchMode(DMTypeEnum NowMode)
        {
            try
            {
                if (this.NowModel == NowMode)
                {
                    return;
                }
                else
                {
                    this.NowModel = NowMode;
                }
                if (osds != null)
                {
                    osds.SetOFF(true);
                    osds = null;
                    Recover();
                }
                Dispatcher.Invoke(new Action(() =>
                {
                    switch (NowMode)
                    {
                        case DMTypeEnum.NewDDM:
                            osds = new NewOsdShow(false, this);
                            break;
                        case DMTypeEnum.DesktopDM:
                            osds = new OsdShow(false, this);
                            break;
                        case DMTypeEnum.MusicList:
                            osds = new Osdmcshow(false, this);
                            break;
                        case DMTypeEnum.GameDM:
                            osds = new INGAMEShow(false, this);
                            break;
                        default:
                            osds = new OsdShow(false, this);
                            break;
                    }
                    osds.InitDM();
                    osds.OsdFontSize = App.uic.DMFontSize;
                    osds.OsdHexColor = "#FFFFFFFF";
                }));

                Thread tr = new Thread(osds.WaitDMTW);
                tr.Start();
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("换弹幕模式错误",e);
            }
            
        }
        public void TurnOff()
        {
            osds.SetOFF(true);
            if (App.uic.OsdType == DMTypeEnum.DesktopDM || App.uic.OsdType == DMTypeEnum.NewDDM)
            {
                Hide();
            }
        }
        #endregion

        #region 桌面弹幕与歌单
        /// <summary>
        /// 恢复初始值
        /// </summary>
        public void Recover()
        {
            try
            {
                foreach (var item in dmpanel.Children)
                {
                    TextBlock tb = (TextBlock)item;
                    if (tb.Name != "top_tips" || tb.Name != "osdText")
                    {
                        tb.Opacity = 0;
                        tb.Text = "";
                    }
                }
                this.Opacity = dmpanel.Opacity =1;

            }
            catch (Exception e)
            {
                LogHelper.WirteLog("桌面弹幕清空错误",e);
            }
            
        }
        public void ShowList(string v)
        {
            try
            {
                if (App.uic.OsdType == DMTypeEnum.MusicList)
                {
                    osds.SetTt_row(App.uic.TotalRow);
                    osds.AddDM("","L",v, true);
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("ShowList Error", e);
            }
        }
        public void ShowDM(string name,string text, bool IsNewNow)
        {
            try
            {
                if (App.uic.OsdType == DMTypeEnum.MusicList)
                {
                    return;
                }
                osds.SetTt_row(App.uic.TotalRow);
                if (App.uic.OsdType == DMTypeEnum.NewDDM)
                {
                    osds.AddDM(name, "N", text, IsNewNow);
                }
                else
                {
                    if (App.uic.DMSort == "BA")
                    {
                        osds.AddDM(name, "N", text + "[" + name + "]", IsNewNow);
                    }
                    else
                    {
                        osds.AddDM(name, "N", "[" + name + "]" + text, IsNewNow);
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("sdm",e);
            }
            
        }
        public void TestAndApp(string FontName, int FontSize, double _textopt)
        {
            osds.TestAndApplication(FontName,FontSize, _textopt);
        }
        public void SetFadeTime(int fadetime)
        {
            if (osds != null)
            {
                if (fadetime < 1)
                {
                    osds.FadeTime = 1;
                }
                else
                {
                    osds.FadeTime = fadetime;
                }
            }


        }
        public int GetFadeTime()
        {
            if (osds!=null)
            {
                return osds.FadeTime;
            }
            return 5;
        }
        #endregion

        #region DLL
        [DllImport("user32", EntryPoint = "SetWindowLong")]
        private static extern uint SetWindowLong(IntPtr hwnd, int nIndex, uint dwNewLong);
        [DllImport("user32", EntryPoint = "GetWindowLong")]
        private static extern uint GetWindowLong(IntPtr hwnd, int nIndex);

        #endregion

        #region 封装
        public double Textopt
        {
            get
            {
                return textopt;
            }
            set
            {
                textopt = value;
                Opacity = textopt;
            }
        }
        public string GetFontColourString()
        {
            return FontColour.Color.ToString();
        }
        private void SetBgColour(string colourbrush)
        {

            System.Drawing.Color tempc = System.Drawing.ColorTranslator.FromHtml(colourbrush);
            Color xtempc = Color.FromRgb(tempc.R, tempc.G, tempc.B);
            if (string.IsNullOrWhiteSpace(colourbrush))
            {
                xtempc = Colors.White;
            }
            SetBgColour(xtempc);
        }
        public void SetBgColour(Color colourbrush)
        {
            if (colourbrush.Equals(Colors.White))
            {
                BgColour.Color = Colors.Transparent;
            }
            else
            {
                BgColour.Color = colourbrush;
            }
            Background = BgColour;
        }
        public SolidColorBrush getBgColour()
        {
            return BgColour;
        }
        public string GetBgColourString()
        {
            string colorcode = BgColour.Color.ToString();
            if (BgColour.Color.Equals(Colors.Transparent))
            {
                colorcode = Colors.White.ToString();
            }
            return colorcode;
        }
        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (osds!=null)
            {
                osds.SetOFF(true);
                osds.ClearDM();
            }
        }
    }
}
