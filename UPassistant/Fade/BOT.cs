﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace UPassistant.Fade
{
    class BOT
    {
        public bool Switch = true;
        public static string BName = "机器人";//机器人的名字
        private int rate = 20;//机器人的回复率
        private Netios.NetHTTP bes = new Netios.NetHTTP();
        public BOT(bool IsOpened,int Rate)
        {
            Switch = IsOpened;
            rate = Rate;
        }
        /// <summary>
        /// 问机器人问题
        /// </summary>
        /// <param name="question">问题内容</param>
        /// <param name="dm_uid">弹幕的标识，除了system收不到，其余都收到</param>
        /// <param name="dm_nickname">发弹幕的那个人</param>
        /// <param name="res">回传答案</param>
        /// <returns></returns>
        [Obsolete]
        public bool AskToBot(string question,string dm_uid,string dm_nickname,out string res)
        {
            res = BName+"已关闭";
            if (Switch)
            {
                if (dm_uid=="bot")
                {
                    return false;
                }
                if (CanAsk())//判断发送给机器人的几率
                {
                    Models.JsonHelper jh = new Models.JsonHelper();
                    string botinfo = jh.SerializeObject(new AskBotMsg { key= "ad79a0fdc9e06c428f0de159d80301b0", info=question });
                    //string botinfo=string.Format("{\"key\":\"{0}\",\"info\":\"{1}\"}", , question);
                    //res = bes.pub_doPOST("http://www.tuling123.com/openapi/api", botinfo, Encoding.UTF8);
                    res = bes.pub_doResponse(new Netios.postItem { GOP="POST", datatype= "application/json", postData= botinfo, postUrl= "http://www.tuling123.com/openapi/api" },Encoding.UTF8);
                    try
                    {
                        BotMsg ose = jh.DeserializeJsonToObject<BotMsg>(res);
                        res = ose.text;
                    }
                    catch (Exception)
                    {
                        res = "你的话我不懂！";
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        [Obsolete]
        public bool CanAsk()
        {
            Random _rd = new Random();
            int eca = _rd.Next(0, 100);
            if (rate==0)//设置了几率为0
            {
                return false;
            }
            else if(rate==100)
            {
                return true;
            }
            if (rate > eca)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    public class BotMsg
    {
        public int code { get; set; }
        public string text { get; set; }
    }
    public class AskBotMsg
    {
       public string key { get; set; }
       public string info { get; set; }
    }
}
