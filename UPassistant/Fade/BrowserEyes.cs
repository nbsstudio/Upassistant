﻿using System;
using System.IO;
using System.Net;
using System.Text;
namespace UPassistant.Fade
{
    class BrowserEyes
    {

        private string rid = "";
        private string url = "";
        public string pub_doGET(string url,Encoding _encode)
        {

            string result = "";
            if (url !=null||url!= string.Empty)
            {
                try
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] data = encoding.GetBytes("");
                    HttpWebRequest myRequest =
                    (HttpWebRequest)WebRequest.Create(url);
                    myRequest.Method = "GET";
                    myRequest.ContentType = "application/x-www-form-urlencoded";
                    myRequest.ContentLength = data.Length;
                    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                    StreamReader reader = new StreamReader(myResponse.GetResponseStream(), _encode);
                    result = reader.ReadToEnd();
                    result = result.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    int status = (int)myResponse.StatusCode;
                    reader.Close();

                }
                catch (Exception)
                {
                    
                }
            }
            return result;
        }
        public string pub_doPOST(string url, string POdata, Encoding _encode)
        {

            string result = "";
            if (url != null || url != string.Empty)
            {
                try
                {
                    //ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] data = _encode.GetBytes(POdata);
                    HttpWebRequest myRequest =
                    (HttpWebRequest)WebRequest.Create(url);
                    myRequest.Method = "POST";
                    myRequest.ContentType = "application/x-www-form-urlencoded";
                    myRequest.ContentLength = data.Length;
                    Stream newStream = myRequest.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                    StreamReader reader = new StreamReader(myResponse.GetResponseStream(), _encode);
                    result = reader.ReadToEnd();
                    result = result.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    int status = (int)myResponse.StatusCode;
                    reader.Close();

                }
                catch (Exception)
                {

                }
            }
            return result;
        }


        public string transpf(int pfid)
        {
            string url = "";
            switch (pfid)
            {
                case 1:
                    //鬥魚
                    url = "http://www.douyutv.com/";
                    break;
                case 2:
                    //bilibili
                    url = "http://live.bilibili.com/";
                    break;
                default:
                    break;
            }
            return url;
        }

        public postItem transpost(string mode)
        {
            postItem pi = new postItem();
            switch (mode)
            {
                case "bilibili-dm":
                    pi.postData = "roomid=" + rid;
                    pi.postUrl = url + "ajax/msg";
                    pi.GOP = "POST";
                    break;
                case "panda-dm":
                    pi.postData = "roomid=" + rid;
                    pi.postUrl = "http://s.panda.tv/hit";
                    pi.GOP = "GET";
                    break;
            }
            return pi;
        }

        internal string pub_cerfGET(string v)
        {
            throw new NotImplementedException();
        }
    }

    struct postItem
    {
        public string postData;
        public string postUrl;
        public string GOP;
    }
}
