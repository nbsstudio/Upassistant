﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using UPassistant.Models.Music;

namespace UPassistant.Fade
{

    public class RequestMusic : Models.CMDClass
    {
        private bool _Switch;

        public bool Switch
        {
            get { return _Switch; }
            set { _Switch = value;App.uic.DisableRQM = !_Switch; }
        }
        public bool LrcSwitch = true;//输出歌词开关
        public int LrcOut = 1;//歌词输出模式：1-原词优先，2-翻译优先，3-结合版
        public bool ADMSwitch = true;//允许房管按序号删歌
        public List<SongInfo> songCacheStu = new List<SongInfo>();
        private Queue<RQMwork> DoWork = new Queue<RQMwork>();//工作队列，避免音乐列表数组在操作过程中的冲突
        private const string BKM = "黑名单歌曲";
        private const string KW = "关键词.kw";
        /// <summary>
        /// 用于判断点歌命令的分割符
        /// </summary>
        public char spc = ' ';
        /// <summary>
        /// 用于判断点歌命令的头部
        /// </summary>
        public string spt = Models.AIconfig.DEFULT_SPT;
        public string adminspt = Models.AIconfig.DEFULT_ADMINSPT;//用于判断房管切歌
        private int[] topdlthread = new int[2] { 5, 0 };//下载音乐进程的封顶值，{封顶，当前}
        public bool isPlaying = false;
        private delegate string LetSH(string sname);
        private Netios.NetHTTP bes = new Netios.NetHTTP();
        private List<string> address = new List<string>();
        private delegate void SendMusicToPlayer(string filepath);
        public bool assist_playnext = false;//当播放列表被占用时，会将此次指令留到下次rmmOne()触发时顺便触发
        public string[] modes = new string[] { "163music", "qqmusic", "xiamimusic", "kuwoomusic", "5sing" };
        public string mode = "163music";
        public bool isAutoChangeSearchEG = true;
        private delegate void dl_start(string P_sUrl, string P_mID, string P_mode, bool P_isWaittingMode);
        //语言读取时出问题
        public string[] cacheInfoC = new string[] { "正在队列", "緩存中", "缓存完成", "缓存失败" };
        public int RmkickTime = 10;
        public bool LimitPresonRQM = false;
        public int LimitCount = 5;
        private string CloudAddress;
        private AutoControlModel autoControlModel;
        /// <summary>
        /// 点歌时自动操作外部播放器等功能的开关
        /// </summary>
        public bool AllowAutoControl
        {
            get { return autoControlModel.Switch; }
            set { autoControlModel.Switch = value; }
        }

        #region 引擎选项
        public bool Use_kuwo_eg = true;
        public bool Use_xiami_eg = true;
        public bool Use_163_eg = true;
        public bool Use_5sing_sw_eg = true;//这个是启用禁用的开关
        public bool Use_5sing_eg = false;//这个是完全采用5sing的开关
        public bool Use_KeyWord = false;
        #endregion
        public RequestMusic()
        {
            autoControlModel = new AutoControlModel();
            //载入点歌文本的模板
            ReadModul();
            //初始化网络地址
            CloudAddress = App.rqmc.LoadMusicCloudAddress();
            string stp = AppDomain.CurrentDomain.BaseDirectory + @"cache\";
            makeLostDirs(stp);
            RQM.PublicLib.EndDownLoadEvent += PublicLib_EndDownLoadEvent;
        }

        public bool rqmOne(string musicName, string owner, string rq_time, bool isAdmin, int index)
        {
            if (musicName != "")
            {
                RQM_CMD ccc = new RQM_CMD(spc, spt, adminspt);
                //Regex regex = new Regex("^弹幕：", RegexOptions.IgnoreCase);
                //string pmusicName = regex.Replace(musicName, string.Empty);
                RQM_CMD.Ptr Cmdptr = ccc.GetFuncCode(musicName);
                bool canAdd = Switch;//默认跟开关的值一致
                switch (Cmdptr)
                {
                    case RQM_CMD.Ptr.RequestMusic:
                        string TotalName = ccc.GetMatchValueOne(RQM_CMD.Ptr.RequestMusic, musicName);
                        if (canAdd)
                        {
                            canAdd = CheckRP(TotalName);//检测是否有重复
                        }

                        if (LimitPresonRQM && CheckLimit(owner))
                        {
                            App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = owner + "->点太多歌了，超出了设定范围" });
                            //App.B_eg.AddNewLog( owner + "->点太多歌了，超出了设定范围");
                            App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]" + owner + "->点歌超出了限制" });
                            return false;
                        }
                        if (canAdd)
                        {
                            //接着添加缓存信息
                            SongInfo sif = new SongInfo();
                            Random _rdn = new Random();
                            sif.id = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmss")) + _rdn.Next(5000);
                            //关键词和黑名单查看
                            string vdName = CheckKeyWord(TotalName);//关键词和黑名单合并，所以此句置Use_KeyWord前
                            if (vdName == BKM)
                            {
                                sif.CacheSta = 3;
                                App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = TotalName + "->此歌曲被列入黑名单了，不是我的锅T.T" });
                                //App.B_eg.AddNewLog(TotalName + "->此歌曲被列入黑名单了，不是我的锅T.T");
                                App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]" + TotalName + "->此歌曲被列入黑名单了" });
                                sif.name = BKM;
                            }
                            else
                            {
                                if (Use_KeyWord)
                                {
                                    sif.name = vdName;
                                }
                                else
                                {
                                    sif.name = TotalName;
                                }
                                sif.CacheSta = 0;
                            }
                            sif.or_name = TotalName;//记录解析前的名称
                            sif.or_dm = musicName;
                            sif.Play_Stu = 0;
                            sif.mode = "搜索中";
                            sif.owner = owner;
                            sif.link = "";
                            sif.HoldingTime = 0;//占用时间用于无人值守自动化处理
                            songCacheStu.Add(sif);
                            App.DoMC.AddItemToMusicList(sif.name, "正在队列", sif.mode, sif.owner);
                            //搜索曲库
                            Thread StartSerch = new Thread(MutiSS);
                            StartSerch.Start(sif.id);
                            WriteList();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case RQM_CMD.Ptr.AdminCutMusic:
                        if (isAdmin && canAdd)
                        {
                            ///////////////////////////////////////////
                            //由于自动切歌没有重新加载文件，所以不能直接调用playnext
                            if (App.musicPlayer.wavePlayer != null)
                            {
                                App.musicPlayer.DoStop();
                                //App.wavePlayer.Stop();
                            }
                            ///////////////////////////////////////////
                            return false;
                        }
                        break;
                    case RQM_CMD.Ptr.CancelMusic:
                        if (canAdd)
                        {
                            try
                            {
                                for (int i = songCacheStu.Count - 1; i < songCacheStu.Count; i--)
                                {
                                    if (songCacheStu[i].owner == owner)
                                    {
                                        addWork(3, i);
                                        return false;
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                        return false;
                    case RQM_CMD.Ptr.AdminDeleteMusic:
                        if (isAdmin && ADMSwitch)
                        {
                            string idx = ccc.GetMatchValueOne(RQM_CMD.Ptr.AdminDeleteMusic, musicName);
                            this.addWork(3, int.Parse(idx) - 1);
                        }
                        return true;
                    case RQM_CMD.Ptr.None:
                        return false;
                }
            }
            return false;
        }

        #region 工作队列
        /// <summary>
        /// 本类的自我操控，禁止阻塞。
        /// </summary>
        public void WaitDL()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(1000);
                    QueueAction();//处理工作
                    KickWrongThink();//剔除错误的东西
                }
                catch (Exception e)
                {
                    Models.LogHelper.WirteLog("rqm_WaitDL",e);
                }
            }
        }

        /// <summary>
        /// 添加工作队列
        /// </summary>
        /// <param name="workid">1是切歌2是清空3是删除4是黑名单5是强制删除6是切歌（自动模式）</param>
        public void addWork(int workid, int Muindex = 0)
        {
            RQMwork c = new RQMwork();
            c.ID = workid;
            c.P_index = Muindex;
            DoWork.Enqueue(c);
        }
        private void QueueAction()
        {
            try
            {
                if (topdlthread[1] == 0)
                {
                    if (DoWork.Count > 0 && DoWork != null)
                    {
                        RQMwork cc = DoWork.Dequeue();
                        switch (cc.ID)
                        {
                            case 1:
                                PlayNext();
                                break;
                            case 2:
                                clearAll();
                                break;
                            case 3:
                                DeleteAMusic(cc.P_index);
                                break;
                            case 4:
                                BlackListTheMusic(cc.P_index);
                                break;
                            case 5:
                                ForceDeleteAMusic(cc.P_index);
                                break;
                            case 6:
                                PlayNext(true);
                                break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }
        private void KickWrongThink()
        {
            int index = songCacheStu.FindIndex((SongInfo s) => s.CacheSta == 0 || s.CacheSta == 3);//踢走长期队列、长期缓存完成、缓存失败
            SongInfo sif = new SongInfo();
            if (index > -1 && songCacheStu.Count > index)
            {
                sif = songCacheStu[index];
            }
            else
            {
                int cosindex = songCacheStu.FindIndex((SongInfo s) => s.CacheSta == 2 && s.Play_Stu == 0);
                if (cosindex == 0)//对缓存完成做特殊处理
                {
                    sif = songCacheStu[cosindex];
                }
                else
                {
                    return;
                }
            }
            try
            {
                sif.HoldingTime++;
                songCacheStu[index] = sif;
                if (sif.HoldingTime >= RmkickTime)
                {
                    DeleteAMusic(index);
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region 下载音乐

        public void pd_beforeDownload(long Vid)
        {
            //检测下载结果
            try
            {
                for (int i = 0; i < songCacheStu.Count; i++)
                {
                    if (songCacheStu[i].id == Vid)
                    {
                        SongInfo temp = songCacheStu[i];
                        if (temp.CacheSta == 1)
                        {
                            string result = pd_isCached(temp.matchIDs.mid);
                            temp.CacheSta = result == App.Current.FindResource("pstu_done").ToString() ? 2 : 3;
                            songCacheStu[i] = temp;
                            WriteList();
                            RefreshITFL();
                        }
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("下载异常",e);
            }
        }
        private string pd_isCached(string musicID)
        {
            string stp = AppDomain.CurrentDomain.BaseDirectory + @"cache\";
            //makeLostDirs(stp);
            string _default = App.Current.FindResource("pstu_download_err").ToString();//默认显示缓存失败
            if (System.IO.File.Exists(stp + musicID + ".ms"))
            {
                if (System.IO.File.Exists(stp + musicID + ".ms.ok"))
                {
                    _default = App.Current.FindResource("pstu_done").ToString();
                }
                else
                {
                    _default = App.Current.FindResource("pstu_download_err").ToString();//默认显示缓存失败
                }
            }
            return _default;
        }

        private void makeLostDirs(string stp)
        {
            if (!System.IO.Directory.Exists(stp))
            {
                System.IO.Directory.CreateDirectory(stp);
            }
        }
        #endregion

        #region 事件
        public void clearAll()
        {
            songCacheStu.Clear();
            RefreshITFL();
            WriteList();
            App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]音乐列表已清空" });
            autoControlModel.autoOutsidePlay(false, false);
        }
        public bool DeleteAMusic(int muindex)
        {
            if (songCacheStu.Count == 0)
            {
                return false;
            }
            if (muindex == 0 && songCacheStu[muindex].Play_Stu == 1)
            {
                App.musicPlayer.DoStop();
                return true;
            }
            else
            {
                songCacheStu.RemoveAt(muindex);
                WriteList();
                RefreshITFL();
                return true;
            }
        }
        private bool ForceDeleteAMusic(int muindex)
        {
            if (songCacheStu.Count == 0)
            {
                return false;
            }
            string sname = songCacheStu[muindex].name;
            songCacheStu.RemoveAt(muindex);
            WriteList();
            RefreshITFL();
            App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = "已强制删除歌曲：" + sname + "，可能是系统解码失败或源数据被加密。" , nickname= "除错机制",uid= "system" });
            //App.B_eg.AddNewLog("已强制删除歌曲：" + sname + "，可能是系统解码失败或源数据被加密。", "system", "除错机制");
            App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]已强制删除歌曲：" + sname });
            if (muindex == 0 && songCacheStu.Count > 0)
            {
                PlayQueue();
            }
            else if (songCacheStu.Count == 0)
            {
                autoControlModel.autoOutsidePlay(false, false);
            }
            return true;
        }
        public bool BlackListTheMusic(int muindex)
        {
            try
            {
                string blsm_name = songCacheStu[muindex].matchIDs.name;
                XmlDocument _xmld = new XmlDocument();
                if (!System.IO.File.Exists(App.mydocdir + KW))
                {
                    //创建根节点 config
                    XmlElement BlackListMusic = _xmld.CreateElement("Main");
                    //把根节点加到xml文档中
                    _xmld.AppendChild(BlackListMusic);
                    _xmld.Save(App.mydocdir + KW);
                }
                _xmld.Load(App.mydocdir + KW);
                XmlNode blmnode = _xmld.FirstChild;
                //创建一个节点 path(用于做子节点)
                XmlElement _xe = _xmld.CreateElement("KeyWord");
                XmlAttribute _xa = _xmld.CreateAttribute("keyword");
                XmlAttribute _xa2 = _xmld.CreateAttribute("stg");
                _xa.Value = blsm_name;
                _xa2.Value = "本地";
                _xe.Attributes.Append(_xa);
                _xe.Attributes.Append(_xa2);
                _xe.InnerText = BKM;
                blmnode.AppendChild(_xe);
                _xmld.Save(App.mydocdir + KW);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        /// <summary>
        /// 播放队列，遇到缓存失败就播放下一条
        /// </summary>
        /// <returns></returns>
        public bool PlayQueue()
        {
            if (songCacheStu.Count > 0 && songCacheStu != null)
            {
                if (songCacheStu[0].CacheSta == 3)
                {
                    string musicName = songCacheStu[0].name;
                    App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext= musicName + "->播放失败：无法缓存该歌曲" });
                    PlayNext();
                    App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]失败：无法缓存：" + musicName });
                    return false;
                }

                if (songCacheStu[0].Play_Stu == 1 || App.musicPlayer.GetPlayerStu() == 1)
                {
                    return false;
                }

                if (songCacheStu[0].CacheSta == 2 && songCacheStu[0].Play_Stu == 0)
                {
                    SongInfo scs = songCacheStu[0];
                    string fp = AppDomain.CurrentDomain.BaseDirectory + @"cache\" + songCacheStu[0].matchIDs.mid + ".ms";
                    if (File.Exists(fp + ".ok"))
                    {
                        App.musicPlayer.GetSoundToPlayer(fp);
                        App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo {logtext = "[点歌]开始播放：" + songCacheStu[0].name });
                        autoControlModel.autoOutsidePlay(false,false);
                        scs.Play_Stu = 1;
                        //播放歌词
                        if (LrcSwitch)
                        {
                            Models.LryRead lrc = new Models.LryRead(songCacheStu[0].matchIDs.mid);
                            scs.geci = lrc.Read();
                        }
                        songCacheStu[0] = scs;
                        WriteList();
                        RefreshITFL();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除列表的第一个，并命令播放下一个
        /// </summary>
        /// <param name="isAuto">是否属于非人手操作</param>
        public void PlayNext(bool isAuto = false)
        {
            if (isAuto && songCacheStu.Count>0)
            {
                if (songCacheStu[0].Play_Stu == 1)
                {
                    App.musicPlayer.DoStop();
                    App.musicPlayer.CleanUp();//主动清理，而不是等待播放器自动停止信息
                } 
            }
            if (songCacheStu.Count > 0)
            {
                songCacheStu.RemoveAt(0);
                PlayQueue();
                int s_count = songCacheStu.Count;
                App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]正在切换音乐,还有" + s_count + "首音乐待播放" });
                if (s_count == 0)
                {
                    App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]音乐列表已清空" });
                    autoControlModel.autoOutsidePlay(false, false);
                }
            }
            WriteList();
            RefreshITFL();
        }
        #endregion
        public void MutiSS(object Vid)
        {
            int index = GetIndex((long)Vid);
            bool CanDL = false;
            if (index != -1)
            {
                SongInfo temp = songCacheStu[index];
                if (temp.CacheSta == 3 && temp.name == BKM)//判断是否已经不允许下载了
                {
                    return;
                }
                try
                {
                    RQM.FindMusic fdm = new RQM.FindMusic(Use_163_eg, Use_kuwo_eg, Use_xiami_eg, Use_5sing_sw_eg);
                    RQM.MatchIDs _res = fdm.DefaultRes;
                    if (Use_5sing_eg)
                    {
                        //5Sing搜索
                        _res = fdm.VSingSearchOnly(temp.name);
                        if (_res.name != null && _res.name != "未找到")
                        {
                            temp.matchIDs = _res;
                            temp.mode = _res.mode;
                            CanDL = true;
                        }
                        else
                        {
                            CanDL = false;
                        }
                    }
                    else
                    {
                        //全部搜索
                        _res = fdm.SyncSearch(temp.name);
                        if (_res.name != null && _res.name != "未找到")
                        {
                            temp.matchIDs = _res;
                            temp.mode = _res.mode;
                            CanDL = true;
                        }
                        else
                        {
                            CanDL = false;
                        }
                    }
                    if (!CanDL)
                    {
                        SetErr(temp, ErrType.CanNotFound);
                        return;
                    }
                    else
                    {
                        //实行标准化储存
                        temp.name = temp.matchIDs.name;
                        temp.link = temp.matchIDs.custom_link;
                        temp.Artis = temp.matchIDs.artis;
                        App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]水友点歌：" + temp.name });
                        //通知下载音乐
                        temp.CacheSta = 1;
                        songCacheStu[index] = temp;//置缓存中状态
                        App.DoMC.AddMusicItemStatus(index, App.Current.FindResource("pstu_downloading").ToString(), temp.mode);
                        RQM.PRDL dler = new RQM.PRDL((long)Vid, temp.matchIDs.mid, temp.matchIDs.name, 10,!string.IsNullOrWhiteSpace(CloudAddress),CloudAddress);
                       
                        switch (temp.mode)
                        {
                            case "163music":
                                dler.NetEasy_oldapi(temp.matchIDs.custom_link);
                                break;
                            case "kuwoomusic":
                                dler.KuWoPR();
                                break;
                            case "kugomusic":
                                dler.KuGoPR(temp.matchIDs.custom_link);
                                break;
                            case "5sing":
                                dler.VSing();
                                break;
                            default:
                                SetErr(temp, ErrType.DownLoadFaild);
                                break;
                        }
                        WriteList();
                        //寻找歌词
                        RQM.FindLrc flrc = new RQM.FindLrc();
                        flrc.FindAndDownload(temp.name, temp.Artis, temp.matchIDs.mid, App.RQM.LrcOut);
                    }
                }
                catch (Exception)
                {
                    SetErr(temp, ErrType.UnKnow);
                }
            }
        }

        private void PublicLib_EndDownLoadEvent(long taskid)
        {
            //检查下载
            pd_beforeDownload(taskid);
            //播放
            if (songCacheStu.Count > 0 && songCacheStu != null)
            {
                if (GetIndex(taskid) == 0 && songCacheStu[0].Play_Stu == 0)//列表第未开始播放才会触发
                {
                    PlayQueue();
                }
            }
        }

        private string CheckKeyWord(string KeyWord)
        {
            string res = KeyWord;
            XmlDocument _xd = new XmlDocument();
            if (System.IO.File.Exists(App.mydocdir + "关键词.kw"))
            {
                _xd.Load(App.mydocdir + "关键词.kw");
                XmlNode Main = _xd.SelectSingleNode("/Main");
                foreach (XmlNode xdr in Main.ChildNodes)
                {
                    string attr = xdr.Attributes["keyword"].Value;
                    if (Regex.IsMatch(KeyWord, ".*" + attr + ".*"))
                    {
                        res = xdr.InnerText;
                        break;
                    }
                }
            }
            return res;
        }

        private bool CheckLimit(string owner)
        {
            int hasqmc = songCacheStu.FindAll((SongInfo s) => s.owner == owner).Count;
            if (hasqmc + 1 > LimitCount)//+1是模拟本次增加歌曲
            {
                return true;
            }
            return false;
        }
        private enum ErrType
        {
            CanNotFound,
            UnKnow,
            DownLoadFaild
        }
        private void SetErr(SongInfo temp, ErrType type)
        {
            temp.CacheSta = 3;
            int index = GetIndex(temp.id);
            if (index <= songCacheStu.Count && index >= 0)
            {
                songCacheStu[index] = temp;
            }
            WriteList();
            string errstring = "";
            switch (type)
            {
                case ErrType.CanNotFound:
                    errstring = "从曲库中找不到歌曲";
                    break;
                case ErrType.UnKnow:
                    errstring = "出现未知错误(109)";
                    break;
                case ErrType.DownLoadFaild:
                    errstring = "下载失败";
                    break;
                default:
                    errstring = "出现未知错误(100)";
                    break;
            }
            App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = temp.name + "->点歌失败：" + errstring });
            //App.B_eg.AddNewLog(temp.name + "->点歌失败：" + errstring);
            App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]失败：" + errstring + "：" + temp.name });
        }
        private bool CheckRP(string musicname="")
        {
            bool res = true;//默认允许
            Regex _name = new Regex(musicname, RegexOptions.IgnoreCase);
            foreach (SongInfo item in songCacheStu)
            {
                    res = !_name.IsMatch(item.or_name);
                    if (!res)
                    {
                        break;
                    }
                
            }
            return res;
        }
        /// <summary>
        /// 更新用户界面的音乐列表
        /// </summary>
        public void RefreshITFL()
        {
            try
            {
                //App.DoMC.ClearList();
                if (App.DoMC.CheckListSizeLtAccSize(songCacheStu.Count))
                {
                    App.DoMC.CompareAndRemoveMusicItem(songCacheStu);
                }
                if (songCacheStu.Count > 0)
                {
                   
                    for (int i = 0; i < songCacheStu.Count; i++)
                    {
                        string msg = App.Current.FindResource("pstu_ready").ToString();
                        msg = cacheInfoC[songCacheStu[i].CacheSta];
                        if (songCacheStu[i].Play_Stu == 1)
                        {
                            msg = "正在播放";
                        }
                        App.DoMC.ModifyItemToMusicList(songCacheStu[i].name, msg, songCacheStu[i].mode, songCacheStu[i].owner);
                        //App.DoMC.AddItemToMusicList(songCacheStu[i].name, msg, songCacheStu[i].mode, songCacheStu[i].owner);
                    }
                }
                
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("播放异常",e);
            }

        }
        #region 写入文本
        private bool Use_Mod = false;
        private string tpl = "";
        public void ReadModul()
        {
            string mydocdir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\UP主助手\";
            if (!System.IO.Directory.Exists(mydocdir))
            {
                System.IO.Directory.CreateDirectory(mydocdir);
            }
            string path = mydocdir + "obs点歌模板.txt";
            if (!System.IO.File.Exists(path))
            {
                Use_Mod = false;
                return;
            }
            else
            {
                Use_Mod = true;
                tpl = System.IO.File.ReadAllText(path);
            }
        }
        public void WriteList()
        {
            string mydocdir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\UP主助手\";
            if (!System.IO.Directory.Exists(mydocdir))
            {
                System.IO.Directory.CreateDirectory(mydocdir);
            }
            string path = mydocdir + "obs点歌文本.txt";
            try
            {
                using (FileStream fw = System.IO.File.Open(path, FileMode.OpenOrCreate))
                {
                    fw.Seek(0, SeekOrigin.Begin);
                    fw.SetLength(0);
                    fw.Close();
                    FileStream afw = System.IO.File.Open(path, FileMode.OpenOrCreate);
                    string list = GetList();
                    StreamWriter sw = new StreamWriter(afw, System.Text.Encoding.UTF8);
                    if (Use_Mod)
                    {
                        list = tpl.Replace("[列表]", list);
                        list = Regex.Replace(list, @"\{(.*)\}", "");
                        list = list.Replace("[前缀]", spt);
                    }
                    else
                    {
                        sw.Write("点歌方式：" + spt + "+空格+歌名/网易ID");
                        sw.WriteLine();
                        sw.Write("房管切歌请输入"+ adminspt + "+空格+切歌");
                        sw.WriteLine();
                    }
                    //Models.LogHelper.WirteLog(list);
                    sw.Write(list);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Models.LogHelper.WirteLog("写入文本异常", ex);
            }

        }
        #endregion
        /// <summary>
        /// 以文本形式返回歌单
        /// </summary>
        /// <returns></returns>
        public string GetList()
        {
            string rows = "";
            string s_tpl = "[序号].[[引擎]][歌名]->[状态]";
            if (Use_Mod)
            {
                string rule = @"\{(.*)\}";
                if (Regex.IsMatch(tpl, rule))
                {
                    s_tpl = Regex.Match(tpl, rule).Groups[1].Value;
                }
            }
            int count = 0;
            if (songCacheStu.Count == 0)
            {
                rows = App.Current.FindResource("Playend").ToString();
            }
            else
            {
                foreach (SongInfo item in songCacheStu)
                {
                    count++;
                    string sst = cacheInfoC[item.CacheSta];
                    if (item.Play_Stu == 1)
                    {
                        sst = "正在播放";
                    }
                    if (item.Play_Stu == 4)
                    {
                        sst = "播放即将结束";
                    }
                    string row = "";
                    if (count > App.uic.ListRow)
                    {
                        row = App.Current.FindResource("OverLong").ToString();
                        break;
                    }
                    else
                    {
                        char[] _music = "music".ToCharArray();
                        row = s_tpl.Replace("[序号]", count.ToString());
                        row = row.Replace("[歌名]", item.name);
                        row = row.Replace("[歌手]", item.Artis);
                        row = row.Replace("[引擎]", item.mode.TrimEnd(_music));
                        row = row.Replace("[状态]", sst);
                        row = row.Replace("[用户]", item.owner);
                        //row = string.Format("{0}.{1}{2}{3}->{4}", count.ToString(), App.Current.FindResource("SongName").ToString(), "[" + item.mode.TrimEnd(_music) + "]", item.name, sst);
                    }
                    rows += row + "\n";
                }
            }

            return rows;
        }
        /// <summary>
        /// 获取任务的位置
        /// </summary>
        /// <param name="Vid"></param>
        /// <returns></returns>
        public int GetIndex(long Vid)
        {
            //检测下载结果
            int _index = -1;
            try
            {
                for (int i = 0; i < songCacheStu.Count; i++)
                {
                    if (songCacheStu[i].id == Vid)
                    {
                        _index = i;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog(e.Message);
            }
            return _index;
        }
        #region 类型

        public struct SongInfo
        {
            public long id;//任务ID
            public string name;
            public string or_dm;//源弹幕信息
            public string or_name;//源名称，解析前的名称
            public int CacheSta;//0爲隊列1爲下載中2爲下載完3爲下載失敗
            public int Play_Stu;//0未开始1播放中2暂停中3停止中4接近停止
            public RQM.MatchIDs matchIDs;
            public string mode;//记载他的搜歌模式，下载的时候要一致
            public string owner;//谁点的歌
            public string Artis;//歌手名
            public string link;
            public List<string[]> geci;
            public double HoldingTime;//以次数为单位
        }
        public class RQMwork
        {
            public int ID { get; set; }
            public int P_index { get; set; }
        }
        public class VsingItem
        {
            public string collectCnt { get; set; }
            public string composer { get; set; }
            public string createTime { get; set; }
            public string downloadCnt { get; set; }
            public string ext { get; set; }
            public string lyric { get; set; }
            public string nickName { get; set; }
            public string optComposer { get; set; }
            public string originSinger { get; set; }
            public string playCnt { get; set; }
            public string popularityCnt { get; set; }
            public string postProduction { get; set; }
            public string singer { get; set; }
            public string singerId { get; set; }
            public string songId { get; set; }
            public string songName { get; set; }
            public string songSize { get; set; }
            public string songWriter { get; set; }
            public string status { get; set; }
            public string style { get; set; }
            public string type { get; set; }
            public string typeEname { get; set; }
            public string typeName { get; set; }
            public string originalName { get; set; }
            public string songurl { get; set; }
            public string downloadurl { get; set; }
        }

        #endregion

    }
}
