﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Models.NewCloud
{
    interface IUPCSUser
    {
        /// <summary>
        /// 发送弹幕到新的云服务
        /// </summary>
        /// <param name="danmu"></param>
        void SendDMToCloud(CodyDanMu.DanmuInfo danmu);
    }
}
