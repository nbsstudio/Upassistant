﻿using System.Net;

namespace UPassistant.Models.NewCloud
{
    public class CloudConfig
    {
        public IPAddress sIp { get; set; }
        public string heartBeat { get; set; }
    }
}
