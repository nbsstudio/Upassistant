﻿using System;
using CodyDanMu;

namespace UPassistant.Models.NewCloud
{
    class UpcsMessageClient : IUpcsMessageClient
    {
        JsonHelper jsonHelper = new JsonHelper();
        public string Parse(string msgInfo)
        {
            MessageEntity me = new MessageEntity();
            me.content = msgInfo;
            ExchangeMsg(me);
            return jsonHelper.SerializeObject(me);
        }

        public string Parse(DanmuInfo msgInfo)
        {
            MessageEntity me = new MessageEntity();
            me.content = jsonHelper.SerializeObject(msgInfo);
            ExchangeMsg(me);
            return jsonHelper.SerializeObject(me);
        }

        public MessageEntity Decompress(string json)
        {
            return jsonHelper.DeserializeJsonToObject<MessageEntity>(json);
        }

        private void ExchangeMsg(MessageEntity me)
        {
            try
            {
                me.rdate = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                me.rtime = int.Parse(DateTime.Now.ToString("HHmmss"));
                me.accessId = App.nkc.AccessId;
            }
            catch (Exception ex)
            {
                LogHelper.getLogger().Info(ex);
            }
        }
    }
    //@MessageReader.Class<2>
    public class MessageEntity
    {
        public int bid { get; set; }
        public string content { get; set; }
        public int rdate { get; set; }
        public int rtime { get; set; }
        public string accessId { get; set; }
        public int level { get; set; } 
        public string attr { get; set; }
        public string encrypt { get; set; }
        public string key { get; set; } 
    }
}
