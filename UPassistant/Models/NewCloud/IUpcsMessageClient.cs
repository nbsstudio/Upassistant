﻿using CodyDanMu;

namespace UPassistant.Models.NewCloud
{
    interface IUpcsMessageClient
    {
        string Parse(DanmuInfo msgInfo);
        string Parse(string msgInfo);
        MessageEntity Decompress(string json);
    }
}
