﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace UPassistant.Models.NewCloud
{
    public class UpcsClient
    {
        public bool IsConnected { get; set; }
        private string heartBeatStr = "NACGAME";
        private Socket upcs;
        private IUpcsMessageClient umc;
        private IPEndPoint cloudIP = new IPEndPoint(IPAddress.Parse("115.28.222.234"), 2333);
        private byte[] data = new byte[2048];
        private System.Timers.Timer heartBeat;
        private int missTime = 0;
        private int retryCount = 0;
        #region 事件委托
        public event EventHandler Socket_Connected;
        public event EventHandler Socket_Disconnected;
        #endregion
        private UpcsClient()
        {
            init();
            //cloudIP = new IPEndPoint(NNS.getEndIp(), 2333);
        }
        private void init()
        {
            upcs = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            umc = new UpcsMessageClient();
            heartBeat = new System.Timers.Timer();
            heartBeat.Elapsed += HeartBeat_Elapsed;
            heartBeat.Interval = 15000;
        }
        public UpcsClient(CloudConfig cfg) : this()
        {
            try
            {
                cloudIP = cfg != null ? new IPEndPoint(cfg.sIp, 2333) : new IPEndPoint(NNS.getEndIp(), 2333);
                heartBeatStr = cfg.heartBeat == null ? heartBeatStr : cfg.heartBeat;
            }
            catch (System.Exception ex)
            {
                LogHelper.getLogger().Info("云平台异常", ex);
            }
        }

        /// <summary>
        /// 连接
        /// </summary>
        /// <returns></returns>
        public bool Connect()
        {
            upcs = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            upcs.ReceiveTimeout = 100000;
            upcs.SendTimeout = 100000;
            try
            {
                upcs.BeginConnect(cloudIP, Connect_CallBack, null);
            }
            catch (SocketException ex)
            {
                LogHelper.getLogger().Info("云平台异常", ex);
                SendDisconnected();
                return false;
            }
            catch (Exception ex)
            {
                LogHelper.getLogger().Info("云平台异常", ex);
                SendDisconnected();
                return false;
            }
            return true;
        }

        private void SendDisconnected()
        {
            IsConnected = false;
            Socket_Disconnected?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// 硬重连
        /// </summary>
        public void HardReconnect()
        {
            if (retryCount < 10)
            {
                if (IsConnected)
                {
                    Disconnect();
                }

                init();
                Connect();
                retryCount++;
            }
        }

        /// <summary>
        /// 关闭
        /// </summary>
        public void Disconnect()
        {
            try
            {
                lock (this)
                {
                    heartBeat.Stop();
                    //upcs.Shutdown(SocketShutdown.Both);
                    upcs.Close();
                    SendDisconnected();
                    upcs = null;
                }
            }
            catch (Exception ex)
            {
                LogHelper.getLogger().Info("云平台异常", ex);
            }
        }
        /// <summary>
        /// 发送
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool Send(string msg, bool useUTF8 = false)
        {
            try
            {
                if (upcs.Connected)
                {
                    byte[] data = null;
                    if (useUTF8)
                    {
                        data = Encoding.UTF8.GetBytes(msg);
                    }
                    else
                    {
                        data = Encoding.ASCII.GetBytes(msg);
                    }
                    int msglen = msg.Length;
                    int len = 0;
                    int retc = 3;
                    do
                    {
                        len = upcs.Send(data);
                        if (--retc < 0)
                        {
                            retc = 3;
                            break;
                        }
                    } while (len < data.Length);
                }
            }
            catch (Exception ex)
            {
                Disconnect();
                Connect();
                LogHelper.getLogger().Info("云平台异常", ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="message">要么发送文本，要么发送一个CodyDanMu.danmu_info的实体类</param>
        /// <returns></returns>
        public bool SendMessage(object message)
        {
            string msg = "";
            if (message.GetType() == typeof(string))
            {
                msg = umc.Parse((string)message);
            }
            else if (message.GetType() == typeof(CodyDanMu.DanmuInfo))
            {
                msg = umc.Parse((CodyDanMu.DanmuInfo)message);
            }
            return msg == "" ? false : Send(msg, true);
        }

        private void Connect_CallBack(IAsyncResult result)
        {
            try
            {
                upcs.EndConnect(result);
                if (null != upcs)
                {
                    heartBeat.Start();
                    upcs.BeginReceive(data, 0, data.Length, SocketFlags.None, Receive_CallBack, null);
                    SendFirstHeartBeat();
                }
                IsConnected = true;
                missTime = 0;
                Socket_Connected?.Invoke(this, new EventArgs());
            }
            catch (SocketException)
            {
                SendDisconnected();
            }
            catch (Exception e)
            {
                LogHelper.getLogger().Info("云平台连接异常", e);
            }

        }

        private void SendFirstHeartBeat()
        {
            //第一个心跳
            Send(heartBeatStr);
        }

        private void Receive_CallBack(IAsyncResult result)
        {
            try
            {

                int bytesRead = upcs.EndReceive(result);
                if (bytesRead > 0)
                {
                    string remsg = Encoding.UTF8.GetString(data);
                    if (remsg == heartBeatStr)
                    {
                        missTime = 0;
                        //umc.Decompress(remsg);//接收与解析
                    }
                    else
                    {
                        Console.WriteLine(remsg);
                    }
                    data = new byte[2048];
                    upcs.BeginReceive(data, 0, data.Length, SocketFlags.None, Receive_CallBack, null);
                }
                else
                {
                    SendDisconnected();
                }

            }
            catch (SocketException)
            {
                SendDisconnected();
            }
            catch (System.IO.IOException)
            {
                SendDisconnected();
            }
            catch (Exception ex)
            {
                LogHelper.getLogger().Info("云平台异常", ex);
            }
        }

        private void HeartBeat_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!Send(heartBeatStr))
            {
                SendDisconnected();
                if (missTime > 3)
                {
                    //Console.WriteLine("丢失心跳，重连");
                    HardReconnect();
                }
                missTime++;
            }
        }
    }
}
