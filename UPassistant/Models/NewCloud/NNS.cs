﻿using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace UPassistant.Models.NewCloud
{
    public class NNS
    {
        public static IPAddress getEndIp()
        {
            string[] NS = { "nacgame.com" };

            foreach (var item in NS)
            {
                IPHostEntry iphost = null;
                try
                {
                    iphost = Dns.GetHostEntry(item);   //解析主机
                }
                catch (System.Exception)
                {
                    throw;
                }
                
                if (iphost != null && iphost.AddressList != null && iphost.AddressList.Length > 0)
                {
                    bool isPass = false;
                    try
                    {
                        isPass = checkAlive(iphost.AddressList[0].ToString());
                    }
                    catch (System.Exception)
                    {
                        try
                        {
                            isPass = checkIp(iphost.AddressList[0].ToString());
                        }
                        catch (System.Exception)
                        {
                            throw new System.Exception("UPCS Is Not Available!");
                        } 
                    }
                    if (isPass)
                    {
                        return iphost.AddressList[0];
                    }
                }
            }
            return IPAddress.Parse("115.28.222.234");
        }
        private static bool checkAlive(string ipStr)
        {
            string mc = "mc=upas_" + System.Guid.NewGuid();
            string res = new Netios.NetHTTP().pub_doPOST("http://"+ipStr+ "/alive.html", mc, Encoding.UTF8);
            if (res!=null && res=="200.OK")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool checkIp(string ipStr)
        {
            //构造Ping实例  
            Ping pingSender = new Ping();
            //Ping 选项设置  
            PingOptions options = new PingOptions();
            options.DontFragment = true;
            //测试数据  
            byte[] buffer = Encoding.ASCII.GetBytes("test data abcabc");
            //设置超时时间  
            int timeout = 120;
            //调用同步 send 方法发送数据,将返回结果保存至PingReply实例  
            PingReply reply = pingSender.Send(ipStr, timeout, buffer, options);
            if (reply.Status == IPStatus.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
