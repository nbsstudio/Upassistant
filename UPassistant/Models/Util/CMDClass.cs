﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace UPassistant.Models
{
    /// <summary>
    /// 弹幕命令行解析
    /// </summary>
    public abstract class CMDClass
    {
        /// <summary>
        /// 针对点歌
        /// </summary>
        public class RQM_CMD
        {
            public RQM_CMD(char spc = ' ', string spt = "233",string adminspt = "666")
            {
                ex = new Regex("^" + spt + spc + "(.*)?");
                adminex = new Regex("^" + adminspt + spc + "切歌.*?");
                admindl = new Regex("^" + adminspt + spc + "删歌(.*)?");
                adminrb = new Regex("^" + adminspt + spc + "开车?");
                cancel = new Regex("^取消点歌.*?");
            }
            private Regex ex;
            private Regex adminex;
            private Regex cancel;
            private Regex admindl;
            private Regex adminrb;

            public Ptr GetFuncCode(string text)
            {
                if (cancel.IsMatch(text))
                {
                    return Ptr.CancelMusic;
                }
               else if (ex.IsMatch(text))
                {
                    return Ptr.RequestMusic;
                }
                else if(adminex.IsMatch(text))
                {
                    return Ptr.AdminCutMusic;
                }
                else if (admindl.IsMatch(text))
                {
                    return Ptr.AdminDeleteMusic;
                }
                else if (adminrb.IsMatch(text))
                {
                    return Ptr.AdminCallRobot;
                }
                else
                {
                    return Ptr.None;
                }
            }
            public string GetMatchValueOne(Ptr ExPtr,string text)
            {
                string TotalName=string.Empty;
                if (ExPtr == Ptr.RequestMusic)
                {
                     TotalName = ex.Match(text).Groups[1].Value;
                }
                if (ExPtr == Ptr.AdminDeleteMusic)
                {
                    TotalName = admindl.Match(text).Groups[1].Value;
                }
                return TotalName;
            }
            public enum Ptr
            {
                RequestMusic,
                AdminCutMusic,
                CancelMusic,
                AdminDeleteMusic,
                AdminCallRobot,
                None
            }
        }
        /// <summary>
        /// 针对行为
        /// </summary>
        public class Action
        {
            public  void GetFuncCode()
            {

            }
        }
    }
}
