﻿using log4net.Util;
using System;
using System.IO;
using System.Text;

namespace UPassistant.Models
{
    class LogHelper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("log");
        private static string hh = "错误已记录，请有空反馈到QQ：294426436\r";
        static LogHelper()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"\exlog.config";
            log4net.Config.XmlConfigurator.Configure(new FileInfo(path));
        }
        public static log4net.ILog getLogger()
        {
            return log;
        }
        public static void WirteLog(string msg)
        {
            if (log.IsInfoEnabled)
            {
                log.Info(msg);
            }
        }
        public static void WirteLog(string msg,Exception se)
        {
            if (log.IsInfoEnabled)
            {
                log.Info(msg,se);
            }
        }
        public static void WirteLog(string path ,string msg)
        {
            System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"错误.log", hh + path + "\r"+ msg + "\r");
        }
        /// <summary>
        /// 透过读取记录数据诊断程序是否正常退出
        /// </summary>
        /// <returns></returns>
       public static bool diagnoseIsShutdownNormally()
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"错误.log"))
            {
                try
                {
                    string[] lines = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + @"错误.log", Encoding.Default);
                    if (lines[lines.Length - 1].IndexOf("[LogEnd]") < 0)
                    {
                        if (lines[lines.Length - 2].IndexOf("正常退出") < 0 || lines[lines.Length - 2].IndexOf("[LogEnd]") < 0)
                        {
                            return false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    WirteLog("启动BUG反馈器异常",ex);
                }
            }
           
            return true;
        }
    }
}
