﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace UPassistant.Models.Util
{
    class NewCodyLink
    {
        public static string DoInterface(string cmd)
        {
            SecureString secureString = new SecureString();
            foreach (var item in cmd)
            {
                secureString.AppendChar(item);
            }
            IntPtr cmdPtr = Marshal.SecureStringToGlobalAllocAnsi(secureString);
            IntPtr resPtr = Interface(cmdPtr);
            return Marshal.PtrToStringAnsi(resPtr);
        }
        [DllImport("NewCodyImpl.dll", CharSet = CharSet.Ansi,CallingConvention =CallingConvention.Cdecl)]
        public extern static IntPtr Interface(IntPtr cmd);
    }
}
