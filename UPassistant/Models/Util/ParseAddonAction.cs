﻿using System;
using System.Threading;
using NAudio.Wave;
using System.Threading.Tasks;

namespace UPassistant.Models
{
    class ParseAddonAction
    {
        string NowFunction;
        string NowAddonName;
        private bool NowActionIsAdmin;
        public bool Parse(string Exp,string name)
        {
            string[] Expa = Exp.Split('.');
            if (Expa.Length < 2)
            {
                return false;
            }
            NowFunction = Expa[0];
            NowAddonName = name;
            NowActionIsAdmin = false;
            if (Expa.Length > 2)
            {
                string args = "";
                for (int i = 2; i < Expa.Length; i++)
                {
                    args += "." + Expa[i];
                }
                if (Expa[1].IndexOf("M_") == 0)
                {
                    return Mparse(Expa[1]);
                }
                else
                {

                    GParse(Expa[1], args);
                }
            }
            else
            {
                if (Expa[1].IndexOf("M_") == 0)
                {
                    return Mparse(Expa[1]);
                }
                else
                {
                    GParse(Expa[1], "");
                }
            }
            return true;
        }
        #region Addon Behavior Language解析链
        /// <summary>
        /// 主要部分解析
        /// </summary>
        private bool GParse(string FunctionName, string args)
        {
            string FN = FunctionName;
            if (FunctionName.IndexOf("playsound:") == 0)
            {
                try
                {
                    string filename = FunctionName.TrimStart("playsound:".ToCharArray()) + args;
                    Player sp = new Player();
                    //SoundPlayer sp = new SoundPlayer();
                    if (NowFunction != "main")
                    {
                        sp.callBack = NowFunction;
                    }
                    sp.GetSoundToPlayer(filename);
                }
                catch (Exception)
                {

                }
            }
            if (FunctionName.IndexOf("Admin") > -1)
            {
                FN = FunctionName.Remove(0, 6).Trim();
                NowActionIsAdmin = true;
                bool cam = App.addonsController.ThisAddonCanUseInAdminMode(NowFunction);
                if (!cam)
                {
                    //不够权限
                    SendAdminModeErr();
                    return false;
                }

            }
            switch (FN)
            {
                case "oninit;":
                    Thread init = new Thread(Oninit);
                    init.Start();
                    return true;
                case "ondminit;":
                    Thread exdm_fc = new Thread(OnDMinit);
                    exdm_fc.Start();
                    return true;
                case "onswitch(true);":
                    Thread sw_on = new Thread(Switch);
                    sw_on.Start(true);
                    return true;
                case "onswitch(false);":
                    Thread sw_off = new Thread(Switch);
                    sw_off.Start(false);
                    return true;
                case "load_fanscount;":
                    Thread ld_fc = new Thread(new ThreadStart(LoadFansCount));
                    ld_fc.Start();
                    return true;
                case "load_roompeoplecount;":
                    LoadRPCount();
                    return true;
                case "main_ChangeMusic;":
                    ChangeMusic();
                    return true;
                case "main_PauseMusic;":
                    PauseMusic();
                    return true;
                case "main_ClearMusic;":
                    ClearMusic();
                    return true;
                case "main_FlushOBSTextA;":
                    FlushOBSText(false);
                    return true;
                case "main_FlushOBSTextP;":
                    //留以后有OBS插件时用
                    FlushOBSText(true);
                    return true;
                case "obs_Start;":
                    StartOBS();//后台启动的obs必须用户设置过路径才可以正常使用，这不是BUG
                    return true;
                case "config_update_LimitCount;":
                    bool res = App.rqmc.SaveRQMLimitCount();
                    return res;
                case "main_Hidden;":
                    if (NowActionIsAdmin)
                    {
                        HiddenWindow();
                    }
                    return true;
                default:
                    return Gparse2(FN, args);
            }
        }
        /// <summary>
        /// 网络部分解析
        /// </summary>
        private bool Gparse2(string cmd, string args)
        {
            Network nt = new Network(NowAddonName);
            switch (cmd)
            {
                case "cloud_send_msg":
                    nt.SendMessage(args);
                    return true;
                default:
                    return Gparse3(cmd,args);
            }
        }
        /// <summary>
        /// 系统底层部分解析
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private bool Gparse3(string cmd,string args)
        {
            switch (cmd)
            {
                case "change_dm_mode;":
                    App.uic.LoadDMCMode();
                    break;
                case "main_Shutdown;":
                    Environment.Exit(0);
                    break;
                case "null":
                    return false;
                default:
                    return false;
            }
            //能匹配到命令的都不直接返回，需要先记录行为再返回
            LogHelper.WirteLog("插件：" + NowAddonName + "，操作：" + cmd);
            return true;
        }
        /// <summary>
        /// 新的自动解析运行脚本
        /// TODO 预计1.1.0.1版本后停用Gparse
        /// </summary>
        /// <param name="FN"></param>
        /// <returns></returns>
        private bool Mparse(string FN)
        {
            try
            {
                Task.Run(()=> {
                    object instance = Activator.CreateInstance(GetType());
                    GetType().GetMethod(FN).Invoke(instance, null);
                });
                return true;
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("插件CALL M信息错误：", e);
                return false;
            }
        }
        #endregion
        /// <summary>
        /// 插件初始化时
        /// </summary>
        private void Oninit()
        {
            switch (NowFunction)
            {
                case "bot":
                    PubConf.TTS_Enable = true;
                    App.DoMC.BOT_Trun_ON_or_OFF();//关闭机器人
                    PubConf.TTS_Switch = false;
                    break;
                default:

                    break;
            }
        }
        private void OnDMinit()
        {
            bool IsEXAD = App.addonsController.FindoutAddon(NowFunction);
            if (IsEXAD)
            {
                App.addonsController.dmSkinName = NowFunction;
                App.DoMC.TurnOffOSD();
            }
        }
        /// <summary>
        /// 开关变化时
        /// </summary>
        /// <param name="bl"></param>
        private void Switch(object bl)
        {
            switch (NowFunction)
            {
                case "bot":
                    PubConf.TTS_Switch = (bool)bl;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 读取粉丝关注数目
        /// </summary>
        private void LoadFansCount()
        {
            if (NowFunction == "main")
            {
                int fc = App.B_eg.GetRoomFans();
                App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext= "[房间]粉丝关注数" + fc + ";" });
            }
        }
        /// <summary>
        /// 读取粉丝关注数目
        /// </summary>
        private void LoadRPCount()
        {
            if (NowFunction == "main")
            {
                int prc = App.B_eg.RoomPeopleCount;
                App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[房间]房间人数:" + prc + ";" });
            }
        }
        /// <summary>
        /// 插件切歌
        /// </summary>
        private void ChangeMusic()
        {
            App.musicPlayer.DoStop();
        }
        /// <summary>
        /// 插件暂停或播放歌曲
        /// </summary>
        private void PauseMusic()
        {
            App.musicPlayer.PlayOrPause();
        }
        private void ClearMusic()
        {
            if (App.musicPlayer.wavePlayer != null)
            {
                App.RQM.addWork(2);
                App.musicPlayer.DoStop();
            }
        }
        private void FlushOBSText(bool IsPlugin)
        {
                if (IsPlugin)
                {
                    //留个位置
                    return;
                }
                App.RQM.WriteList();
        }
        private void StartOBS()
        {
            if (System.IO.File.Exists(App.uic.OBSPath))
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = new System.Diagnostics.ProcessStartInfo(App.uic.OBSPath);
                p.Start();
            }
            else
            {
                App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = App.Current.FindResource("Non-Set:obs_path").ToString() });
                //App.B_eg.AddNewLog(App.Current.FindResource("Non-Set:obs_path").ToString());
            }
        }
        public void SendAdminModeErr()
        {
            App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = App.Current.FindResource("cannotuseinadmin").ToString() });
            App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = App.Current.FindResource("erroraddon").ToString() + NowFunction });
            //App.B_eg.AddNewLog(App.Current.FindResource("cannotuseinadmin").ToString());
            //App.B_eg.AddNewLog(App.Current.FindResource("erroraddon").ToString() + NowFunction);
        }
        public void HiddenWindow()
        {
            //最小化窗口
            App.DoMC.WindowState = System.Windows.WindowState.Minimized;
        }
    }
    class Player : SoundPlayer
    {
        public Player()
        {
        }
        public override void GetSoundToPlayer(string File)
        {
            VolumeChange((float)App.SystemSoundVol / 100);//由于这个是动态调用所以必须每次设定音量
            base.GetSoundToPlayer(File);
        }
        public override void wavePlayer_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            base.wavePlayer_PlaybackStopped(sender, e);
            if (callBack != "")
            {
                App.addonsController.SendCommand(callBack, "[回调]声音播放完毕;");
            }
        }
    }
    public class Network
    {
        string Name;
        public Network(string name)
        {
            Name = name;
        }
        public void SendMessage(string args)
        {
            //限制一、照出4000字节的文本都不能发送
            if (System.Text.Encoding.UTF8.GetByteCount(args) >4000)
            {
                return;
            }
        }
    }
}
