﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Models.Util
{
    /// <summary>
    /// 特殊日，节假日特殊程序
    /// </summary>
    class SpecailDay
    {
        private int nowDate;

        public SpecailDay()
        {
            DateTime ndd = DateTime.Now;
            nowDate = int.Parse(String.Format("{0:yyyyMMdd}", ndd));
            LogHelper.WirteLog(nowDate.ToString());
        }

        public string runDayCheckAndGetMsg()
        {
            if (nowDate >= 20180216 && nowDate <= 20180223)
            {
                return "UP主助手祝你2018狗年大吉，关注助手群有惊喜哦！群号：81637550";
            }
            return null;
        }


    }
}
