﻿using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UPassistant.Models.Util
{
    class APlayerLink
    {
        private static Socket Server;
        private static APlayerLink apl;
        private IPEndPoint thisIP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 23334);
        private IPEndPoint sendIP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 23333);


        APlayerLink()
        {
            try
            {
                if (Server == null)
                {
                    Server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    Server.Bind(thisIP);
                }
                App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo {  nickname="系统",logtext= "内置放映厅互联器已启动" });
            }
            catch (System.Exception e)
            {
                LogHelper.WirteLog("启动放映厅失败", e);
            }
            
        }

        public static APlayerLink GetInstance()
        {
            if (apl==null)
            {
                apl = new APlayerLink();
            }
            return apl;
        }

        public void SendDMToAPlayer(CodyDanMu.DanmuInfo danmu)
        {
            try
            {
                if (Server != null)
                {
                    bool isadmin = danmu.admin == 1 ? true : false;
                    bool isly = danmu.vip == 1 ? true : false;
                    DM send = new DM { User = danmu.nickname == "道具娘" ? "系统：道具娘" : danmu.nickname, Uid = danmu.bilibiliuid, Msg = danmu.logtext, IsAdmin = isadmin, IsLaoYe = isly, RoomId = danmu.roomid };
                    string sendStr = JsonConvert.SerializeObject(send);
                    Server.SendTo(Encoding.UTF8.GetBytes(sendStr), sendIP);
                }
            }
            catch (System.Exception e)
            {
                LogHelper.WirteLog("发送放映厅联动失败", e);
            }
            
        }


    }
    public class DM
    {
        /// <summary>
        /// 用户
        /// </summary>
        public string User { get; set; }
        /// <summary>
        /// B站ID
        /// </summary>
        public string Uid { get; set; }
        /// <summary>
        /// 弹幕信息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 是否房管
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 是否老爷
        /// </summary>
        public bool IsLaoYe { get; set; }
        /// <summary>
        /// 房间号
        /// </summary>
        public string RoomId { get; set; }
    }
}
