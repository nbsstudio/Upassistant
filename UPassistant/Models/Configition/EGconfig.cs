﻿namespace UPassistant.Models
{
    public class EGconfig : IniHelper
    {
        public EGconfig()
        {
            EnableAPlayerLink = true;
        }
        public override void Load()
        {
            int fbw2;
            if (int.TryParse(ReadIniKeys("user", "ForbiddenWay2", path + "conf.ini"), out fbw2))
            {
                App.B_eg.forbidden_way2 = fbw2 == 1 ? true : false;
            }
            else
            {
                App.B_eg.forbidden_way2 = false;
            }
            App.DoMC.startBtn.SetRoomID(RoomID = ReadIniKeys("user", "room", path + "conf.ini"));
            if (ReadIniKeys("user", "EnableAPlayerLink", path + "conf.ini") == "0" && EnableAPlayerLink)
            {
                EnableAPlayerLink = false;
            }
            App.engc.IsAutoCalcScreenClearTimes = ReadIniKeys("user", "IsAutoCalcScreenClearTimes", path + "conf.ini")=="1";
            try
            {
                App.engc.LastTimeUserGotDMCount = int.Parse(ReadIniKeys("user", "accessid", path + "conf.ini"));
            }
            catch (System.Exception)
            {
                App.engc.LastTimeUserGotDMCount = App.DoMC.crv;
            }
            SupportOldAddons = false;
            ThisLoaded = true;
        }

        /// <summary>
        /// 只读取FR模式的值（该选项只针对部分兼容的组件或插件生效，所以不推荐加载到内存里）
        /// </summary>
        /// <returns></returns>
        public bool LoadFRModel()
        {
            return ReadIniKeys("user", "FRModel", path + "conf.ini") == bool.TrueString;
        }
        public override void Save(string[] Args)
        {
            WriteIniKeys("user", "Wasapi", App.audioOutPut.ToString(), path + "conf.ini");
            WriteIniKeys("user", "ForbiddenWay2", App.B_eg.forbidden_way2 ? "1" : "0", path + "conf.ini");
            WriteIniKeys("user", "room", RoomID, path + "conf.ini");
            WriteIniKeys("user", "FRModel", Args[0], path + "conf.ini");
            CalcAndSave();
            BackupSetting();
        }
        public void SaveRoomId()
        {
            WriteIniKeys("user", "room", RoomID, path + "conf.ini");
        }

        public void CalcAndSave()
        {
            //保存清屏行数
            WriteIniKeys("user", "IsAutoCalcScreenClearTimes", App.engc.IsAutoCalcScreenClearTimes ? "1" : "0", path + "conf.ini");
            //WriteIniKeys("user", "LastTimeUserGotDMCount", App.engc.LastTimeUserGotDMCount.ToString(), path + "conf.ini");
            if (App.engc.IsAutoCalcScreenClearTimes)
            {
                //计算
                int rowCount = 300;
                float rate = 0.4F;
                if (App.engc.LastTimeUserGotDMCount >= 20000)
                {
                    rate = 0.5F;
                }
                if (App.engc.LastTimeUserGotDMCount >= 40000)
                {
                    rate = 0.6F;
                }
                if (App.engc.LastTimeUserGotDMCount>1000)
                {
                    rowCount = (int)(App.engc.LastTimeUserGotDMCount * rate);
                }
                WriteIniKeys("user", "RowsToClear", rowCount.ToString(), path + "conf.ini");
            }
        }
        #region 内存中的配置
        public bool SupportOldAddons { get; set; } //是否支持旧插件的弹幕发送
        public string RoomID { get; set; }
        public bool EnableAPlayerLink { get; set; }
        public override bool ThisLoaded { get; set; }

        #endregion
    }

}
