﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using UPassistant.UI;
using UPassistant.ViewModels;

namespace UPassistant.Models
{
    public class UIconfig : IniHelper
    {
        public UIconfig()
        {
            UserNameMap = new ObservableCollection<Entities.UserNameMapConfigEntity>();
            DMStyle = new ObservableCollection<Entities.DMStyleEntity> { new Entities.DMStyleEntity { ID = "DEFULT", IsCurrentUsed = true, Name = "默认" , Dir= "Skin" } };
            NewDMOverLoadOpt = 20;
            UseWellcomBoard = false;
        }
        public override void Load()
        {
            LoadDMCMode();
            LoadFont();
            int.TryParse(ReadIniKeys("user", "DMFullRows", path + "conf.ini"), out App.uic.TotalRow);
            int disableRQM = 0;
            int.TryParse(ReadIniKeys("system", "DisableRQM", path + "conf.ini"), out disableRQM);
            _DisableRQM = disableRQM == 1;
            int xxtime = 5;
            bool xst = int.TryParse(ReadIniKeys("user", "dm_staytime", path + "conf.ini"), out xxtime);
            FadeTime = xxtime;
            if (xxtime == 0 || !xst)
            {
                FadeTime = 5;
                //App.osd.SetXSTime(5);
            }
            double osd_x = 0;
            double osd_y = 0;
            double.TryParse(ReadIniKeys("user", "dm_position_x", path + "conf.ini"), out osd_x);
            double.TryParse(ReadIniKeys("user", "dm_position_y", path + "conf.ini"), out osd_y);
            OsdPosition.X = osd_x;
            OsdPosition.Y = osd_y;
            if (TotalRow < 3)
            {
                App.uic.TotalRow = 3;
                WriteIniKeys("user", "DMFullRows", "3", path + "conf.ini");
            }
            App.OsdShow = ReadIniKeys("user", "dm_show", path + "conf.ini") == "true";
            App.RQM.spt = ReadIniKeys("user", "spt_input", path + "conf.ini");
            OsdBgColor = ReadIniKeys("user", "BgColour", path + "conf.ini");
            try
            {
                App.engc.UseTray = int.Parse(ReadIniKeys("user", "TrayOn", path + "conf.ini")) == 1 ? true : false;
            }
            catch (Exception)
            {
                App.engc.UseTray = false;
            }
            int.TryParse(ReadIniKeys("user", "FullRows", path + "conf.ini"), out ListRow);
            App.uic.ListRow = App.uic.ListRow < 3 ? 3 : App.uic.ListRow;
            int.TryParse(ReadIniKeys("user", "RowsToClear", path + "conf.ini"), out App.DoMC.crv);
            int tfs;
            if (int.TryParse(ReadIniKeys("user", "FontSize", path + "conf.ini"), out tfs))
            {
                App.uic.DMFontSize = tfs < 6 ? 6 : tfs;
            }
            else
            {
                App.uic.DMFontSize = 12;
            }
            LoadMainRect();
            try
            {
                string userNameMap = ReadIniKeys("user", "UserNameMap", path + "conf.ini");
                if (userNameMap != null && userNameMap.Length > 0)
                {
                    UserNameMap = new JsonHelper().DeserializeJsonToObject<ObservableCollection<Entities.UserNameMapConfigEntity>>(ReadIniKeys("user", "UserNameMap", path + "conf.ini"));
                }
            }
            catch (Exception e)
            {
                UserNameMap = new ObservableCollection<Entities.UserNameMapConfigEntity>();
                LogHelper.WirteLog(e.Message, e);
            }
            DMSort = ReadIniKeys("user", "DMSort", path + "conf.ini");
            OBSPath = ReadIniKeys("user", "obs_path", path + "conf.ini");
            double tmpopt = 0;
            TextOPT = double.TryParse(ReadIniKeys("user", "TextOPT", path + "conf.ini"), out tmpopt) ? tmpopt : 1;
            DontNag = ReadIniKeys("user", "DontNag", path + "conf.ini").ToLower()=="true";
            //读取新弹幕样式
            try
            {
                string json = File.ReadAllText(@".\" + FindCurrentSkinDir() + @"\ndm.tjson");
                this.NewDDMTheme = new JsonHelper().DeserializeJsonToObject<ThemeInfo>(json);
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("读取样式错误", e);
            }
            FindAvaliableSkin();
            LoadDMStyleSetting();
            ThisLoaded = true;
        }

        public string FindCurrentSkinDir()
        {
            string dir = "";
            foreach (var item in DMStyle)
            {
                if (item.IsCurrentUsed)
                {
                    dir = item.Dir;
                }
            }

            return dir;
        }

        public void LoadMainRect()
        {
            //读取主窗口位置信息
            Int32Rect xywh = new Int32Rect(130, 130, 700, 556);
            try
            {
                xywh.X = int.Parse(ReadIniKeys("user", "form_x", path + "conf.ini"));
                xywh.Y = int.Parse(ReadIniKeys("user", "form_y", path + "conf.ini"));
                xywh.Width = int.Parse(ReadIniKeys("user", "form_w", path + "conf.ini"));
                xywh.Height = int.Parse(ReadIniKeys("user", "form_h", path + "conf.ini"));
                MainWindowXYWH = xywh;

                //读取主窗口背景信息
                MainWindowBackground = ReadIniKeys("user", "background", path + "conf.ini");
            }
            catch (Exception)
            {
            }
        }

        public void LoadDMCMode()
        {
            int tmpInt = 0;
            int.TryParse(ReadIniKeys("user", "changeDMtoMusicList", path + "conf.ini"), out tmpInt);
            OsdType = (ViewModels.DMTypeEnum)tmpInt;
        }

        public string LoadFontColour()
        {
            return ReadIniKeys("user", "FontColour", path + "conf.ini");
        }

        public bool LoadAutoClearSetting()
        {
            return ReadIniKeys("user", "AutoClear", AppDomain.CurrentDomain.BaseDirectory + "conf.ini") == "1";
        }
        public override void Save(string[] Args)
        {
            WriteIniKeys("user", "Font", Args[0], path + "conf.ini");
            WriteIniKeys("user", "RowsToClear", Args[1], path + "conf.ini");
            WriteIniKeys("user", "DMFullRows", Args[2], path + "conf.ini");
            WriteIniKeys("user", "FullRows", Args[3], path + "conf.ini");
            WriteIniKeys("user", "changeDMtoMusicList", ((int)OsdType).ToString(), path + "conf.ini");
            WriteIniKeys("user", "AutoClear", App.osd.AutoClear == true ? "1" : "0", path + "conf.ini");
            WriteIniKeys("user", "FontColour", App.osd.GetFontColourString(), path + "conf.ini");
            WriteIniKeys("user", "BgColour", App.osd.GetBgColourString(), path + "conf.ini");
            WriteIniKeys("user", "TextOPT", App.osd.Textopt.ToString(), path + "conf.ini");
            WriteIniKeys("user", "FontSize", DMFontSize.ToString(), path + "conf.ini");
            WriteIniKeys("user", "TrayOn", App.engc.UseTray?"1":"0", path + "conf.ini");
            WriteIniKeys("user", "dm_staytime", App.osd.GetFadeTime().ToString(), path + "conf.ini");
            WriteIniKeys("user", "DMSort", DMSort, path + "conf.ini");
            WriteIniKeys("user", "obs_path", OBSPath, path + "conf.ini");
            WriteIniKeys("user", "DontNag", DontNag.ToString(), path + "conf.ini");
            BackupSetting();
        }

        public void SaveUserNameMapToIni()
        {
            string json = new JsonHelper().SerializeObject(UserNameMap);
            WriteIniKeys("user", "UserNameMap", json, path + "conf.ini");
            BackupSetting();
        }

        public void SaveDMStyleSelection(string id)
        {
            WriteIniKeys("user", "DMStyle", id, path + "conf.ini");
            BackupSetting();
        }

        public void SaveMainWindowSetting()
        {
            WriteIniKeys("user", "room", App.DoMC.startBtn.GetRoomID(), path + "conf.ini");
            double volume = App.musicPlayer.SetVolume * 100;
            WriteIniKeys("user", "SetVolume", volume.ToString(), path + "conf.ini");
            WriteIniKeys("user", "dm_position_y", App.osd.Top.ToString(), path + "conf.ini");
            WriteIniKeys("user", "dm_position_x", App.osd.Left.ToString(), path + "conf.ini");
            WriteIniKeys("user", "dm_show", App.osd.IsVisible.ToString(), path + "conf.ini");
            WriteIniKeys("user", "bot_name", Fade.BOT.BName, path + "conf.ini");
            WriteIniKeys("user", "form_x", App.DoMC.Left.ToString(), path + "conf.ini");
            WriteIniKeys("user", "form_y", App.DoMC.Top.ToString(), path + "conf.ini");
            WriteIniKeys("user", "form_w", App.DoMC.Width.ToString(), path + "conf.ini");
            WriteIniKeys("user", "form_h", App.DoMC.Height.ToString(), path + "conf.ini");
            WriteIniKeys("user", "LastTimeUserGotDMCount", App.dmCount.ToString(), path + "conf.ini");//记录本次使用总共收到多少弹幕
            WriteIniKeys("user", "background", MainWindowBackground, path + "conf.ini");
            BackupSetting();
        }


        public void SaveFont(string fontName)
        {
            Font = fontName;
            WriteIniKeys("user", "Font", fontName, path + "conf.ini");
        }

        public string LoadFont()
        {
            Font = ReadIniKeys("user", "Font", path + "conf.ini");
            return Font; 
        }
        /// <summary>
        /// 用户名映射功能的自动替换用户名操作
        /// </summary>
        public string FindReplaceName(string userName)
        {
            string res = userName;
            try
            {
                if (UserNameMap.Count>0)
                {
                    foreach (var item in UserNameMap)
                    {
                        if (item.UserName == userName)
                        {
                            res = item.ReplaceValue;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog(e.Message, e);
            }
            return res;
        }

        public void FindAvaliableSkin()
        {
            try
            {
                string[] dirs = Directory.GetDirectories(AppDomain.CurrentDomain.BaseDirectory);
                string json = "{}";
                foreach (var item in dirs)
                {
                    string[] paths = Directory.GetFiles(item);
                    foreach (var path in paths)
                    {
                        if (path.Contains("info.txt"))
                        {
                            json = File.ReadAllText(path);
                            Entities.DMStyleEntity dMStyleEntity = new JsonHelper().DeserializeJsonToObject<Entities.DMStyleEntity>(json);
                            DMStyle.Add(dMStyleEntity);
                            break;
                        }
                    }
                    
                }
                
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("找自定义样式失败", e);
            }
        }

        private void LoadDMStyleSetting()
        {
            string value = ReadIniKeys("user", "DMStyle", path + "conf.ini");
            foreach (var item in DMStyle)
            {

                 item.IsCurrentUsed = (item.ID == value);
            }
        }
        #region 内存中的配置项
        public ObservableCollection<Entities.UserNameMapConfigEntity> UserNameMap { get; set; }
        public ObservableCollection<Entities.DMStyleEntity> DMStyle {get; set;}
        public string DMSort { get; set; }
        public int DMFontSize { get; set; }
        private DMTypeEnum osdType;

        public DMTypeEnum OsdType
        {
            get { return osdType; }
            set {
                osdType = value;
                if (App.osd!=null)
                {
                    App.osd.SwitchMode(osdType);
                }
            }
        }

        public int FadeTime { get; set; }
        public string OsdBgColor { get; set;}
        public int TotalRow = 3;
        public int ListRow = 10;
        public Point OsdPosition;
        public int NewDMOverLoadOpt { get; set; }
        public string OBSPath { get; set; }
        public string MainWindowBackground { get; set; }
        public Int32Rect MainWindowXYWH { get; set; }
        public double TextOPT { get; set; }
        /// <summary>
        /// 不显示烦人的部分提示
        /// </summary>
        public bool DontNag { get; set; }
        public ThemeInfo NewDDMTheme { get; set; }
        public string Font { get; set; }
        public bool UseWellcomBoard { get; set; }
        private bool _DisableRQM = false;
        public bool DisableRQM
        {
            get { return _DisableRQM; }
            set
            {
                _DisableRQM = value; WriteIniKeys("system", "DisableRQM", _DisableRQM ? "1" : "0", path + "conf.ini");
            }
        }

        public override bool ThisLoaded { get; set; }
        #endregion
    }
}
