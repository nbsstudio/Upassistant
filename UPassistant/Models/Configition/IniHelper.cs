﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using UPassistant.Langs;

namespace UPassistant.Models
{
     public abstract class IniHelper
    {
        public IniHelper()
        {
            path = AppDomain.CurrentDomain.BaseDirectory;
        }
        #region INI操作
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern uint GetPrivateProfileSection(string lpAppName, IntPtr lpReturnedString, uint nSize, string lpFileName);

        private  string ReadString(string section, string key, string def, string filePath)
        {
            StringBuilder temp = new StringBuilder(1024);

            try
            {
                GetPrivateProfileString(section, key, def, temp, 1024, filePath);
            }
            catch
            { }
            return temp.ToString();
        }
        /// <summary>
        /// 根据section，key取值
        /// </summary>
        /// <param name="section"></param>
        /// <param name="keys"></param>
        /// <param name="filePath">ini文件路径</param>
        /// <returns></returns>
        public virtual string ReadIniKeys(string section, string keys, string filePath)
        {
            return ReadString(section, keys, "", filePath);
        }

        /// <summary>
        /// 保存ini
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="filePath">ini文件路径</param>
        public virtual void WriteIniKeys(string section, string key, string value, string filePath)
        {
            WritePrivateProfileString(section, key, value, filePath);
        }
        #endregion
        protected string path;
        public abstract void Load();
        public abstract void Save(string[] Args);
        private static string sourceFile = AppDomain.CurrentDomain.BaseDirectory + "conf.ini";
        private static string destinationFile = AppDomain.CurrentDomain.BaseDirectory + "conf.ini.bak";
        public abstract bool ThisLoaded { get; set; }

        public static void BackupSetting()
        {
            MoveConfigFile(false);

        }

        private static void MoveConfigFile(bool isRecover)
        {
            try
            {
                if (isRecover)
                {
                    System.IO.File.Copy(destinationFile, sourceFile, true);
                    MessageBox.Show(LangModel.L("pub_saved")+LangModel.L("SoftwareShutdown"));
                    //模拟正常退出
                    Models.LogHelper.WirteLog("正常退出");
                    Environment.Exit(0);
                }
                else
                {
                    System.IO.File.Copy(sourceFile, destinationFile, true);
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog(e.Message, e);
            }
            
        }

        public static void RecoverSetting()
        {
            try
            {
                Task.Run(() =>
                {
                    if (File.Exists(sourceFile) && File.Exists(destinationFile))
                    {
                        bool ise = File.ReadAllText(sourceFile).Trim().Equals(File.ReadAllText(destinationFile).Trim());
                        if (!ise)
                        {
                            var res = MessageBox.Show("监测到你的配置信息与备份不一致，你可能已经重新安装过软件更新，\n请问你是否需要还原安装更新前的配置？\n--点击“是”会还原之前的配置，点“否”则不会有任何变化。", "维修天使", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                            if (res == MessageBoxResult.Yes)
                            {
                                MoveConfigFile(true);
                            }
                        }
                    }
                });
                
            }
            catch (Exception e)
            {
                LogHelper.WirteLog(e.Message,e);
            }
            
        }
    }
}
