﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Models.Configition
{
    public class NKconfig : IniHelper
    {
        public NKconfig()
        {
            AutoLogin = false;
        }
        public override void Load()
        {
            AutoLogin = ReadIniKeys("user", "AutoLogin", path + "conf.ini")=="1";
            AccessId = ReadIniKeys("user", "accessid", path + "conf.ini");
            ThisLoaded = true;
        }

        public override void Save(string[] Args)
        {
            WriteIniKeys("user", "AutoLogin", AutoLogin ? "1" : "0", path + "conf.ini");
            WriteIniKeys("user", "accessid", AccessId, path + "conf.ini");
            BackupSetting();
        }

        #region 内存中的配置
        public bool AutoLogin { get; set; }
        public string AccessId { get; set; }
        public override bool ThisLoaded { get ; set ; }
        #endregion
    }
}
