﻿namespace UPassistant.Models
{
    /// <summary>
    /// 游戏内弹幕配置项
    /// </summary>
    public class IGconfig : IniHelper
    {
        public int igFontSizeMem { get; set; }
        public string igFontStyleMem { get; set; }
        public string igFontColorMem { get; set; }
        public double igFontPointXMem { get; set; }
        public double igFontPointYMem { get; set; }
        public override bool ThisLoaded { get; set; }

        private static IGconfig igconfig = null;

        private IGconfig()
        {
            igFontPointXMem = 0.0046875;
            igFontPointYMem = 0.301851851851852;
        }
        public static IGconfig getInstance()
        {
            if (igconfig==null)
            {
                igconfig = new IGconfig();
            }
            return igconfig;
        }
        public override void Load()
        {
            int temp_int=0;
            //读取值
            int.TryParse(ReadIniKeys("user", "InnerGameFontSize", path + "conf.ini"), out temp_int);
            igFontStyleMem =  ReadIniKeys("user", "InnerGameFont", path + "conf.ini");
            igFontColorMem = ReadIniKeys("user", "InnerGameFontColor", path + "conf.ini");
            //设置值
            if (string.IsNullOrEmpty(igFontStyleMem))
            {
                igFontStyleMem = "微软雅黑";
            }

            if (temp_int<10)
            {
                temp_int = 10;
            }
            igFontSizeMem = temp_int;
            ThisLoaded = true;
        }

        public override void Save(string[] Args)
        {
            igFontSizeMem = int.Parse(Args[0]);
            igFontStyleMem = Args[1];
            igFontColorMem = Args[2];
            WriteIniKeys("user", "InnerGameFontSize",Args[0], path + "conf.ini");
            WriteIniKeys("user", "InnerGameFont", Args[1], path + "conf.ini");
            WriteIniKeys("user", "InnerGameFontColor", Args[2], path + "conf.ini");
            BackupSetting();
        }
    }
}
