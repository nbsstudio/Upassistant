﻿using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPassistant.Models
{
    /// <summary>
    /// 人工智能配置
    /// </summary>
    public class AIconfig : IniHelper
    {
        public const string DEFULT_ADMINSPT = "666";
        public const string DEFULT_SPT = "点歌";
        public override void Load()
        {
            App.RQM.Use_163_eg = ReadIniKeys("user", "Use_163_eg", path + "conf.ini") == "True" ? true : false;
            App.RQM.Use_kuwo_eg = ReadIniKeys("user", "Use_kuwo_eg", path + "conf.ini") == "True" ? true : false;
            App.RQM.Use_xiami_eg = ReadIniKeys("user", "Use_xiami_eg", path + "conf.ini") == "True" ? true : false;
            App.RQM.Use_5sing_sw_eg = ReadIniKeys("user", "Use_5sing_sw_eg", path + "conf.ini") == "True" ? true : false;
            if (!App.RQM.Use_163_eg && !App.RQM.Use_kuwo_eg && !App.RQM.Use_xiami_eg && !App.RQM.Use_5sing_sw_eg)
            {
                EDDRWindow edd = new EDDRWindow(true);
                edd.ShowDialog();
            }
            App.RQM.LrcSwitch = ReadIniKeys("user", "LrcSwitch", path + "conf.ini") == "True" ? true : false;
            App.RQM.Use_KeyWord = ReadIniKeys("user", "Use_KeyWord", path + "conf.ini") == "True" ? true : false;
            int.TryParse(ReadIniKeys("user", "RmkickTime", path + "conf.ini"), out App.RQM.RmkickTime);
            if (App.RQM.RmkickTime < 8)
            {
                App.RQM.RmkickTime = 12;
                WriteIniKeys("user", "RmkickTime", App.RQM.RmkickTime.ToString(), path + "conf.ini");
            }
            App.RQM.LimitPresonRQM = ReadIniKeys("user", "LimitPresonRQM", path + "conf.ini") == "1" ? true : false;
            if (!int.TryParse(ReadIniKeys("user", "LimitCount", path + "conf.ini"), out App.RQM.LimitCount))
            {
                App.RQM.LimitCount = 5;
            }
            double volume = 100;
            bool _isParseVol = double.TryParse(ReadIniKeys("user", "SetVolume", path + "conf.ini"), out volume);
            if (_isParseVol)
            {
                App.musicPlayer.SetVolume = (float)volume / 100;
            }
            bool _isParseOLL = int.TryParse(ReadIniKeys("user", "OverLimitLength", path + "conf.ini"), out App.musicPlayer.OverLimitLength);
            if (!_isParseOLL)
            {
                App.musicPlayer.OverLimitLength = -1;
            }
            int.TryParse(ReadIniKeys("user", "Wasapi", path + "conf.ini"), out App.audioOutPut);
            App.RQM.adminspt = ReadIniKeys("user", "adminspt_input", path+"conf.ini");
            if (string.IsNullOrWhiteSpace(App.RQM.adminspt))
            {
                App.RQM.adminspt = DEFULT_ADMINSPT;
            }
            int lrcOutMode = 1;
            App.RQM.LrcOut =int.TryParse( ReadIniKeys("user", "LrcOutMode", path + "conf.ini"),out lrcOutMode) ?lrcOutMode:1;
            App.RQM.AllowAutoControl = ReadIniKeys("user", "AllowAutoControl", path + "conf.ini")=="1"?true:false;
            int selectedDeviceNumber = 0;
            int.TryParse(ReadIniKeys("user", "SelectedDeviceNumber", path + "conf.ini"), out selectedDeviceNumber);
            App.rqmc.SelectedDeviceNumber = selectedDeviceNumber;
            BotName = ReadIniKeys("user", "bot_name", path + "conf.ini");
            ThisLoaded = true;
        }

        public string LoadMusicCloudAddress()
        {
            return ReadIniKeys("cloud", "music_download_from", AppDomain.CurrentDomain.BaseDirectory + "conf.ini");
        }
        public override void Save(string[] Args)
        {
            WriteIniKeys("user", "DownLoadOverTimeThenShutDown", Args[0], path + "conf.ini");
            WriteIniKeys("user", "spt_input", App.RQM.spt, path + "conf.ini");
            WriteIniKeys("user", "adminspt_input", App.RQM.adminspt, path + "conf.ini");
            WriteIniKeys("user", "LimitPresonRQM", App.RQM.LimitPresonRQM == true ? "1" : "0", path + "conf.ini");
            WriteIniKeys("user", "LimitCount", App.RQM.LimitCount.ToString(), path + "conf.ini");
            WriteIniKeys("user", "OverLimitLength", App.musicPlayer.OverLimitLength.ToString(), path + "conf.ini");
            WriteIniKeys("user", "RmkickTime", App.RQM.RmkickTime.ToString(), path + "conf.ini");
            WriteIniKeys("user", "AllowAutoControl", App.RQM.AllowAutoControl ? "1" : "0", path + "conf.ini");
            WriteIniKeys("user", "SelectedDeviceNumber", App.rqmc.SelectedDeviceNumber.ToString(), path + "conf.ini");
            BackupSetting();
        }
        public void SaveAdvSetting()
        {
            WriteIniKeys("user", "Use_163_eg", App.RQM.Use_163_eg.ToString(), path + "conf.ini");
            WriteIniKeys("user", "Use_kuwo_eg", App.RQM.Use_kuwo_eg.ToString(), path + "conf.ini");
            WriteIniKeys("user", "Use_xiami_eg", App.RQM.Use_xiami_eg.ToString(), path + "conf.ini");
            WriteIniKeys("user", "Use_5sing_sw_eg", App.RQM.Use_5sing_sw_eg.ToString(), path + "conf.ini");
            WriteIniKeys("user", "LrcSwitch", App.RQM.LrcSwitch.ToString(), path + "conf.ini");
            WriteIniKeys("user", "LrcOutMode", App.RQM.LrcOut.ToString(), path + "conf.ini");
            BackupSetting();
        }

        public void SaveKeyWord(string keyWord)
        {
            WriteIniKeys("user", "Use_KeyWord", keyWord, path + "conf.ini");
        }

        public void SaveAiSetting()
        {
            WriteIniKeys("user", "bot_name", BotName, path + "conf.ini");
        }

        public bool SaveRQMLimitCount()
        {
            return int.TryParse(ReadIniKeys("user", "LimitCount", path + "conf.ini"), out App.RQM.LimitCount);
        }
        #region 选项
        public int SelectedDeviceNumber { get; set; }
        public int DownLoadOverTimeThenShutDown { get; set; }
        public string BotName { get; set; }
        public override bool ThisLoaded { get; set ; }
        #endregion
    }
}
