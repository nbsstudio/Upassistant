﻿using UPassistant.Models.Util;

namespace UPassistant.Models.Music
{
    class AutoControlModel
    {
        public AutoControlModel()
        {
            Switch = true;
            Init();
        }
        public AutoControlModel(bool Switch)
        {
            this.Switch = Switch;
            Init();
        }
        private void Init()
        {
            IsPlaying = true;
        }

        
        #region 控制外部播放器
        private bool IsPlaying = false;//外部播放器是否在播放
        public bool UseOtherKey { get; set; }
        public bool Switch { get; set; }

        /// <summary>
        /// 自动控制其他播放器的“播放/暂停”
        /// </summary>
        /// <param name="eliminate">消除重音，当管理员使用消除重音指令时则自动校正播放状态</param>
        /// <param name="isAdmin">是否房管或UP主在操作</param>
        public void autoOutsidePlay(bool eliminate, bool isAdmin)
        {
            if (!Switch)
            {
                return;
            }
            if (eliminate && isAdmin)
            {
                PressPlayKey();
            }
            else
            {
                Play();
            }
        }
        private void Play()
        {
            if (!IsPlaying)
            {
                PressPlayKey();
                IsPlaying = true;
            }
            else
            {
                PressPlayKey();
                IsPlaying = false;
            }
        }
        private void PressPlayKey()
        {
            if (UseOtherKey)
            {
                KeyHook.Play2();
            }
            else
            {
                KeyHook.Play();
            }
        }
        #endregion

    }
}
