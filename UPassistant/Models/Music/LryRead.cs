﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace UPassistant.Models
{
    public class LryRead
    {
        private string FilePath = string.Empty;
        private List<string[]> geci = new List<string[]>();
        public LryRead()
        {

        }
        //载入歌词的入口
        public LryRead(string mid)
        {
            if (mid != "" || mid != "null")
            {
                FilePath = AppDomain.CurrentDomain.BaseDirectory + "cache\\" + mid + ".lrc";
            }
        }
        public List<string[]> Read()
        {
            if (FilePath != string.Empty)
            {
                if (System.IO.File.Exists(FilePath))
                {
                    geci.Clear();
                    FileStream fls = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite);
                    StreamReader file = new StreamReader(fls);
                    while (file.Peek() >= 0)
                    {
                        string row = file.ReadLine();
                        if (row.Length>10)
                        {
                            string content = Regex.Replace(row,@"\[.*\]",string.Empty);
                            if (Regex.IsMatch(row, @"\[(.*)\]"))
                            {
                                foreach (Match item in Regex.Matches(row, @"\[([^\[\]]*)\]"))
                                {
                                    geci.Add(new string[] { item.Groups[1].Value, content });
                                }
                            }                         
                        }
                    }
                    file.Close();
                    fls.Close();
                }
                else
                {
                    App.DoMC.AddDMToInterface(new CodyDanMu.DanmuInfo { logtext = "载入歌词失败，没有歌词文件" });
                    //App.B_eg.AddNewLog("载入歌词失败，没有歌词文件");
                    App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[点歌]载入歌词失败" });
                }
            }
            return geci;
        }
        public void PlayTrack(string[] _TT,List<string[]> _geci)
        {
            try
            {
                if (_geci==null)
                {
                    return;
                }
                foreach (string[] item in _geci)
                {
                    if (item[0].Substring(0,5) == _TT[0])
                    {
                        WriteToOBSText(item[1]);
                        break;
                    }
                }
            }
            catch (Exception)
            {

            }

        }
        public void Clear()
        {
            WriteToOBSText(" ");
        }
        private void WriteToOBSText(string text)
        {
            try
            {
                string mydocdir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\UP主助手\";
                if (!System.IO.Directory.Exists(mydocdir))
                {
                    System.IO.Directory.CreateDirectory(mydocdir);
                }
                string path = mydocdir + "obs点歌歌词.txt";

                FileStream fw = System.IO.File.Open(path, FileMode.OpenOrCreate);
                fw.Seek(0, SeekOrigin.Begin);
                fw.SetLength(0);
                fw.Close();
                FileStream afw = System.IO.File.Open(path, FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(afw, System.Text.Encoding.UTF8);
                sw.Write(text);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {
                //throw;
            }
        }
    }
}
