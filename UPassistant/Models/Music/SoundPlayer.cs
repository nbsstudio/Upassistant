﻿using NAudio.CoreAudioApi;
using NAudio.Wave;
using NAudioDemo.AudioPlaybackDemo;
using System;

namespace UPassistant.Models
{
    public abstract class SoundPlayer
    {
        #region NaudioPlay
        public IWavePlayer wavePlayer;
        public IOutputDevicePlugin wp;
        public AudioFileReader file;
        private string fileName;
        public float SetVolume { get; set; }
        private bool[] closeend = { false, false };//接近停止判断
        public string callBack = "";
        public string ErrMsg ="";
        public System.Timers.Timer ProccessTimer = new System.Timers.Timer(1000);

        public SoundPlayer()
        {
            ProccessTimer.Elapsed += ProccessTimer_Elapsed;
            LoadPlugin();
            SetVolume = 1;
        }

        public void LoadPlugin()
        {
            //获取设备列表
            switch (App.audioOutPut)
            {
                case 0:
                    wp = new WaveOutPlugin();
                    wp.InitialiseDeviceCombo();
                    break;
                case 1:
                    wp = new WasapiOutPlugin();
                    wp.InitialiseDeviceCombo();
                    break;
                case 2:
                    wp = new DirectSoundOutPlugin();
                    wp.InitialiseDeviceCombo();
                    break;
                default:
                    wp = new DirectSoundOutPlugin();
                    wp.InitialiseDeviceCombo();
                    break;
            }
        }

        public virtual void ProccessTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            
        }

        private IWavePlayer CreateWavePlayer()
        {
            switch (App.audioOutPut)
            {
                case 0:
                    if (wp.Name != "WaveOut")
                    {
                        LoadPlugin();
                    }
                    WaveOutEvent outDevice =  new WaveOutEvent();
                    outDevice.DeviceNumber = (int)wp.GetCurrentDevice();
                    return outDevice;
                case 1:
                    if (wp.Name != "WasapiOut")
                    {
                        LoadPlugin();
                    }
                    var outDevice2 = new WasapiOutGuiThread((MMDevice)wp.GetCurrentDevice(), AudioClientShareMode.Shared, 300);
                    //var outDevice2 = new WasapiOut((MMDevice)wp.GetCurrentDevice(), AudioClientShareMode.Shared, false,300);
                    
                    return outDevice2;
                case 2:
                    if (wp.Name != "DirectSound")
                    {
                        LoadPlugin();
                    }
                    Guid deviceGuid= new Guid(wp.GetCurrentDevice().ToString());
                    var outDevice3 = new DirectSoundOut(deviceGuid, 300);
                    return outDevice3;
                default:
                    Guid deviceGuid2 = new Guid(wp.GetCurrentDevice().ToString());
                    var outDevice4 = new DirectSoundOut(deviceGuid2, 300);
                    return outDevice4;
            }
        }
        public virtual void BeginPlayback(string filename)
        {
            //Debug.Assert(this.wavePlayer == null);
            wavePlayer = CreateWavePlayer();
            fileName = filename;
            file = new AudioFileReader(filename);
            file.Volume = SetVolume;
            wavePlayer.Init(file);
            wavePlayer.PlaybackStopped += wavePlayer_PlaybackStopped;
            wavePlayer.Play();
            ProccessTimer.Start();
        }

        /// <summary>
        /// 当播放完毕时自动触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual  void wavePlayer_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            // we want to be always on the GUI thread and be able to change GUI components
            //Debug.Assert(!this.InvokeRequired, "PlaybackStopped on wrong thread");
            // we want it to be safe to clean up input stream and playback device in the handler for PlaybackStopped
            CleanUp();
        }
        public bool DoStop()
        {
            if (file != null)
            {
                wavePlayer.Stop();
                //CleanUp();
            }
            return true;
        }
        public void CleanUp()
        {
            if (file != null)
            {
                file.Dispose();
                file = null;
            }
            if (wavePlayer != null)
            {
                wavePlayer.Dispose();
                wavePlayer = null;
            }
            ProccessTimer.Stop();
        }
        public void VolumeChange(float volume)
        {
            SetVolume = volume;
            if (file != null)
            {
                file.Volume = SetVolume;
            }
        }
        public virtual void GetSoundToPlayer(string File)
        {
            ErrMsg = string.Empty;
            try
            {
                bool isEX = System.IO.File.Exists(File);
                if (isEX)
                {
                    BeginPlayback(File);
                }
            }
            catch (Exception e)
            {
                ErrMsg = e.Message;
            }

        }
        /// <summary>
        /// 把播放器的状态转为RQM的播放状态数字
        /// </summary>
        /// <returns></returns>
        public int GetPlayerStu()
        {
            if (file != null)
            {
                switch (wavePlayer.PlaybackState)
                {
                    case PlaybackState.Stopped:
                        return 3;
                    case PlaybackState.Playing:
                        return 1;
                    case PlaybackState.Paused:
                        return 2;
                }
                return 0;
            }
            else
            {
                return 0;
            }
        }
        public void PlayOrPause()
        {
            if (wavePlayer.PlaybackState == PlaybackState.Paused)
            {

                wavePlayer.Play();
            }
            else if (wavePlayer.PlaybackState == PlaybackState.Playing)
            {
                wavePlayer.Pause();
            }
        }
        #endregion
    }
}
