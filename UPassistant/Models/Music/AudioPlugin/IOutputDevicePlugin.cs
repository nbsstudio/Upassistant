﻿using System;
using System.Linq;
using NAudio.Wave;
using System.Windows.Forms;
using System.Collections.Generic;

namespace NAudioDemo.AudioPlaybackDemo
{
    public interface IOutputDevicePlugin
    {
        void InitialiseDeviceCombo();
        object GetDevicies();
        object GetCurrentDevice();
        string Name { get; }
        bool IsAvailable { get; }
        int Priority { get; }
    }
}
