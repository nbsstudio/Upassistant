﻿using System;
using NAudio.Wave;
using System.Collections.Generic;

namespace NAudioDemo.AudioPlaybackDemo
{
    class WaveOutPlugin : IOutputDevicePlugin
    {
        List<string> ComboBoxDevice = new List<string>();

        public void InitialiseDeviceCombo()
        {
            for (int deviceId = 0; deviceId < WaveOut.DeviceCount; deviceId++)
            {
                var capabilities = WaveOut.GetCapabilities(deviceId);
                ComboBoxDevice.Add(String.Format("{0} ({1})", deviceId, capabilities.ProductName));
            }
        }

        public object GetCurrentDevice()
        {
            return UPassistant.App.rqmc.SelectedDeviceNumber;
        }

        public object GetDevicies()
        {
            return ComboBoxDevice;
        }

        public string Name
        {
            get { return "WaveOut"; }
        }

        public bool IsAvailable
        {
            get { return WaveOut.DeviceCount > 0; }
        }

        public int Priority
        {
            get { return 1; }
        }
    }
}
