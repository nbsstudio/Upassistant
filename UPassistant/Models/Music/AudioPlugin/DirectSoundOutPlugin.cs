﻿using System;
using System.Linq;
using NAudio.Wave;
using System.Windows.Forms;
using UPassistant.Models;
using System.Collections.Generic;

namespace NAudioDemo.AudioPlaybackDemo
{
    class DirectSoundOutPlugin : IOutputDevicePlugin
    {

        private readonly bool isAvailable;

        private IEnumerable<DirectSoundDeviceInfo> myDevice;
        private List<string> ComboBoxDevice = new List<string>();

        public DirectSoundOutPlugin()
        {
            isAvailable = DirectSoundOut.Devices.Any();
        }

        public void InitialiseDeviceCombo()
        {
            myDevice = DirectSoundOut.Devices;
            ComboBoxDevice = new List<string>();
            foreach (var item in myDevice.ToArray())
            {
                ComboBoxDevice.Add(string.Format("{0}", item.Description));
            }
        }

        public object GetCurrentDevice()
        {
            int index = UPassistant.App.rqmc.SelectedDeviceNumber;
            DirectSoundDeviceInfo[] dr =  myDevice.ToArray();
            return dr[index].Guid.ToString();
        }

        public object GetDevicies()
        {
            return ComboBoxDevice;
        }


        public string Name
        {
            get { return "DirectSound"; }
        }

        public bool IsAvailable
        {
            get { return isAvailable; }
        }

        public int Priority
        {
            get { return 2; } 
        }
    }
}
