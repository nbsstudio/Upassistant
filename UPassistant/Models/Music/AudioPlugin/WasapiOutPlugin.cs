﻿using System;
using NAudio.CoreAudioApi;
using System.Collections.Generic;

namespace NAudioDemo.AudioPlaybackDemo
{
    class WasapiOutPlugin : IOutputDevicePlugin
    {
        private MMDeviceCollection myDevice;
        private List<string> ComboBoxDevice = new List<string>();

        public void InitialiseDeviceCombo()
        {
            var enumerator = new MMDeviceEnumerator();
            myDevice = enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
            foreach (var endPoint in myDevice)
            {
                ComboBoxDevice.Add(string.Format("{0} ({1})", endPoint.FriendlyName, endPoint.DeviceFriendlyName));
            }
        }

        public object GetCurrentDevice()
        {
            int index = UPassistant.App.rqmc.SelectedDeviceNumber;
            var idm =  myDevice[index];
            return idm;
        }

        public MMDevice GetCurrentMMDevice()
        {
            int index = UPassistant.App.rqmc.SelectedDeviceNumber;
            return myDevice[index];
        }

        public object GetDevicies()
        {
            return ComboBoxDevice;
        }

        public string Name
        {
            get { return "WasapiOut"; }
        }

        public bool IsAvailable
        {
            // supported on Vista and above
            get { return Environment.OSVersion.Version.Major >= 6; }
        }

        public int Priority
        {
            get { return 3; }
        }
    }
    class WasapiDeviceComboItem
    {
        public string Description { get; set; }
        public MMDevice Device { get; set; }
    }
}
