﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UPassistant.Models
{
    class SoundPlayerClass
    {
        #region NaudioPlay
        public IWavePlayer wavePlayer;
        private AudioFileReader file;
        private string fileName;
        public float SetVolume = 1;
        private bool[] closeend = { false, false };//接近停止判断

        private static IWavePlayer CreateWavePlayer()
        {
            switch (App.AudioOutPut)
            {
                case 0:
                    return new WaveOutEvent();
                case 1:
                    return new Models.WasapiOutGuiThread();
                case 2:
                    return new DirectSoundOut();
                default:
                    return new DirectSoundOut();
            }
        }
        private void BeginPlayback(string filename)
        {
            //Debug.Assert(this.wavePlayer == null);
            wavePlayer = CreateWavePlayer();
            fileName = filename;
            file = new AudioFileReader(filename);
            file.Volume = SetVolume;
            wavePlayer.Init(file);
            wavePlayer.PlaybackStopped += wavePlayer_PlaybackStopped;
            wavePlayer.Play();
        }
        /// <summary>
        /// 当播放完毕时自动触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wavePlayer_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            // we want to be always on the GUI thread and be able to change GUI components
            //Debug.Assert(!this.InvokeRequired, "PlaybackStopped on wrong thread");
            // we want it to be safe to clean up input stream and playback device in the handler for PlaybackStopped
            CleanUp();
        }
        public bool _doStop()
        {
            if (file != null)
            {
                wavePlayer.Stop();
                CleanUp();
            }
            return true;
        }
        private void CleanUp()
        {
            if (file != null)
            {
                file.Dispose();
                file = null;
            }
            if (wavePlayer != null)
            {
                wavePlayer.Dispose();
                wavePlayer = null;
            }
        }
        public void VolumeChange(float volume)
        {
            SetVolume = volume;
            if (file != null)
            {
                file.Volume = SetVolume;
            }
        }
        public void GetSoundToPlayer(string File)
        {
            try
            {
                bool isEX = System.IO.File.Exists(File);
                if (isEX)
                {
                    BeginPlayback(File);
                }
            }
            catch (Exception)
            {

            }

        }
        #endregion
    }
}
