﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using UPassistant.Entities;
using UPassistant.Langs;
using UPassistant.Models;

namespace UPassistant
{
    /// <summary>
    /// AddonStore.xaml 的交互逻辑
    /// </summary>
    public partial class AddonStore : Window
    {
        private const string HOSTNAME = "http://www.nacgame.com";
        private const string API = HOSTNAME+"/ucenter/softwareapi/getAddonsMarket.html";
        private const string LOCAL_API = "http://localhost:8080/ucenter/softwareapi/getAddonsMarket.html";
        private static string PATH = AppDomain.CurrentDomain.BaseDirectory + "installed-addons.ls";
        private ObservableCollection<MarketItem> marketItems = null;
        private ObservableCollection<Addons.Addonitem> AddonsList = null;
        private List<MarketItem> installedList = null;
        private const string UPSTR = "有更新可用";
        private const string INSTSTR = "已安装";
        private bool javaExists = false;
        public AddonStore(ObservableCollection<Addons.Addonitem> currentAddonsList)
        {
            InitializeComponent();
            marketItems = new ObservableCollection<MarketItem>();
            AddonsList = currentAddonsList;
            MarketDataGrid.ItemsSource = marketItems;
            DownloadPb.Visibility = Visibility.Hidden;
            //检查安装list
            LoadList();
        }

        /// <summary>
        /// (1)检查已安装列表
        /// </summary>
        private void LoadList()
        {
            try
            {
                
                bool exists = File.Exists(PATH);
                if (exists)
                {
                    installedList = new JsonHelper().DeserializeJsonToList<MarketItem>(File.ReadAllText(PATH));
                }
                else
                {
                    File.WriteAllText(PATH, "[]", Encoding.UTF8);
                    installedList = new List<MarketItem>();
                }
                //检查本地插件，如果已并写入安装状态到市场列表
                if (AddonsList!=null)
                {
                    foreach (var item in AddonsList)
                    {
                        CheckInstalledStatus(item);
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("插件市场：读取列表失败", e);
            }
        }

        /// <summary>
        /// (2)在现有的插件清单中查询插件市场中的项目是否有安装过的，并同步到市场安装列表
        /// </summary>
        /// <param name="item"></param>
        private void CheckInstalledStatus(Addons.Addonitem item)
        {
            try
            {
                //检查已安装部分
                bool installStatus = false;
                foreach (var itl in installedList)
                {
                    if (item.name == itl.name && item.author == itl.author)
                    {
                        installStatus = true;
                        break;
                    }
                }
                if (!installStatus)
                {
                    installedList.Add(new MarketItem { name = item.name, author = item.author, version = item.version, isUfc = item.is_ufc ? 1 : 0, NeedJava = item.is_ufc ? "是" : "否", InstallStatus = INSTSTR });
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("检查同步市场失败",e);
            }
            
        }

        //先获取列表，对比本地已安装的插件的列表，检查版本
        //下载安装插件，要记录插件的名称和作者，以便检查版本
        //需要检查用户的JAVA环境

        /// <summary>
        /// (3)获取市场列表并检查本机已安装的插件项目
        /// </summary>
        private void GetList()
        {
            string json = new Netios.NetHTTP().pub_doGET(API, Encoding.UTF8);
            List<MarketItem> list = new JsonHelper().DeserializeJsonToList<MarketItem>(json);
            Dispatcher.Invoke(() =>
            {
                marketItems.Clear();
            });
            list.ForEach((s) =>
            {
                s.NeedJava = s.isUfc == 1?"是":"否";
                if (installedList!=null && installedList.Count>0)
                {
                    //检查是否已安装
                    foreach (var item in installedList)
                    {
                        if (s.name == item.name && s.author == item.author)
                        {
                            s.InstallStatus = INSTSTR;
                            break;
                        }
                    }
                }
                
                Dispatcher.Invoke(() =>
                {
                    marketItems.Add(s);
                });
            });

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Task.Run(new Action(()=>
            {
                GetList();
                //对比已安装插件的版本号
                CheckAddonsVersion();
                javaExists = CheckJava();
            }));
        }

        /// <summary>
        /// (4)检查已安装插件的更新版本
        /// </summary>
        private void CheckAddonsVersion()
        {
            try
            {
                foreach (var item in installedList)
                {
                    foreach (var item2 in marketItems)
                    {
                        if (item.name == item2.name && item.author == item2.author)
                        {
                            if (item.version != item2.version)
                            {
                                item2.InstallStatus = UPSTR;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void MarketDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void InstallBtn_Click(object sender, RoutedEventArgs e)
        {
            object item = MarketDataGrid.SelectedItem;
            if (item!=null)
            {
                var installBtn = sender as Button;
                installBtn.IsEnabled = false;
                MarketItem marketItem = item as MarketItem;

                if (marketItem.isUfc==1 && !javaExists)
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("UFC插件需要JAVA运行环境才能正常使用，请问现在就需要下载JAVA吗？","", MessageBoxButton.YesNo);
                    if (messageBoxResult== MessageBoxResult.Yes)
                    {
                        Process.Start("https://www.java.com/zh_CN/");
                    }
                    else
                    {
                        installBtn.IsEnabled = true;
                        return;
                    }
                }

                string file = Position(marketItem);
                string surfix = marketItem.downloadlink.Substring(marketItem.downloadlink.LastIndexOf("."));
                switch (surfix)
                {
                    case ".zip":
                        if (marketItem.InstallStatus == INSTSTR && MessageBox.Show("检测到你已经安装过本插件，再次安装会覆盖掉该插件的所有文件（包括配置文件），需要覆盖安装吗？", "覆盖安装", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            installBtn.IsEnabled = true;
                            break;
                        }
                        //下载安装
                        bool done = DownloadFile(HOSTNAME + "/" + marketItem.downloadlink, file, DownloadPb);
                        if (done)
                        {
                            //解压插件包并记录插件的ID，方便检查更新
                            Decompress(file);
                            if (marketItem.InstallStatus == UPSTR)
                            {
                                UpdateVersion(marketItem);
                            }
                            else if (marketItem.InstallStatus == "未安装")
                            {
                                installedList.Add(marketItem);
                            }
                            SaveList();
                            //更新状态
                            Task.Run(new Action(() =>
                            {
                                GetList();
                                //对比已安装插件的版本号
                                CheckAddonsVersion();
                            }));
                            MessageBox.Show(LangModel.L("InstallAddonSuccess"), "", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        else
                        {
                            MessageBox.Show(LangModel.L("DownloadAddonFailed"), "", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        installBtn.IsEnabled = true;
                        break;
                    case ".html":
                        installBtn.IsEnabled = true;
                        //弹出网页
                        System.Diagnostics.Process.Start(HOSTNAME + "/" + marketItem.downloadlink);
                        break;
                    default:
                        installBtn.IsEnabled = true;
                        MessageBox.Show("非法的安装包！");
                        break;
                }


            }
        }

        private string Position(MarketItem marketItem)
        {
            string file = AppDomain.CurrentDomain.BaseDirectory + @"Addons\" + marketItem.downloadlink.Substring(marketItem.downloadlink.LastIndexOf("/") + 1);
            if (marketItem.downloadlink.Substring(0, HOSTNAME.Length).Equals(HOSTNAME))
            {
                marketItem.downloadlink = marketItem.downloadlink.Substring(HOSTNAME.Length);
            }

            return file;
        }

        private void UpdateVersion(MarketItem marketItem)
        {
            foreach (var itl in installedList)
            {
                if (itl.name == marketItem.name && itl.author == marketItem.author)
                {
                    itl.version = marketItem.version;
                    break;
                }
            }
        }

        private void SaveList()
        {
            string content = new JsonHelper().SerializeObject(installedList);
            File.WriteAllText(PATH, content);
        }

        /// <summary>        
        /// c#.net 下载文件        
        /// </summary>        
        /// <param name="URL">下载文件地址</param>       
        /// 
        /// <param name="Filename">下载后的存放地址</param>        
        /// <param name="Prog">用于显示的进度条</param>        
        /// 
        public bool DownloadFile(string URL, string filename, ProgressBar prog)
        {
            prog.Visibility = Visibility.Visible;
            float percent = 0;
            try
            {
                System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
                System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
                long totalBytes = myrp.ContentLength;
                if (prog != null)
                {
                    prog.Maximum = (int)totalBytes;
                }
                System.IO.Stream st = myrp.GetResponseStream();
                System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
                long totalDownloadedByte = 0;
                byte[] by = new byte[1024];
                int osize = st.Read(by, 0, (int)by.Length);
                while (osize > 0)
                {
                    totalDownloadedByte = osize + totalDownloadedByte;
                    System.Windows.Forms.Application.DoEvents();
                    so.Write(by, 0, osize);
                    if (prog != null)
                    {
                        prog.Value = (int)totalDownloadedByte;
                    }
                    osize = st.Read(by, 0, (int)by.Length);

                    percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                    //Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
                }
            
                so.Flush();//将缓冲区内在写入到基础流中
                st.Flush();//将缓冲区内在写入到基础流中
                so.Close();
                st.Close();
                prog.Visibility = Visibility.Hidden;
                return true;
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("下载插件错误", e);
                prog.Visibility = Visibility.Hidden;
                return false;
            }
        }

        private void Decompress(string zipFile)
        {
            //解压
            try
            {
                LogHelper.WirteLog("解压文件:" + zipFile);
                
                try
                {
                    using (FileStream zipFileToOpen = new FileStream(zipFile, FileMode.Open))
                    using (ZipArchive archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Read))
                    {
                        foreach (var zipArchiveEntry in archive.Entries)
                        {
                            if (zipArchiveEntry.FullName.IndexOf("/") > 0)
                            {
                                if (Directory.Exists(zipArchiveEntry.FullName))
                                {
                                    Directory.Delete(zipArchiveEntry.FullName, true);
                                }
                            }
                            else
                            {
                                if (File.Exists(zipArchiveEntry.FullName))
                                {
                                    File.Delete(zipArchiveEntry.FullName);
                                }
                            }

                        }
                    }
                    ZipFile.ExtractToDirectory(zipFile, "./Addons");


                }
                catch (Exception e)
                {
                    LogHelper.WirteLog("m",e);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("更新失败，请稍后重试！\n\n" + ex.Message, "更新错误");
                LogHelper.WirteLog("解压错误：", ex.Message);

            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!InstallBtn.IsEnabled)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// (5)检查本机的JAVA环境
        /// </summary>
        /// <returns></returns>
        public static bool CheckJava()
        {
            try
            {
                using (Process myPro = new Process())
                {
                    myPro.StartInfo.FileName = "javaw.exe";
                    myPro.StartInfo.Arguments = @"-version";
                    myPro.StartInfo.UseShellExecute = false;
                    myPro.StartInfo.CreateNoWindow = true;
                    myPro.Start();

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //在本地市场列表中卸载插件
        public bool UninstallOne(Addons.Addonitem addonitem)
        {
            //载入本地市场列表
            LoadList();
            return UninstallOne(new MarketItem { author=addonitem.author, name=addonitem.name});
        }

        public bool UninstallOne(MarketItem addonitem)
        {
            foreach (var item in installedList)
            {
                if (item.name == addonitem.name && item.author == addonitem.author)
                {
                    installedList.Remove(item);
                    SaveList();
                    return true;
                }
            }
            return false;
        }

        private void MarketDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            InstallBtn_Click(InstallBtn, new RoutedEventArgs());
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (MarketDataGrid.SelectedIndex>-1)
            {
                var data = MarketDataGrid.SelectedItem as MarketItem;
                UninstallOne(data);
                //更新状态
                Task.Run(new Action(() =>
                {
                    GetList();
                    //对比已安装插件的版本号
                    CheckAddonsVersion();
                }));
            }
        }
    }
}
