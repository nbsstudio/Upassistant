﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Entities
{
    public class UserNameMapConfigEntity
    {
        public string UserName { get; set; }
        public string ReplaceValue { get; set; }
    }
}
