﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Entities
{
    public class DMStyleEntity 
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public bool IsCurrentUsed { get; set; }

        public string Dir { get; set; }//以程序目录下的子目录作为皮肤存放点，一个皮肤对应一个目录
    }
}
