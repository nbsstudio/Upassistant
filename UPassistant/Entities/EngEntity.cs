﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Entities
{
    //2017-3-13
    /// <summary>
    /// 引擎配置实体
    /// </summary>
    public struct EngEntity
    {
        //是否自动计算清屏行
        public bool IsAutoCalcScreenClearTimes { get; set; }
        public int LastTimeUserGotDMCount { get; set; }
        public bool UseTray { get; set; }
    }
}
