﻿using System.ComponentModel;

namespace UPassistant.Entities
{
    public class MarketItem : INotifyPropertyChanged
    {
        public string name { get; set; }
        public string author { get; set; }
        public string version { get; set; }
        public int isUfc { get; set; }
        public string NeedJava { get; set; }
        public string downloadlink { get; set; }
        private string installStatus = "未安装";
        public string InstallStatus { get { return installStatus; } set { installStatus = value; PropertyChanged?.Invoke(installStatus,new PropertyChangedEventArgs("InstallStatus")); } }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
