﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using UPassistant.Models;
using UPassistant.UI;
using UPassistant.ViewModels;

namespace UPassistant
{
    /// <summary>
    /// ConfigWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ConfigWindow : Window
    {
        private string path = AppDomain.CurrentDomain.BaseDirectory;
        private bool loading = false;
        public ConfigWindow()
        {
            loading = true;
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (saveCacheSetting())
            {
                App.uic.Save(new string[] { DBKFontComboBox.Text, RowsToClear.Text, DMFullRows.Text, FullRowsInput.Text });
                App.rqmc.Save(new string[] { dlotime.Text });
                App.egc.Save(new string[] { FRModelCheckBox.IsChecked.ToString()});
                Models.IGconfig.getInstance().Save(new string[] { IGFontSize.Text, IGFontComboBox.Text, IGColourPicker.Fill.ToString() });
                App.DoMC.notifyIcon.Visible = App.engc.UseTray;
                Close();
            }
            else
            {
                MessageBox.Show(App.Current.FindResource("Save_err_2").ToString());
            }
        }
        private bool saveCacheSetting()
        {
            bool cando = true;
            int cdmml = DMTypeComboBox.SelectedIndex;
            App.engc.UseTray = this.TrayOn.IsChecked == true ? true : false;
            App.B_eg.forbidden_way2 = FbdWay2.IsChecked == true ? true : false;
            App.uic.OsdType = (ViewModels.DMTypeEnum)cdmml;
            App.audioOutPut = this.OutPutMode.SelectedIndex;
            cando = int.TryParse(FullRowsInput.Text, out App.uic.ListRow);
            if (App.uic.ListRow < 3)//检查行控制合法性
            {
                cando = false;
                App.uic.ListRow = 3;
                MessageBox.Show(App.Current.FindResource("value_illegal").ToString(), App.Current.FindResource("pub_error").ToString());
                this.FullRowsInput.Text = "3";
            }
            cando = int.TryParse(RowsToClear.Text, out App.DoMC.crv);
            App.RQM.spt = SptInput.Text.Trim();
            App.RQM.adminspt = AdminsptInput.Text.Trim();
            cando = int.TryParse(this.DMFullRows.Text, out App.uic.TotalRow);
            int xxtime = 5;
            cando = int.TryParse(this.DMStaytime.Text, out xxtime);
            App.osd.SetFadeTime(xxtime);
            int tfs;
            cando = int.TryParse(DBKFontSize.Text, out tfs);
            App.uic.DMFontSize = tfs;
            if (App.uic.DMFontSize < 6)//检查字体合法性
            {
                cando = false;
                MessageBox.Show(App.Current.FindResource("value_illegal").ToString(), App.Current.FindResource("pub_error").ToString());
                this.DBKFontSize.Text =( App.uic.DMFontSize = 6).ToString();
            }
            cando = int.TryParse(this.RmKickTime.Text, out App.RQM.RmkickTime);
            App.RQM.LimitPresonRQM = LimitPresonRQM.IsChecked == true ? true : false;
            App.RQM.LimitCount = int.Parse(LimitCount_input.Text);
            App.osd.Textopt = DmTextOptSet.Value / 100;
            App.osd.AutoClear = DMAutoClearCheckBox.IsChecked == true ? true : false;
            App.osd.FontColour = (SolidColorBrush)FontColourPicker.Fill;
            App.osd.FontFamily = DBKFontComboBox.FontFamily;
            App.osd.SetBgColour(((SolidColorBrush)BgColourPicker.Fill).Color);
            App.musicPlayer.OverLimitLength = int.Parse(((ComboBoxItem)LimitMusicLength.SelectedItem).Tag.ToString());
            App.RQM.AllowAutoControl = AllowAutoControl.IsChecked == true ? true : false;
            App.engc.IsAutoCalcScreenClearTimes = AutoCalcCheckBox.IsChecked == true ? true : false;
            App.uic.DontNag = dontNag.IsChecked == true ? true : false;
            App.uic.DMSort = dmSort.SortValue;


            return cando;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadConfigs();
        }

        private void LoadConfigs()
        {
            DMTypeComboBox.SelectedIndex = (int)App.uic.OsdType;
            OutPutMode.SelectedIndex = App.audioOutPut;
            TrayOn.IsChecked = App.engc.UseTray;
            RowsToClear.Text = App.DoMC.crv.ToString();
            if (App.engc.IsAutoCalcScreenClearTimes)
            {
                RowsToClear.IsEnabled = false;
            }
            FullRowsInput.Text = App.uic.ListRow.ToString();
            DMFullRows.Text = App.uic.TotalRow.ToString();
            SptInput.Text = App.RQM.spt;
            AdminsptInput.Text = App.RQM.adminspt;
            DMStaytime.Text = App.osd.GetFadeTime().ToString();
            DBKFontSize.Text = App.uic.DMFontSize.ToString();
            RmKickTime.Text = App.RQM.RmkickTime.ToString();
            DmTextOptSet.Value = App.osd.Textopt * 100;
            FbdWay2.IsChecked = App.B_eg.forbidden_way2;
            LimitPresonRQM.IsChecked = App.RQM.LimitPresonRQM;
            LimitCount_input.Text = App.RQM.LimitCount.ToString();
            DMAutoClearCheckBox.IsChecked = App.osd.AutoClear;
            FontColourPicker.Fill = App.osd.FontColour;
            BgColourPicker.Fill = App.osd.getBgColour();
            OBSPathTextBox.Text = App.uic.OBSPath;
            Models.IGconfig.getInstance().Load();
            AllowAutoControl.IsChecked = App.RQM.AllowAutoControl;
            AutoCalcCheckBox.IsChecked = App.engc.IsAutoCalcScreenClearTimes;
            OutPutDevice.ItemsSource = (IEnumerable)App.musicPlayer.wp.GetDevicies();
            OutPutDevice.SelectedIndex = App.rqmc.SelectedDeviceNumber;
            try
            {
                //string dlotv = iio.ReadIniKeys("user", "DownLoadOverTimeThenShutDown", path + "conf.ini");
                dlotime.Text = App.rqmc.DownLoadOverTimeThenShutDown.ToString();
                LoadFont(DBKFontComboBox);
                LoadFont(IGFontComboBox);
                IGFontSize.Text = Models.IGconfig.getInstance().igFontSizeMem.ToString();
                IGFontComboBox.Text = Models.IGconfig.getInstance().igFontStyleMem;
                IGColourPicker.setFill(Models.IGconfig.getInstance().igFontColorMem);
                foreach (ComboBoxItem item in LimitMusicLength.Items)
                {
                    if (item.Tag.ToString() == App.musicPlayer.OverLimitLength.ToString())
                    {
                        LimitMusicLength.SelectedItem = item;
                        break;
                    }
                }
            }
            catch (Exception)
            {

            }
            dmSort.SortValue = App.uic.DMSort;
            dontNag.IsChecked = App.uic.DontNag;
            FRModelCheckBox.IsChecked = App.egc.LoadFRModel();
            loading = false;
        }

        private void LoadFont(ComboBox FontComboBox)
        {
            string nowfont = App.uic.LoadFont();
            foreach (FontFamily _f in Fonts.SystemFontFamilies)
            {
                LanguageSpecificStringDictionary _fontDic = _f.FamilyNames;
                if (_fontDic.ContainsKey(XmlLanguage.GetLanguage("zh-cn")))
                {
                    string _fontName = null;
                    if (_fontDic.TryGetValue(XmlLanguage.GetLanguage("zh-cn"), out _fontName))
                    {
                        int idx = FontComboBox.Items.Add(_fontName);
                        if (_fontName == nowfont)
                        {
                            FontComboBox.SelectedIndex = idx;
                            App.uic.Font = FontComboBox.Text;
                        }
                    }
                }
            }
            if (DBKFontComboBox.SelectedIndex == -1)
            {
                FontComboBox.SelectedIndex = 0;
                App.uic.Font = FontComboBox.Text;
                App.uic.SaveFont(FontComboBox.Text);
            }
        }
        private void dm_position_set_Click(object sender, RoutedEventArgs e)
        {
            DanMuPosition dmps = new DanMuPosition(App.uic.TotalRow, (int)App.DoMC.FontSize);
            dmps.ShowDialog();
        }

        private void closethis_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void testdm_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                saveCacheSetting();
                App.osd.TestAndApp(DBKFontComboBox.Text, int.Parse(DBKFontSize.Text), DmTextOptSet.Value);
            }
            catch (Exception ex)
            {
                LogHelper.WirteLog("测试弹幕失败",ex);
            }
        }

        private void FontColourPicker_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.ColorDialog colorDialog = new System.Windows.Forms.ColorDialog();
            colorDialog.AllowFullOpen = true;
            colorDialog.ShowDialog();
            SolidColorBrush scb = new SolidColorBrush();
            Color color = new Color();
            color.A = colorDialog.Color.A;
            color.B = colorDialog.Color.B;
            color.G = colorDialog.Color.G;
            color.R = colorDialog.Color.R;
            scb.Color = color;
            FontColourPicker.Fill = scb;
        }

        private void Set_obs_path_button1_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "OBS或直播姬软件|*.exe";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                OBSPathTextBox.Text = ofd.FileName;
                App.uic.OBSPath = ofd.FileName;
            }
        }

        private void BgColourPicker_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.ColorDialog colorDialog = new System.Windows.Forms.ColorDialog();
            colorDialog.AllowFullOpen = true;
            colorDialog.ShowDialog();
            SolidColorBrush scb = new SolidColorBrush();
            Color color = new Color();
            color.A = colorDialog.Color.A;
            color.B = colorDialog.Color.B;
            color.G = colorDialog.Color.G;
            color.R = colorDialog.Color.R;
            scb.Color = color;
            BgColourPicker.Fill = scb;
        }
        private void label12_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start(new ProcessStartInfo { FileName = "http://www.obsapp.cn/" });
        }

        private void autoCalcCheckBox_Click(object sender, RoutedEventArgs e)
        {
            RowsToClear.IsEnabled = AutoCalcCheckBox.IsChecked == true ? false : true;
        }

        private void OutPutMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (OutPutDevice != null && App.musicPlayer != null && App.musicPlayer.wp != null)
            {
                App.audioOutPut = OutPutMode.SelectedIndex;
                App.musicPlayer.LoadPlugin();
                OutPutDevice.ItemsSource = (List<string>)App.musicPlayer.wp.GetDevicies();
                //矫正
                OutPutDevice.SelectedIndex = 0;
                App.rqmc.SelectedDeviceNumber = 0;

            }

        }

        private void outPutDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!loading)
            {
                App.rqmc.SelectedDeviceNumber = OutPutDevice.SelectedIndex;
            }

        }

        private void IGColourPicker_ColorChanged(Color color)
        {

        }

        private void DMTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (!loading)
            {
                App.uic.OsdType = (DMTypeEnum)DMTypeComboBox.SelectedIndex;
                if (App.uic.OsdType == DMTypeEnum.MusicList)
                {
                    MessageBox.Show("切换到桌面歌单后，您需要重新启动UP主助手才能生效！\n\n注意：“桌面歌单”功能已经停止维护，建议你使用OBS导入歌单文本。");
                }
            }
            try
            {
                settingStyleBtn.IsEnabled = (DMTypeEnum)DMTypeComboBox.SelectedIndex == DMTypeEnum.NewDDM;
            }
            catch (Exception)
            {

            }
        }

        private void settingStyleBtn_Click(object sender, RoutedEventArgs e)
        {
            new NewDanMuSettingWindow().ShowDialog();
        }
    }
}
