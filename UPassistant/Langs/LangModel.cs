﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPassistant.Langs
{
    class LangModel
    {
        public static string L(string flag)
        {
            object text = App.Current.FindResource(flag);
            return text == null ? flag : text.ToString();
        }
    }
}
