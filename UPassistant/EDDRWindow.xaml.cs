﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Windows;
using System.Xml;
using UPassistant.Models;

namespace UPassistant
{
    /// <summary>
    /// EDDRWindow.xaml 的交互逻辑
    /// </summary>
    public partial class EDDRWindow : Window
    {
        public EDDRWindow(bool isFirst)
        {
            InitializeComponent();
            InitData();
            this.isFirst = isFirst;
        }
        private bool isFirst = false;
        private string path = AppDomain.CurrentDomain.BaseDirectory;
        ObservableCollection<eddritem> _eddritems = new ObservableCollection<eddritem>();
        private DataTable kwitem = new DataTable();
        #region 方法
        private void SaveINI()
        {
            App.rqmc.SaveAdvSetting();
        }
        private void InitData()
        {
            this.eddrlist.DataContext = _eddritems;
            kwitem.Columns.Add("关键词");
            kwitem.Columns.Add("歌名");
            kwitem.Columns.Add("储存在");
            KeyWordData.DataContext = kwitem;
        }
        private void SaveKeyWordData()
        {
            XmlDocument _xd = new XmlDocument();
            XmlElement Main = _xd.CreateElement("Main");
            foreach (DataRow dr in kwitem.Rows)
            {
                XmlElement _xe=_xd.CreateElement("KeyWord");
                XmlAttribute _xa = _xd.CreateAttribute("keyword");
                XmlAttribute _xa2 = _xd.CreateAttribute("stg");
                _xa.Value = dr.ItemArray[0].ToString();
                _xa2.Value = dr.ItemArray[2].ToString();
                _xe.Attributes.Append(_xa);
                _xe.Attributes.Append(_xa2);
                _xe.InnerText = dr.ItemArray[1].ToString();
                Main.AppendChild(_xe);
            }
            _xd.AppendChild(Main);
            _xd.Save(App.mydocdir+"关键词.kw");
        }
        private void LoadKeyWordData()
        {
            XmlDocument _xd = new XmlDocument();
            if (System.IO.File.Exists(App.mydocdir + "关键词.kw"))
            {
                _xd.Load(App.mydocdir + "关键词.kw");
                XmlNode Main = _xd.SelectSingleNode("/Main");
                foreach (XmlNode xdr in Main.ChildNodes)
                {
                    DataRow dr = kwitem.Rows.Add();
                    dr["关键词"] = xdr.Attributes["keyword"].Value;
                    dr["歌名"] = xdr.InnerText;
                    dr["储存在"] = xdr.Attributes["stg"].Value;
                }
            } 
        }
        #endregion

        #region 事件
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (string d in Directory.GetFileSystemEntries(AppDomain.CurrentDomain.BaseDirectory + @"cache\"))
                {
                    string[] ssd = d.Split('.');
                    if (File.Exists(d) && ssd[ssd.Length - 1] == "log")
                    {
                        eddritem temp = new eddritem();
                        string[] nss = d.Split('\\');
                        temp.mid = nss[nss.Length - 1].TrimEnd(".log".ToCharArray());
                        string text = System.IO.File.OpenText(d).ReadToEnd();
                        string[] s_text = text.Split(':');
                        temp.music = s_text[0];
                        temp.errst = s_text[1];
                        _eddritems.Add(temp);
                    }
                }
                Use_geci.IsChecked = App.RQM.LrcSwitch;
                Use_163.IsChecked = App.RQM.Use_163_eg;
                Use_kuwo.IsChecked = App.RQM.Use_kuwo_eg;
                Use_xiami.IsChecked = App.RQM.Use_xiami_eg;
                Use_5sing.IsChecked = App.RQM.Use_5sing_sw_eg;
                UI.Manager uim = new UI.Manager();
                kw_switch.Source = uim.SwitchStyle(App.RQM.Use_KeyWord);
                Lrc_Out_Mode.SelectedIndex = App.RQM.LrcOut - 1;
                string mydocdir = App.mydocdir;
                if (!System.IO.Directory.Exists(mydocdir))
                {
                    System.IO.Directory.CreateDirectory(mydocdir);
                }
                string path = mydocdir + "obs点歌模板.txt";
                if (System.IO.File.Exists(path))
                {
                    OBSOutPutTextBox.Text = System.IO.File.ReadAllText(path);
                }
                else
                {
                    OBSOutPutTextBox.AppendText("点歌方式：[前缀]+空格+歌名\n房管切歌请输入 666 +空格 + 切歌\n{[序号].[[引擎]][歌名]->[状态]}[列表]");
                    System.IO.File.WriteAllText(path, OBSOutPutTextBox.Text);
                }
                LoadKeyWordData();
            }
            catch (Exception)
            {

            }
        }

        private void save_button_Click(object sender, RoutedEventArgs e)
        {
            App.RQM.LrcSwitch = Use_geci.IsChecked == true ? true : false;
            App.RQM.Use_163_eg = Use_163.IsChecked == true ? true : false;
            App.RQM.Use_kuwo_eg = Use_kuwo.IsChecked == true ? true : false;
            App.RQM.Use_xiami_eg = Use_xiami.IsChecked == true ? true : false;
            App.RQM.Use_5sing_sw_eg = Use_5sing.IsChecked == true ? true : false;
            App.RQM.LrcOut = Lrc_Out_Mode.SelectedIndex + 1;
            if (!App.RQM.Use_163_eg && !App.RQM.Use_kuwo_eg && !App.RQM.Use_xiami_eg && !App.RQM.Use_5sing_sw_eg)
            {
                MessageBox.Show(Application.Current.FindResource("rqm_lib_warning").ToString());
                App.RQM.Use_kuwo_eg = true;
                Use_kuwo.IsChecked = true;
            }
            else
            {
                SaveINI();
                string mydocdir = App.mydocdir;
                if (!System.IO.Directory.Exists(mydocdir))
                {
                    System.IO.Directory.CreateDirectory(mydocdir);
                }
                string path = mydocdir + "obs点歌模板.txt";
                System.IO.File.WriteAllText(path, OBSOutPutTextBox.Text);
                if (App.RQM.LrcSwitch)
                {
                    path = mydocdir + "obs点歌歌词.txt";
                    System.IO.File.WriteAllText(path, string.Empty);
                }
                MessageBox.Show(Application.Current.FindResource("pub_saved").ToString());
                if (isFirst)
                {
                    Close();
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveKeyWordData();
            if (!App.RQM.Use_163_eg && !App.RQM.Use_kuwo_eg && !App.RQM.Use_xiami_eg && !App.RQM.Use_5sing_sw_eg)
            {
                MessageBox.Show(Application.Current.FindResource("rqm_lib_warning").ToString());
                App.RQM.Use_kuwo_eg = true;
                Use_kuwo.IsChecked = true;
                e.Cancel = true;
            }
        }
        private void kw_switch_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            App.RQM.Use_KeyWord = !App.RQM.Use_KeyWord;
            UI.Manager uim = new UI.Manager();
            kw_switch.Source = uim.SwitchStyle(App.RQM.Use_KeyWord);
            App.rqmc.SaveKeyWord(App.RQM.Use_KeyWord.ToString());
         
        }

        private void AddKeyWord_Click(object sender, RoutedEventArgs e)
        {
            if (KeyWord.Text==string.Empty)
            {
                MessageBox.Show(App.Current.FindResource("rqm_kw_nokw").ToString());
                return;
            }
            if (MusicName.Text == string.Empty)
            {
                MessageBox.Show(App.Current.FindResource("rqm_kw_nomn").ToString());
                return;
            }
            DataRow dr = kwitem.Rows.Add();
            dr["关键词"] = KeyWord.Text;
            dr["歌名"] = MusicName.Text;
            dr["储存在"] = "本地";
        }
        private void kw_switch_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            TipsBlock.Visibility = Visibility.Visible;
        }
        private void kw_switch_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            TipsBlock.Visibility = Visibility.Hidden;
        }
        private void del_kw_Click(object sender, RoutedEventArgs e)
        {
            int c=KeyWordData.SelectedIndex;
            if (c!=-1)
            {
                kwitem.Rows.RemoveAt(c);
            }
        }
        private void recover_Click(object sender, RoutedEventArgs e)
        {
            Models.LryRead lrc = new Models.LryRead();
            lrc.Clear();
        }
        #endregion


    }

    internal class eddritem
    {
        public string music { get; set; }
        public string mid { get; set; }
        public string errst { get; set; }
    }
}
