﻿using System;
using System.Windows;
using System.Data;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Forms;
using System.Windows.Media;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using UPassistant.Models.Util;
using UPassistant.Models.NewCloud;
using CodyDanMu;
using UPassistant.Models;
using System.Net;
using System.Windows.Input;
using System.Threading.Tasks;
using UPassistant.UI;
using UPassistant.Langs;
using System.ComponentModel;

namespace UPassistant
{
    /// <summary>
    /// MainController.xaml 的交互逻辑
    /// </summary>
    public partial class MainController : Window, IUPCSUser
    {
        #region 数据
        private ObservableCollection<MusicItem> MusicList = new ObservableCollection<MusicItem>();
        private DataTable b_dmitem = new DataTable();
        private string ThisPath = AppDomain.CurrentDomain.BaseDirectory;
        public int crv = 3000;//行数限定
        private delegate void Addons_Action(CodyDanMu.DanmuInfo log);
        public int dingjie = 9999999;//定上限,加数不得超过此限
        public int dm_count = 0;//接收到的弹幕总数
        public int lw_count = 0;//接收到的礼物总数
        public int fans_count = 0;
        private DateTime startTime;//接收弹幕开始时间

        private Models.NewCloud.UpcsClient upcsClient = null;//TCP调试
        private object tjTab = null;
        #endregion

        public MainController()
        {
            InitializeComponent();
            InitialTray();
            App.DoMC = this;
            //绑定数据
            musicListView.DataContext = MusicList;
            LoginWindow.logged_event += LoginWindow_logged_event;
            LoginWindow.logout_enent += LoginWindow_logout_enent;
            App.AppInitComplate += App_AppInitComplate;
            Task.Run(() =>
            {
                upcsClient = new Models.NewCloud.UpcsClient(new CloudConfig { sIp = IPAddress.Parse("115.28.222.234"), heartBeat = "NACGAME" });
                //upcsClient = new Models.NewCloud.UpcsClient(new CloudConfig { sIp = IPAddress.Parse("10.0.0.2"), heartBeat = "NACGAME" });
                upcsClient.Socket_Disconnected += UpcsClient_Socket_Disconnected;
            });

        }





        #region Addons
        /// <summary>
        /// 初始化所有插件。从1059起本函数以后台线程运行。
        /// </summary>
        private void InitAddons()
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                addons_mg.Visibility = Visibility.Hidden;
                startBtn.Enable = false;
                b_dmitem.Rows.Add(new object[] { "系统", "正在加载插件...", "" });
            }));
            try
            {
                App.addonsController.InitDll();
                App.addonsController.Start();
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("插件初始化异常", e);
            }
            base.Dispatcher.Invoke(new Action(() =>
            {
                addons_mg.Visibility = Visibility.Visible;
                UpdateQSBar();//显示插件的快速启动栏
                b_dmitem.Rows.Add(new object[] { "系统", "插件加载完成，可以正常工作了", "" });
                Models.LogHelper.WirteLog("插件加载完成");
                startBtn.Enable = true;
            }));
        }
        #endregion

        #region 界面UI
        private void BilibiliDmdataInit()
        {
            b_dmitem.Columns.Add("name");
            b_dmitem.Columns.Add("content");
            b_dmitem.Columns.Add("time");
            bilibili_dmdata.DataContext = b_dmitem;
            App.B_eg.RPCHasChanged += B_eg_RPCHasChanged;
        }

        int[] recCount = new int[2] { 0, 3000 };
        private void InitializeUI()
        {
            //App.uic.Load();
            //修复按钮图标
            UI.Manager uim = new UI.Manager();
            rqm_switch.Source = uim.SwitchStyle(App.RQM.Switch);
            App.uic.LoadMainRect();
            string bgfl = App.uic.MainWindowBackground;
            if (bgfl != "" && System.IO.File.Exists(bgfl))
            {
                MainGrid.Background = new ImageBrush() { ImageSource = uim.MainBackground(bgfl, false), Stretch = Stretch.UniformToFill };
            }
            else
            {
                MainGrid.Background = new ImageBrush() { ImageSource = uim.MainBackground(), Stretch = Stretch.UniformToFill };
            }
            this.Title = App.Current.FindResource("MainWindowTitle").ToString() + "_" + AssemblyVersion;
            this.Left = App.uic.MainWindowXYWH.X;
            this.Top = App.uic.MainWindowXYWH.Y;
            this.Width = App.uic.MainWindowXYWH.Width;
            this.Height = App.uic.MainWindowXYWH.Height;
            //初始化统计
            dm_count_now.Text = App.Current.FindResource("DM_count").ToString() + "\n" + dm_count.ToString();
            lw_count_now.Text = App.Current.FindResource("LW_count").ToString() + "\n" + lw_count.ToString();
            gz_count_now.Text = App.Current.FindResource("GZ_count").ToString() + "\n请连接房间";
            rp_count_now.Text = App.Current.FindResource("RP_count").ToString() + "\n请连接房间";
            notifyIcon.Visible = App.engc.UseTray;

        }
        public void HideStartButtonMask()
        {
            startBtn.HideStartButtonMask();
        }
        //程序特性访问器
        public string AssemblyVersion
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        /// <summary>
        /// 增加弹幕到界面列表
        /// </summary>
        /// <param name="dmContent"></param>
        public void AddDMToInterface(CodyDanMu.DanmuInfo dmContent)
        {
            try
            {
                base.Dispatcher.Invoke(new Action(() =>
                {
                    if (b_dmitem.Rows.Count == crv)
                    {
                        SaveDM();
                        b_dmitem.Rows.Clear();
                    }
                    DataRow dr = b_dmitem.Rows.Add();
                    dr["name"] = dmContent.nickname;
                    dr["content"] = dmContent.logtext;
                    dr["time"] = dmContent.datetime;
                    CountTheDM(dmContent.type);
                }));
            }
            catch (Exception ex)
            {
                Models.LogHelper.WirteLog("弹幕异常", ex);
            }

        }
        private void CountTheDM(string uid)
        {
            switch (uid)
            {
                case "gift":
                    if (lw_count != dingjie)
                    {
                        lw_count++;
                    }
                    break;
                case "system":
                    return;
                default:
                    if (dm_count != dingjie)
                    {
                        dm_count++;
                    }
                    break;
            }
            base.Dispatcher.Invoke(new Action(() =>
            {
                dm_count_now.Text = App.Current.FindResource("DM_count").ToString() + "\n" + dm_count.ToString();
                lw_count_now.Text = App.Current.FindResource("LW_count").ToString() + "\n" + lw_count.ToString();
            }));
        }
        public void UpdateQSBar()
        {
            UI.Manager uim = new UI.Manager();
            List<UI.QuickStartItem> mlist = uim.GetDynamicQuickStartBar();
            if (mlist.Count <= 0)
            {
                QuickStartBar.Visibility = Visibility.Hidden;
                MainTab.Margin = new Thickness(MainTab.Margin.Left, MainTab.Margin.Top, MainTab.Margin.Right, 10);
            }
            else
            {
                QuickStartBar.Visibility = Visibility.Visible;
                AddonsListBox.DataContext = mlist;
                MainTab.Margin = new Thickness(MainTab.Margin.Left, MainTab.Margin.Top, MainTab.Margin.Right, 42);
            }
        }
        public System.Windows.Forms.NotifyIcon notifyIcon = null;
        private void InitialTray()
        {
            try
            {
                //设置托盘的各个属性
                notifyIcon = new System.Windows.Forms.NotifyIcon();
                notifyIcon.BalloonTipText = "程序开始运行";
                notifyIcon.Text = "托盘图标";
                notifyIcon.Icon = Properties.Resources.TrayIcon;
                notifyIcon.Visible = false;
                notifyIcon.ShowBalloonTip(2000);
                notifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(notifyIcon_MouseClick);

                //设置菜单项
                System.Windows.Forms.MenuItem menu1 = new System.Windows.Forms.MenuItem("关于");
                menu1.Click += menu1_Click;
                System.Windows.Forms.MenuItem menu2 = new System.Windows.Forms.MenuItem("设置");
                menu2.Click += menu2_Click;
                //退出菜单项
                MenuItem exit = new System.Windows.Forms.MenuItem("拜拜2333");
                exit.Click += new EventHandler(exit_Click);

                //关联托盘控件
                MenuItem[] childen = new System.Windows.Forms.MenuItem[] { menu1, menu2, exit };
                notifyIcon.ContextMenu = new System.Windows.Forms.ContextMenu(childen);

                //窗体状态改变时候触发
                this.StateChanged += new EventHandler(SysTray_StateChanged);
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("托盘图标出错", e);
            }
        }
        //private void InitialBot()
        //{
        //    Fade.BOT.BName = App.rqmc.BotName;
        //    bot_name.Text = Fade.BOT.BName;
        //}

        #endregion

        #region 播放器控制
        public void EnableButtons(bool playing)
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                this.cutMusic.IsEnabled = playing;
            }));
        }
        #endregion

        #region 列表操作
        public void AddMusicItemStatus(int index, string status, string mode)
        {

            base.Dispatcher.Invoke(new Action(() =>
                {
                    if (MusicList.Count > index)
                    {
                        //MusicList[index] = new MusicItem { Name = MusicList[index].Name, Status = status, Nowmode = mode };
                        MusicList[index].Status = status;
                        MusicList[index].Nowmode = mode;
                    }
                }
            ));
        }
        private delegate void aitm();
        /// <summary>
        /// 增加点歌项
        /// </summary>
        /// <param name="musicname"></param>
        /// <param name="state"></param>
        /// <param name="mode"></param>
        /// <param name="owner"></param>
        public void AddItemToMusicList(string musicname, string state, string mode, string owner)
        {
            base.Dispatcher.Invoke(new Action(() =>
                MusicList.Add(new MusicItem { Name = musicname, Status = state, Nowmode = mode, Xowner = owner })
            ));

        }
        public void ModifyItemToMusicList(string musicname, string state, string mode, string owner)
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                foreach (var item in MusicList)
                {
                    if (item.Name == musicname)
                    {
                        item.Status = state;
                        item.Nowmode = mode;
                        item.Xowner = owner;
                    }
                }
            }
            ));

        }
        /// <summary>
        /// 查询显示列表是否比实际列表的数量多
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public bool CheckListSizeLtAccSize(int size)
        {
            return (MusicList.Count > size);
        }
        public void CompareAndRemoveMusicItem(List<Fade.RequestMusic.SongInfo> songs)
        {
            for (int i = 0; i < MusicList.Count; i++)
            {
                bool del = true;
                foreach (var item in songs)
                {
                    if (item.name == MusicList[i].Name)
                    {
                        del = true;
                        break;
                    }
                }
                if (del)
                {
                    Dispatcher.Invoke(() =>
                    {
                        MusicList.RemoveAt(i);
                    });
                }
            }
        }
        public void ClearList()
        {
            base.Dispatcher.Invoke(new Action(() =>
                MusicList.Clear()
            ));
        }
        #endregion

        #region 反向操作界面
        /// <summary>
        /// 播放器操作本界面的数据
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="args"></param>
        public void MusicBoxCtr(string mode, string[] args)
        {
            switch (mode)
            {
                case "stoped":
                    base.Dispatcher.Invoke(new Action(() =>
                    {
                        labelNowTime.Content = args[0];
                        labelTotalTime.Content = args[0];
                    }));
                    break;
                case "playing":
                    base.Dispatcher.Invoke(new Action(() =>
                    {
                        labelNowTime.Content = args[0];
                        labelTotalTime.Content = args[1];
                    }));
                    break;
                default:
                    break;
            }
        }
        public void TurnOffOSD()
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                DMSwitch.Opacity = 0.5D;
                DMSwitch.ToolTip = LangModel.L("DKB_OFF");
                App.osd.TurnOff();
            }));
        }
        public void TurnOffWellcomBoard()
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                WellcomeSwitch.Opacity = 0.5D;
            }));
        }
        /// <summary>
        /// B站直播引擎操作本界面的房间人数
        /// </summary>
        /// <param name="RPC"></param>
        private void B_eg_RPCHasChanged(int RPC)
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                if (RPC != -1)
                {
                    startBtn.RPC = RPC;
                    rp_count_now.Text = App.Current.FindResource("RP_count").ToString() + "\n" + RPC.ToString();
                    if (App.isLogin)
                    {
                        //云：发送房间人数给统计
                    }
                }
                else
                {
                    startBtn.ShowRPC1.Visibility = Visibility.Hidden;
                    rp_count_now.Text = "N/A";
                }
            }));
        }
        public void PressCutMusicButton()
        {
            base.Dispatcher.Invoke(new Action(() =>
            {
                cutMusic_Click(cutMusic, new RoutedEventArgs());
            }
            ));
        }
        public void ConnectToCloud()
        {
            if (string.IsNullOrWhiteSpace(App.nkc.AccessId))
            {
                System.Windows.MessageBox.Show(App.Current.FindResource("Err_No_accessid").ToString());
                return;
            }
            //if (!cloud_client.getConnectCloud())
            //{
            //    cloud_client.start_connect();
            //}
            //else if (System.Windows.MessageBox.Show("你已连接至云端，需要重新连接吗？", "警告：", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            //{
            //    cloud_client.restart_connect();
            //}
        }
        #endregion

        #region 文件读写
        private void SaveDM(string filename = "")
        {
            string logpath = "";
            if (filename != "")
            {
                logpath = filename;
            }
            else
            {
                string mydocdir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\UP主助手\";
                if (!System.IO.Directory.Exists(mydocdir))
                {
                    System.IO.Directory.CreateDirectory(mydocdir);
                }
                logpath = mydocdir + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + ".csv";
            }
            SaveCSV(b_dmitem, logpath);
        }
        /// <summary>
        /// 将DataTable中数据写入到CSV文件中
        /// </summary>
        /// <param name="dt">提供保存数据的DataTable</param>
        /// <param name="fileName">CSV的文件路径</param>
        public void SaveCSV(DataTable dt, string fileName)
        {
            FileStream fs = new FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default);
            string data = "";
            //写出列名称
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                data += dt.Columns[i].ColumnName.ToString();
                if (i < dt.Columns.Count - 1)
                {
                    data += ",";
                }
            }
            sw.WriteLine(data);
            //写出各行数据
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                data = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    data += dt.Rows[i][j].ToString();
                    if (j < dt.Columns.Count - 1)
                    {
                        data += ",";
                    }
                }
                sw.WriteLine(data);
            }
            sw.Close();
            fs.Close();
        }
        #endregion

        #region 网络对接
        /// <summary>
        /// 主要更新与Nacgame对接的网络数据到统计
        /// </summary>
        //private void UpdateWebData()
        //{
        //    base.Dispatcher.BeginInvoke(new Action(() =>
        //    {
        //        if (!App.isLogin)
        //        {
        //            //2016-5-25合并登录框中的自动登录
        //            //AutoLogin(MQueueClient.getInstance());
        //        }
        //        //当还没登录的时候，下面的代码相当于对界面的初始化，当AutoLogin函数走完会重新执行
        //        ff_count_now.Text = App.Current.FindResource("updating").ToString();
        //    }), null);

        //    string fftext = App.Current.FindResource("MM_level").ToString() + "\n请登录后查看";
        //    string jftext = App.Current.FindResource("JF_count").ToString() + "\n请登录后查看";
        //    if (App.isLogin)
        //    {
        //        ConnectToCloud();//自动上云
        //    }
        //    base.Dispatcher.BeginInvoke(new Action(() =>
        //    {
        //        ff_count_now.Text = fftext;
        //        jf_count_now.Text = jftext;
        //    }), null);
        //}

        private void UpdateFans()
        {
            fans_count = App.B_eg.GetRoomFans();
            this.Dispatcher.Invoke(new Action(() => gz_count_now.Text = App.Current.FindResource("GZ_count").ToString() + "\n" + fans_count));
        }

        /// <summary>
        /// 发送礼物信息到云服务
        /// </summary>
        /// <param name="danmu"></param>
        public void SendDMToCloud(DanmuInfo danmu)
        {
            try
            {
                if (null != upcsClient && upcsClient.IsConnected)
                {
                    if (danmu.type.Equals("gift"))
                    {
                        upcsClient.SendMessage(danmu);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.getLogger().Info("网络对接出错", ex);
            }
        }
        #endregion

        #region 系统
        /// <summary>
        /// 诊断异常
        /// </summary>
        private static void Diagnose()
        {
            if (!Models.LogHelper.diagnoseIsShutdownNormally())
            {
                //App.DoMC.AddDMToInterface(new DanmuInfo { logtext = "上次不正常关闭助手，请按F1获取更多帮助或反馈给开发者！", nickname=LangModel.L("sysinfo") });
                LogHelper.WirteLog("EDR101检测到上次非正常关闭");
            }
        }
        #endregion

        #region 事件
        /// <summary>
        /// 程序初始化完成
        /// </summary>
        private void App_AppInitComplate()
        {
            //初始化开始按钮
            if (!string.IsNullOrEmpty(startBtn.GetRoomID()))
            {
                HideStartButtonMask();
            }
            //初始化音量
            this.volumeSlider1.Value = App.musicPlayer.SetVolume * 100;
            //初始化网络和内置放映厅互联
            if (App.nkc.AutoLogin)
            {
                ShowLoginWindow(true);
            }
            //放映厅联动整合版
            if (App.egc.EnableAPlayerLink)
            {
                APlayerLink.GetInstance();
            }
            //恢复RQM开关按钮状态
            if (App.uic.DisableRQM && App.RQM.Switch)
            {
                RQM_Trun_ON_or_OFF();
            }
            if (!App.uic.DisableRQM && !App.RQM.Switch)
            {
                RQM_Trun_ON_or_OFF();
            }

        }
        private void bilibili_dmdata_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && e.Key == Key.Z)
            {
                new UI.NewDanMuSettingWindow().ShowDialog();
            }
            else if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && e.Key == Key.P)
            {
                App.egc.SupportOldAddons = true;
                System.Windows.MessageBox.Show("插件兼容已打开！", "隐藏功能：");
            }
        }

        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                HelpWindow.getInstance().Show();
                //System.Windows.MessageBox.Show("按了F1");
            }
            //TODO 正式版的时候注释下面这段
            if (e.Key == Key.F2)
            {
                new WellcomBaord(new DanmuInfo { nickname = "豪豪BOSS" }).Poup();
            }
        }
        private void UpcsClient_Socket_Disconnected(object sender, EventArgs e)
        {
            AddDMToInterface(new DanmuInfo { nickname = Langs.LangModel.L("sysinfo"), logtext = Langs.LangModel.L("StopConnect"), datetime = DateTime.Now.ToString() });
            App.isLogin = false;
        }

        private void LoginWindow_logged_event()
        {
            //连接云
            if (null != upcsClient && !upcsClient.IsConnected)
            {
                upcsClient.Connect();
            }
        }

        private void LoginWindow_logout_enent()
        {
            if (null != upcsClient && upcsClient.IsConnected)
            {
                upcsClient.Disconnect();
            }
        }

        private void MQueueClient_updateInfo()
        {
            //UpdateWebData();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BilibiliDmdataInit();
            #region Player
            EnableButtons(false);
            //PopulateOutputDriverCombo();
            //_doStop();
            #endregion
            InitializeUI();
            Diagnose();
            #region 特殊日子信息
            string tt_msg = new SpecailDay().runDayCheckAndGetMsg();
            if (tt_msg != null)
            {
                b_dmitem.Rows.Add(new object[] { "系统", tt_msg, "" });
            }
            #endregion
            Thread th = new Thread(InitAddons);
            th.Start();
            //InitialBot();
            //UpdateWebData();
            Models.LogHelper.WirteLog("正常启动");
            b_dmitem.Rows.Add(new object[] { "助手", Langs.LangModel.L("PressF1GetHelp"), "" });
        }



        //private void bot_name_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        //{
        //    Fade.BOT.BName = this.bot_name.Text;
        //}
        private void bot_speed_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (PubConf.TTS_Enable)
            {
                Addons_Action act = new Addons_Action(App.addonsController.SendActionLog);
                act.BeginInvoke(new DanmuInfo { logtext = "[TTS]SetRate:" + bot_speed.Value.ToString() + ";" }, null, null);
            }
        }

        private void bot_vol_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (PubConf.TTS_Enable)
            {
                Addons_Action act = new Addons_Action(App.addonsController.SendActionLog);
                act.BeginInvoke(new DanmuInfo { logtext = "[TTS]SetVol:" + bot_vol.Value.ToString() + ";" }, null, null);
                //同时更改系统音源
                App.SystemSoundVol = bot_vol.Value;
            }
        }
        void menu2_Click(object sender, EventArgs e)
        {
            ConfigWindow cw = new ConfigWindow();
            cw.ShowDialog();
        }
        void menu1_Click(object sender, EventArgs e)
        {
            HelpWindow.getInstance().Show();

        }

        private void notifyIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this.Visibility == Visibility.Visible)
                {
                    this.Visibility = Visibility.Hidden;
                }
                else
                {
                    this.WindowState = WindowState.Normal;
                    this.Visibility = Visibility.Visible;
                    this.Activate();
                }
            }
        }

        private void SysTray_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized && App.engc.UseTray)
            {
                this.Visibility = Visibility.Hidden;
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// 开启或关闭直播状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SetOnAir()
        {
            try
            {
                switch (App.B_eg.InProcess)
                {
                    case false:
                        string rid = startBtn.GetRoomID();
                        if (string.IsNullOrEmpty(rid))
                        {
                            LoginWindow lgw = new LoginWindow();
                            lgw.ShowDialog();
                            if (string.IsNullOrEmpty(rid))
                            {
                                return;
                            }
                            else
                            {
                                HideStartButtonMask();
                            }
                        }
                        App.B_eg.Start(rid);
                        base.Dispatcher.Invoke(new Action(() =>
                        {
                            startBtn.PressStart();
                            Thread th = new Thread(UpdateFans);
                            th.Start();
                        }));
                        App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[主体]发起连接;" });
                        startTime = DateTime.Now;
                        break;
                    case true:
                        base.Dispatcher.Invoke(new Action(() =>
                        {
                            startBtn.LinkOkey();
                            rp_count_now.Text = App.Current.FindResource("RP_count").ToString() + "\n请连接房间";
                        }));
                        App.B_eg.Stop();
                        //共享数据
                        if (App.isLogin)
                        {
                            //new Client().sendPackge("", new CpackgeItem { accessid = App.accessid, roomid = App.B_eg.RoomID, dmc = dm_count, lwc = lw_count, gzc = fans_count });
                        }
                        App.addonsController.SendActionLog(new CodyDanMu.DanmuInfo { logtext = "[主体]手动断开连接;" });
                        //MQueueClient.getInstance()._sendTimeLenght(startTime, DateTime.Now);
                        break;
                }
            }
            catch (Exception me)
            {
                Models.LogHelper.WirteLog("连接引擎异常", me);
            }

        }

        private void volumeSlider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                double sv = volumeSlider1.Value;
                if (App.musicPlayer != null)
                {
                    App.musicPlayer.VolumeChange((float)sv / 100);
                }

            }
            catch (Exception)
            {
            }
        }

        private void pauseMusicButton_Click(object sender, RoutedEventArgs e)
        {
            App.PNPplayback(true);
            if (App.musicPlayer.GetPlayerStu() == 2)
            {
                this.pauseMusicButton.Content = App.Current.FindResource("P_continue").ToString();
            }
            else
            {
                this.pauseMusicButton.Content = App.Current.FindResource("P_pause").ToString();
            }
        }

        private void cutMusic_Click(object sender, RoutedEventArgs e)
        {
            App.PNPplayback(false);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }
        private void Window_Closed(object sender, EventArgs e)
        {
            //Environment.Exit(0);//强制退出
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            //if (App.osd.cdmml == ViewModels.DMTypeEnum.GameDM)
            //{
            //    for (int i = 0; i < 11; i++)
            //    {
            //        App.osd.ShowDM("", "", true);
            //    }

            //}
            if (App.B_eg.InProcess)
            {
                SetOnAir();
            }
            if (null != upcsClient && upcsClient.IsConnected)
            {
                //断开云服务
                upcsClient.Disconnect();
            }
            SaveDM();
            base.Dispatcher.BeginInvoke(new Action(() =>
            {
                notifyIcon.Dispose();
                App.nkc.Save(null);//保存云、网络服务的配置
                App.uic.SaveMainWindowSetting();
                App.RQM.songCacheStu.Clear();
                App.RQM.WriteList();
                App.osd.Close();
                try
                {
                    App.addonsController.Stop();
                    App.addonsController.Shutdown();
                }
                catch (Exception ex)
                {
                    LogHelper.WirteLog("退出插件异常", ex);
                }
                DeleteFolder(AppDomain.CurrentDomain.BaseDirectory + @"cache\");
                Models.LogHelper.WirteLog("正常退出");
                Environment.Exit(0);//强制退出
            }));
        }
        /// 清空指定的文件夹，但不删除文件夹
        /// </summary>
        /// <param name="dir"></param>
        public static void DeleteFolder(string dir)
        {
            try
            {
                foreach (string d in Directory.GetFileSystemEntries(dir))
                {
                    if (File.Exists(d))
                    {
                        FileInfo fi = new FileInfo(d);
                        if (fi.Attributes.ToString().IndexOf("ReadOnly") != -1)
                            fi.Attributes = FileAttributes.Normal;
                        File.Delete(d);//直接删除其中的文件  
                    }
                    else
                    {
                        if (!Directory.Exists(dir))
                        {
                            return;
                        }
                        DirectoryInfo d1 = new DirectoryInfo(d);
                        if (d1.GetFiles().Length != 0)
                        {
                            DeleteFolder(d1.FullName);////递归删除子文件夹
                        }
                        Directory.Delete(d);
                    }
                }
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("清理软件时异常", e);
            }

        }
        private void b_saveDM_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.DefaultExt = "*.csv";
            sfd.Filter = "分隔符列表文件(*.csv)|*.csv";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SaveDM(sfd.FileName);
            }

        }

        private void addons_mg_Click(object sender, RoutedEventArgs e)
        {
            AddonsWindow adsw = new AddonsWindow();
            adsw.RefreshQuickStartBar += Adsw_RefreshQuickStartBar;
            adsw.ShowDialog();
        }
        /// <summary>
        /// 收到由插件管理器传回来的更新界面信号
        /// </summary>
        private void Adsw_RefreshQuickStartBar()
        {
            UpdateQSBar();
        }

        private void rqm_err_d_btn_Click(object sender, RoutedEventArgs e)
        {
            EDDRWindow eddr = new EDDRWindow(false);
            eddr.ShowDialog();
        }

        private void vsing_music_Checked(object sender, RoutedEventArgs e)
        {
            bool? b = vsing_music.IsChecked;
            if (b == null)
                App.RQM.Use_5sing_eg = false;
            else
                App.RQM.Use_5sing_eg = (bool)b;
        }
        private void vsing_music_Unchecked(object sender, RoutedEventArgs e)
        {
            bool? b = vsing_music.IsChecked;
            if (b == null)
                App.RQM.Use_5sing_eg = false;
            else
                App.RQM.Use_5sing_eg = (bool)b;
        }
        private void rqm_switch_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (App.uic.ThisLoaded)
            {
                RQM_Trun_ON_or_OFF();
            }
        }
        private void RQM_Trun_ON_or_OFF()
        {
            UI.Manager uim = new UI.Manager();
            App.RQM.Switch = !App.RQM.Switch;
            rqm_switch.Source = uim.SwitchStyle(App.RQM.Switch);
        }
        private void bot_switch_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            BOT_Trun_ON_or_OFF();
        }
        public void BOT_Trun_ON_or_OFF()
        {
            UI.Manager uim = new UI.Manager();
            base.Dispatcher.Invoke(new Action(() =>
            {
                if (PubConf.TTS_Enable)
                {
                    PubConf.TTS_Switch = !PubConf.TTS_Switch;
                    BotSider.Visibility = System.Windows.Visibility.Visible;
                    bot_switch.Source = uim.SwitchStyle(PubConf.TTS_Switch);
                    if (PubConf.TTS_Switch)
                    {
                        App.botStwich = 1;//机器人联动
                        Addons_Action action = new Addons_Action(App.addonsController.SendActionLog);
                        action.BeginInvoke(new DanmuInfo { logtext = "[TTS]Start;" }, null, null);
                    }
                    else
                    {
                        Addons_Action action = new Addons_Action(App.addonsController.SendActionLog);
                        action.BeginInvoke(new DanmuInfo { logtext = "[TTS]Stop;" }, null, null);
                        App.botStwich = 0;//机器人联动
                    }
                }
                else
                {
                    //当功能组件未载入时直接不允许用户开关
                    bot_switch.Source = uim.SwitchStyle(false);
                    BotSider.Visibility = System.Windows.Visibility.Hidden;
                    App.botStwich = 0;//机器人联动
                }
            }));
        }
        private void ClearMSC_Click(object sender, RoutedEventArgs e)
        {
            App.RQM.addWork(2);
            ClearList();
            if (App.musicPlayer.wavePlayer != null)
            {
                App.musicPlayer.DoStop();
                //App.wavePlayer.Stop();
            }
        }
        private void BlsMSC_Click(object sender, RoutedEventArgs e)
        {
            if (this.musicListView.SelectedItem != null)
            {
                int Muindex = this.musicListView.SelectedIndex;
                App.RQM.addWork(4, Muindex);
                System.Windows.Forms.MessageBox.Show(App.Current.FindResource("black_msg").ToString().Replace("\\n", "\n"));
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("请选中音乐再加入黑名单哦！");
            }
        }
        private void DeleteMSC_Click(object sender, RoutedEventArgs e)
        {
            if (this.musicListView.SelectedItem != null)
            {
                int Muindex = this.musicListView.SelectedIndex;
                App.RQM.addWork(3, Muindex);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("请选中音乐再点删除哦！");
            }
        }
        private void MainSetting_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ConfigWindow cw = new ConfigWindow();
            cw.ShowDialog();
        }

        private void b_roomid_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                SetOnAir();

            }
        }
        private void UserController_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                base.DragMove();
            }
            catch (Exception)
            {

            }

        }

        private void MainContent_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                base.DragMove();
            }
            catch (Exception)
            {

            }
        }
        private void minisize_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void closethis_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Close();
        }
        private void MainTab_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void StartListen_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            SetOnAir();
        }
        private void ff_count_now_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //MQueueClient.getInstance()._getMyLevel();
            //MQueueClient.getInstance()._getMyIntegrall();
        }
        private void changebg_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show("警告！使用自定义背景图片可能会影响电脑性能，建议您更换背景之后重新打开本软件！");
            OpenFileDialog ofl = new OpenFileDialog();
            ofl.Filter = "JPG图像文件|*.jpg|BMP图像文件|*.bmp|PNG图像文件|*.png";
            bool drs = ofl.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            if (drs)
            {
                UI.Manager uim = new UI.Manager();
                string[] ss = ofl.FileName.Split('.');
                string ext = ss[ss.Length - 1];
                Random rdm = new Random();
                string fl = App.mydocdir + rdm.Next(10, 8888888) + "bg." + ext;
                try
                {
                    if (File.Exists(fl))
                    {
                        File.Delete(fl);
                    }
                    File.Copy(ofl.FileName, fl, true);
                    MainGrid.Background = new ImageBrush() { ImageSource = uim.MainBackground(fl, false), Stretch = Stretch.UniformToFill };
                    App.uic.MainWindowBackground = fl;
                }
                catch (Exception)
                {
                    System.Windows.MessageBox.Show("很抱歉不支持该格式的图片！");
                    // throw;
                }
            }
        }
        //private void maimeng_rate_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        App.botRate = Convert.ToInt16(((System.Windows.Controls.ComboBoxItem)maimeng_rate.SelectedItem).Content.ToString());
        //    }
        //    catch (Exception)
        //    {

        //    }
        //}

        private void skinchange_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (SkinBox.Visibility == Visibility.Visible)
            {
                SkinBox.Visibility = Visibility.Hidden;
            }
            else
            {
                SkinBox.Visibility = Visibility.Visible;
            }
        }
        private void SkinBox_LostMouseCapture(object sender, System.Windows.Input.MouseEventArgs e)
        {
            SkinBox.Visibility = Visibility.Hidden;
        }

        private void ClearDM_Click(object sender, RoutedEventArgs e)
        {
            b_dmitem.Clear();
        }
        private void botother_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                App.addonsController.Admin("智能聊天", false);
                App.addonsController.Admin("蒙蔽机器人的心脏", false);
            }
            catch (Exception ex)
            {
                LogHelper.WirteLog("插件异常", ex);
            }

        }
        private void AddonAdminClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Windows.Controls.Image simg = (System.Windows.Controls.Image)sender;
            System.Windows.Forms.MessageBox.Show(simg.Tag.ToString());
        }
        private void AddonsListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (AddonsListBox.SelectedItem != null)
            {
                try
                {
                    UI.Manager uim = new UI.Manager();
                    string add_name = uim.GetDynamicQuickStartBar()[AddonsListBox.SelectedIndex].Name;
                    App.addonsController.SendCommand(add_name, "admin");
                }
                catch (Exception ex)
                {
                    LogHelper.WirteLog("调用插件Admin出错", ex);
                }
            }
        }
        private void RMAddon_Click(object sender, RoutedEventArgs e)
        {
            if (AddonsListBox.SelectedItem != null)
            {
                UI.Manager uim = new UI.Manager();
                string add_name = uim.GetDynamicQuickStartBar()[AddonsListBox.SelectedIndex].Name;
                App.addonsController.SetQuickStart(App.addonsController.FindAddon(add_name), false);
                UpdateQSBar();
            }
        }

        private void obsbutton_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!System.IO.File.Exists(App.uic.OBSPath))
            {
                if (System.IO.File.Exists(App.obsPath1))
                {
                    App.uic.OBSPath = App.obsPath1;
                }
                else if (System.IO.File.Exists(App.obsPath2))
                {
                    App.uic.OBSPath = App.obsPath2;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(App.Current.FindResource("Non-Set:obs_path").ToString(), "咸鱼：");
                    ConfigWindow cw = new ConfigWindow();
                    cw.ShowDialog();
                    return;
                }
            }
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(App.uic.OBSPath);
            p.StartInfo.WorkingDirectory = App.uic.OBSPath.Substring(0, App.uic.OBSPath.LastIndexOf("\\"));
            p.Start();
        }
        private void exmenu_button_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            vmenu.Visibility = vmenu.Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
        }
        private void exmenu_item1_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            vmenu.Visibility = Visibility.Hidden;
            if (!string.IsNullOrEmpty(startBtn.GetRoomID()))
            {
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo("http://live.bilibili.com/" + startBtn.GetRoomID());
                p.Start();
            }
        }
        private void exmenu_item2_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            vmenu.Visibility = Visibility.Hidden;
            ConnectToCloud();
        }
        private void startBtn_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.RightButton == System.Windows.Input.MouseButtonState.Pressed)
            {
                ShowLoginWindow();
            }
            else
            {
                SetOnAir();
            }
        }

        private static void ShowLoginWindow(bool systemAutoLogin = false)
        {
            LoginWindow lgw = new LoginWindow();
            try
            {
                if (systemAutoLogin)
                {
                    lgw.DoAutoLog();
                }
                else
                {
                    lgw.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("网络请求异常，您可能无法登陆！", "错误");
                Models.LogHelper.WirteLog("打开登陆界面异常", ex);
            }
        }
        private void addons_dev_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://gitee.com/nbsstudio/mengbi");
        }
        #endregion

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                base.DragMove();
            }
            catch (Exception)
            {

            }
        }

        private void DMSwitch_MouseDown(object sender, MouseButtonEventArgs e)
        {

            if (DMSwitch.Opacity == 1.0)
            {
                DMSwitch.Opacity = 0.5;
                DMSwitch.ToolTip = LangModel.L("DKB_OFF");
                App.osd.Hide();
            }
            else
            {
                DMSwitch.Opacity = 1.0;
                DMSwitch.ToolTip = LangModel.L("DKB_ON");
                App.osd.Show();
            }
        }

        private void WellcomeSwitch_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (WellcomeSwitch.Opacity == 1.0)
            {
                WellcomeSwitch.Opacity = 0.5;
                App.uic.UseWellcomBoard = false;
            }
            else
            {
                WellcomeSwitch.Opacity = 1.0;
                App.uic.UseWellcomBoard = true;
            }
        }

        /// <summary>
        /// 统计按钮按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TjClick(object sender, MouseButtonEventArgs e)
        {
            if (!MainTab.Items.Contains(tjTab))
            {
                System.Windows.Controls.TabItem tabItem = new System.Windows.Controls.TabItem
                {
                    Header = LangModel.L("Statistics"),
                    Style = System.Windows.Application.Current.FindResource("for_tabitem") as Style,
                    Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255))
                };
                MainTab.Items.Add(tabItem);
                tjTab = tabItem;
                MainTab.SelectedIndex = MainTab.Items.Count - 1;
            }
            

        }
    }
    public class MusicItem : INotifyPropertyChanged
    {
        private string _status;
        private string _nowmode;

        public string Name { get; set; }
        public string Status
        {
            get => _status;
            set
            {
                _status = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Status"));
            }
        }
        public string Nowmode
        {
            get => _nowmode; set
            {
                _nowmode = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Nowmode"));
            }
        }
        public string Xowner { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    public class Bilibili_dmitem
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public string Time { get; set; }
    }
}
