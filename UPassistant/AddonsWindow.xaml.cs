﻿using Addons.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using UPassistant.Langs;
using UPassistant.Models;

namespace UPassistant
{
    /// <summary>
    /// AddonsWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AddonsWindow : Window
    {
        public AddonsWindow()
        {
            InitializeComponent();
            addonsList.DataContext = AddonsList;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SearchAddons();
        }
        private ObservableCollection<Addons.Addonitem> AddonsList = new ObservableCollection<Addons.Addonitem>();
        public delegate void Ref_QuickStartBar();
        public event Ref_QuickStartBar RefreshQuickStartBar;
        private void SearchAddons()
        {
            try
            {
                foreach (Addons.Addonitem item in App.addonsController.addonsn)
                {
                    AddonsList.Add(item);
                }
                List<Addon> jadList = App.addonsController.getJavaUpassistantAddons();
                if (jadList != null)
                {
                    foreach (var jad in App.addonsController.getJavaUpassistantAddons())
                    {
                        Addons.Addonitem item = new Addons.Addonitem { is_ufc = true,name = jad.name, version = jad.version, author = jad.author , description = jad.descript, enable = jad.enable, enable_str=jad.enable?LangModel.L("Enabled"): LangModel.L("Stoped") };
                        AddonsList.Add(item);
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("读取插件列表异常",e);
            }
            
        }
        private void Refs()
        {
            try
            {
                AddonsList.Clear();
                SearchAddons();
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("插件异常",e);
            }
        }
        #region 事件
        private void add_install_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "*.zip";
            ofd.Filter = "插件压缩包(*.zip)|*.zip";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.Threading.Thread th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(App.addonsController.InstallZipPack));
                th.Start((object)ofd.FileName);
            }
        }
        private void add_list_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExecAdminAction();
        }

        private void ExecAdminAction()
        {
            try
            {
                if (addonsList.SelectedIndex > -1)
                {
                    Addons.Addonitem addonitem = AddonsList[addonsList.SelectedIndex];
                    App.addonsController.Admin(addonitem.name, addonitem.is_ufc);
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("命令错误",e);
            }
            
        }

        private void add_admin_Click(object sender, RoutedEventArgs e)
        {
            ExecAdminAction();
        }
        private void add_enable_Click(object sender, RoutedEventArgs e)
        {
            if (addonsList.SelectedIndex > -1)
            {
                Addons.Addonitem current = (Addons.Addonitem)addonsList.SelectedItem;
                if (current.Instance == null)
                {
                    //通知UFC启用
                    App.addonsController.SetStatus(current.name,true);
                }else
                {
                    App.addonsController.SetStatus(addonsList.SelectedIndex, true);
                }
                
            }
            Refs();
        }
        private void add_disable_Click(object sender, RoutedEventArgs e)
        {
            if (addonsList.SelectedIndex > -1)
            {
                Addons.Addonitem current = (Addons.Addonitem)addonsList.SelectedItem;
                if (current.Instance == null)
                {
                    //通知UFC禁用
                    App.addonsController.SetStatus(current.name, false);
                }
                else
                {
                    App.addonsController.SetStatus(addonsList.SelectedIndex, false);
                }
            }
            Refs();
        }
        private void add_uninstall_Click(object sender, RoutedEventArgs e)
        {
            if (addonsList.SelectedIndex > -1)
            {
                Addons.Addonitem current = (Addons.Addonitem)addonsList.SelectedItem;
                if (current.Instance == null)
                {
                    App.addonsController.uninstallUFCAddon(AddonsList[addonsList.SelectedIndex].name);
                }
                else
                {
                    App.addonsController.Uninstall(AddonsList[addonsList.SelectedIndex].name, AddonsList[addonsList.SelectedIndex].author);
                }
                new AddonStore(null).UninstallOne(current);
            }
            Refs();
        }

        private void closethis_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //预留
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        private void addonsshopbtn_Click(object sender, RoutedEventArgs e)
        {
            new AddonStore(AddonsList).ShowDialog();
        }
        private void add_quickstart_Click(object sender, RoutedEventArgs e)
        {
            if (addonsList.SelectedIndex > -1)
            {
                Addons.Addonitem current = (Addons.Addonitem)addonsList.SelectedItem;
                if (current.Instance == null)
                {
                    System.Windows.MessageBox.Show(LangModel.L("UFCCanNotAddInQuickStartBar"));
                }else
                {
                    App.addonsController.SetQuickStart(addonsList.SelectedIndex, true);
                    RefreshQuickStartBar();
                }
            }
        }
        private void add_authorization_Click(object sender, RoutedEventArgs e)
        {
            if (addonsList.SelectedIndex > -1)
            {
                App.addonsController.Auth_Addon(AddonsList[addonsList.SelectedIndex].name);
            }
        }
        #endregion
    }

}
