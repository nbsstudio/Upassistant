﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

/// <summary>
/// 位图资源优化区
/// 目的是减少硬盘读取资源
/// </summary>
namespace UPassistant.ViewModels.Opt
{
    public class BitmapResourceBucket
    {
        private static Dictionary<string, BitmapImage> imageDict = new Dictionary<string, BitmapImage>();


        public static BitmapImage TryTakeBitmapImage(string imagePath)
        {
            return TryTakeBitmapImage(imagePath, UriKind.Relative);
        }


        public static BitmapImage TryTakeBitmapImage(string imagePath, UriKind uriKind)
        {
            BitmapImage img;
            if (imageDict.ContainsKey(imagePath))
            {
                imageDict.TryGetValue(imagePath,out img);
                return img;
            }
            else
            {
                img = new BitmapImage(new Uri(imagePath, UriKind.Relative));
                imageDict.Add(imagePath, img);
                return img;
            }
        }
    }
}
