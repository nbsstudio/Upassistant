﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using UPassistant.Models;
using UPassistant.UI;

namespace UPassistant.ViewModels.DanMn
{
    class NewOsdShow : IOsd
    {
        public NewOsdShow(bool IsOff, DanmuShow dmsf)
        {
            this.dmsf = dmsf;
            danrudanchu.Elapsed += Danrudanchu_Elapsed;

        }

        private void Danrudanchu_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (dmQueus.Count==0)
            {
                dmsf.Dispatcher.Invoke(() =>
                {
                    DoubleAnimation daV = new DoubleAnimation(dmsf.Opacity, 0, new Duration(TimeSpan.FromMilliseconds(2000)));
                    dmsf.BeginAnimation(UIElement.OpacityProperty, daV);
                });
                danrudanchu.Stop();
            }
            DeleteUnused();

        }
        #region properties
        public int OsdFontSize {
            get { return sample.DMFontSize; }
            set { sample.DMFontSize = value; }
        }

        public string OsdHexColor { get; set; }

        public double OsdTextOpt {
            get {
                return sample.BaseOpt;
            }
            set
            {
                sample.BaseOpt = value;
            }
        }

        public int FadeTime {
            get
            {
                return (int)(danrudanchu.Interval/1000);
            }
            set
            {
                danrudanchu.Interval = value *1000;
            }
        }
        #endregion

        #region field
        private MessageBase sample;
        private DanmuShow dmsf;
        private int ttRow =0;
        private bool isOff = false;
        private System.Timers.Timer danrudanchu = new System.Timers.Timer(5000);
        private Queue<DanMu> dmQueus = new Queue<DanMu>();
        #endregion

        public void AddDM(string nickname, string status, string v, bool IsNewNow)
        {
            if (dmQueus.Count>App.uic.NewDMOverLoadOpt)
            {
                try
                {
                    dmQueus.Clear();
                    if (!App.uic.DontNag)
                    {
                        dmQueus.Enqueue(new DanMu { Danmu= "弹幕刷新过快，跳过部分弹幕以优化内存...",NickName="系统", Status="K"} );
                    }
                }
                catch (Exception)
                {
                }
            }
            dmQueus.Enqueue(new DanMu { Danmu = v, NickName = nickname, Status = status} );

        }

        private MessageBase CreateMBInstance()
        {
            MessageBase mb = new MessageBase();
            mb.DMFont = sample.DMFont;
            mb.DMFontSize = sample.DMFontSize;
            mb.BaseOpt = sample.BaseOpt;
            mb.BaseFontColor = sample.BaseFontColor;
            if (App.uic.NewDDMTheme!=null)
            {
                mb.BaseTheme = App.uic.NewDDMTheme;
            }
            return mb;
        }

        private void MoveDownAnimation(int animateMS)
        {
            dmsf.Dispatcher.Invoke(() =>
            {
            for (int i = 0; i<dmsf.newDMK.Children.Count;i++)
                {
                    var item = dmsf.newDMK.Children[i];
                    if (item!=null && item.GetType() == typeof(MessageBase))
                    {
                        MessageBase mbb = (MessageBase)item;
                                Thickness tk = new Thickness { Left = mbb.Margin.Left, Top = mbb.Margin.Top + mbb.CHeight };
                                ThicknessAnimation animation = new ThicknessAnimation();
                                animation.From = mbb.Margin;
                                animation.To = tk;
                                animation.Duration = TimeSpan.FromMilliseconds(animateMS);
                                Storyboard.SetTarget(animation, mbb);
                                Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
                                Storyboard storyboard = new Storyboard();
                                storyboard.Children.Add(animation);
                                storyboard.Begin();
                                DoubleAnimation daV = new DoubleAnimation(mbb.Opacity, 0, new Duration(TimeSpan.FromMilliseconds(2000)));
                                mbb.BeginAnimation(UIElement.OpacityProperty, daV);
                        }
                    }
            });
        }
        private void DeleteUnused()
        {
            Task.Run(async () =>
            {
                await dmsf.Dispatcher.InvokeAsync(() =>
                {
                    for (int i = 0; i < dmsf.newDMK.Children.Count; i++)
                    {
                        var item = dmsf.newDMK.Children[i];
                        if (item.GetType() == typeof(MessageBase))
                        {
                            MessageBase mbb = (MessageBase)item;
                            if (mbb.Opacity < 0.2)
                            {
                                dmsf.newDMK.Children.Remove(mbb);
                            }
                        }
                    }
                });
            });
        }
        public void ClearDM()
        {
            try
            {
                if (dmQueus.Count>0)
                {
                    dmQueus.Clear();
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("清空弹幕错误",e);
            }
        }

        public int GetDMQCount() { return -1; }

        public FontFamily GetNowFont()
        {
            return sample.DMFont;
        }

        public bool GetOFF()
        {
            return isOff;
        }

        public int GetTt_row()
        {
            return ttRow;
        }

        public void InitDM()
        {
            sample = new MessageBase();
            sample.EnableBubble = true;
            sample.Visibility = Visibility.Visible;
            try
            {
                sample.FontFamily = new FontFamily(App.uic.Font);

            }
            catch (Exception e)
            {
                LogHelper.WirteLog("字体错误：",e);
            }
            dmsf.Height = 600;
            dmsf.Width = 500;
            dmsf.Visibility = Visibility.Visible;
            dmsf.Opacity = 1;
            dmsf.bgt.Opacity = 0;
            Task.Run(()=>
            {
                ShowGroup();
            });
        }

        public void SetNowFont(FontFamily NowFont)
        {
            sample.DMFont = NowFont;
        }

        public void SetOFF(bool off)
        {
            if (off && dmQueus.Count>0)
            {
                dmQueus.Clear();
            }
            isOff = off;
        }

        public void SetTimerStart()
        {
            //throw new NotImplementedException();
        }

        public void SetTimerStop()
        {
            //throw new NotImplementedException();
        }

        public void SetTt_row(int row)
        {
            ttRow = row;
        }

        public void ShowGroup()
        {
            try
            {
                while (!isOff)
                {
                    int cc = dmQueus.Count;
                    if (cc==0)
                    {
                        Thread.Sleep(5);
                        continue;
                    }
                    DanMu v = dmQueus.Dequeue();
                    if (v==null)
                    {
                        continue;
                    }
                    dmsf.Dispatcher.Invoke(new Action(() =>
                    {
                        dmsf.Topmost = false;
                        dmsf.Topmost = true;
                        //int row_offset = rowCount * (int)sample.ActualHeight;
                        MessageBase mb = CreateMBInstance();
                        mb.EnableBubble = true;
                        mb.NickName = v.NickName;
                        mb.DanMuString = v.Danmu;
                        //Regex spdmmsg = new Regex("\\[道具娘\\]");
                        //if (spdmmsg.IsMatch(v))
                        //{
                        //    mb.Style = (Style)App.Current.FindResource("SP_OSDText");
                        //}
                        //else
                        //{
                        //    mb.Style = (Style)App.Current.FindResource("OSDText");
                        //    mb.Foreground = dmsf.FontColour;
                        //}
                        mb.Margin = new Thickness(0, 0, 0, 0);
                        Task.Run(() =>
                        {
                            MoveDownAnimation(200);
                            dmsf.Dispatcher.Invoke(() => dmsf.newDMK.Children.Add(mb));
                        });

                        if (dmsf.Opacity < OsdTextOpt)
                        {
                            DoubleAnimation daV = new DoubleAnimation(dmsf.Opacity, OsdTextOpt, new Duration(TimeSpan.FromMilliseconds(100)));
                            dmsf.BeginAnimation(UIElement.OpacityProperty, daV);
                        }
                        danrudanchu.Start();
                    }));
                    Thread.Sleep(200);
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("新弹幕显示出错", e);
            }
        }

        public void ShowOne() { }

        public void TestAndApplication(string FontName, int FontSize, double textopt)
        {
            this.OsdTextOpt = textopt / 100;
            SetNowFont(new FontFamily(FontName));
            OsdFontSize = FontSize;
            AddDM("系统消息","T","[测试姬]这是一则测试！", true);
        }

        public void WaitDMTW() { }
    }

    class DanMu
    {
        public string Danmu { get; set; }
        public string NickName { get; set; }
        public string Status { get; set; }
    }
}
