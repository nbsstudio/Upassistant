﻿using System;
using System.Timers;
using System.Windows.Media;

namespace UPassistant.ViewModels
{
    /// <summary>
    /// 封装桌面歌单
    /// </summary>
    class Osdmcshow : IOsd
    {
        public int FadeTime { get; set; }

        public double OsdTextOpt { get; set; }

        public string OsdHexColor{ get; set; }

        public int OsdFontSize { get; set; }

        private DanmuShow dmsf;
        private bool off = false;
        private Timer rchtimer;
        private double MusicListBackgroundOPT = 0.7;
        private string tips1 = "自助点歌开启中！主播可以用OBS导入文本";
        private string tips2 = "房管切歌方式：输入666+空格+切歌";
        private string list;
        private int tt_row;
        private FontFamily NowFont;


        public Osdmcshow(bool IsOff, DanmuShow dmsf)
        {
            try
            {
                this.dmsf = dmsf;
                rchtimer = new Timer();
                tt_row = 10;
                off = IsOff;
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("桌面歌单错误", e);
            }
        }
        public void AddDM(string nickname, string status, string v, bool IsNewNow)
        {
            try
            {
                list = v;
                ShowOne();

            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("桌面歌单错误", e);
            }
        }

        public void ClearDM()
        {
            list = "";
        }

        public int GetDMQCount()
        {
            return 0;
        }

        public FontFamily GetNowFont()
        {
            return NowFont;
        }

        public bool GetOFF()
        {
            return off;
            
        }

        public int GetTt_row()
        {
            return tt_row;
        }

        public void InitDM()
        {
            try
            {
                //开启顶部提示切换时钟
                rchtimer.Interval = 10000;//10s
                rchtimer.Elapsed += Rchtimer_Elapsed;
                rchtimer.Start();
                dmsf.TopTips.Visibility = System.Windows.Visibility.Visible;
                dmsf.OsdText.Visibility = System.Windows.Visibility.Visible;
                dmsf.Opacity = 1;
                dmsf.bgt.Opacity = MusicListBackgroundOPT;
                dmsf.dmpanel.Opacity = dmsf.OsdText.Opacity = dmsf.TopTips.Opacity = 1;
            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("桌面歌单错误", e);
            }
        }

        private void Rchtimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                dmsf.Dispatcher.Invoke(new Action(() =>
                {
                    if (dmsf.TopTips.Text == tips1)
                    {
                        dmsf.TopTips.Text = tips2;
                    }
                    else
                    {
                        tips1 = "自助点歌开启！格式：" + App.RQM.spt + "+空格+歌名/网易ID";
                        dmsf.TopTips.Text = tips1;
                    }
                }));
            }
            catch (Exception ex)
            {
                Models.LogHelper.WirteLog("桌面歌单错误", ex);
            }
        }

        public void SetNowFont(FontFamily NowFont)
        {
            this.NowFont = NowFont;
        }

        public void SetOFF(bool off)
        {
            if (off)
            {
                SetTimerStop();
                dmsf.dmpanel.Opacity = dmsf.OsdText.Opacity = dmsf.TopTips.Opacity = 0;
            }
            this.off = off;
        }

        public void SetTimerStart()
        {
            if (rchtimer!=null)
            {
                rchtimer.Start();
            }
        }

        public void SetTimerStop()
        {
            if (rchtimer!=null)
            {
                rchtimer.Stop();
            }
        }

        public void SetTt_row(int row)
        {
            tt_row = row;
        }

        public void ShowGroup()
        {

        }

        public void ShowOne()
        {
            try
            {
                dmsf.Dispatcher.Invoke(new Action(() =>
                {
                    //显示歌单
                    dmsf.Topmost = false;
                    dmsf.Topmost = true;
                    dmsf.OsdText.Text = list;
                    double totalRowHeight = dmsf.OsdText.Text.Split('\n').Length * 20 + 32;
                    dmsf.Height = totalRowHeight;
                    dmsf.Width = 400;
                }));

            }
            catch (Exception e)
            {
                Models.LogHelper.WirteLog("桌面歌单错误", e);
            }
        }

        public void TestAndApplication(string FontName,int FontSize, double _textopt)
        {
            
        }

        public void WaitDMTW()
        {
            SetTimerStart();
        }
    }
}
