﻿using System.Windows.Media;

namespace UPassistant.ViewModels//<--请保持命名空间一致
{
    /// <summary>
    /// 桌面弹幕接口
    /// </summary>
    interface IOsd
    {
        /// <summary>
        /// 请在实现接口是，声明带参构造函数如下
        /// 类名(bool IsOff, danmushow dmsf, int cdmml){}
        /// <param name="IsOff">是否为关闭状态，如果真代表用户不启动桌面弹幕</param>
        /// <param name="dmsf">在创建类时就传入的弹幕窗口，继承一个WPF窗口</param>
        /// <param name="cdmml">弹幕与歌单状态，弹幕开发者不需理会</param>
        /// </summary>
        #region 属性
        
        double OsdTextOpt { get; set; }
        /// <summary>
        /// 消失时间用于
        /// </summary>
        int FadeTime { get; set; }
        /// <summary>
        /// 弹幕文本的字体颜色（hex）
        /// </summary>
        string OsdHexColor { get; set; }
        /// <summary>
        /// 字体大小
        /// </summary>
        int OsdFontSize { get; set; }
        #endregion

        /// <summary>
        /// 初始化弹幕
        /// </summary>
        void InitDM();
        /// <summary>
        /// 设置关闭阀门
        /// </summary>
        /// <param name="off">true为断路，反则为通路</param>
        void SetOFF(bool off);
        /// <summary>
        /// 获取阀门状态
        /// </summary>
        /// <returns></returns>
        bool GetOFF();
        /// <summary>
        /// 向队列插入一条弹幕元素
        /// </summary>
        void AddDM(string nickname,string status,string v, bool IsNewNow);
        /// <summary>
        /// 背景处理函数，用于处理队列单元
        /// </summary>
        void WaitDMTW();
        /// <summary>
        /// 显示一条弹幕
        /// </summary>
        void ShowOne();
        /// <summary>
        /// 以组的方式显示弹幕（海量弹幕）
        /// </summary>
        void ShowGroup();
        /// <summary>
        /// 清空弹幕内容
        /// </summary>
        void ClearDM();
        /// <summary>
        /// 获取弹幕队列的元素个数
        /// </summary>
        /// <returns></returns>
        int GetDMQCount();
        /// <summary>
        /// 测试弹幕
        /// </summary>
        /// <param name="FontName">字体名称</param>
        /// <param name="_textopt">弹幕透明度</param>
        void TestAndApplication(string FontName,int FontSize, double TextOpt);
        /// <summary>
        /// 启动背景时钟
        /// </summary>
        void SetTimerStart();
        /// <summary>
        /// 停止背景时钟
        /// </summary>
        void SetTimerStop();
        /// <summary>
        /// 设置显示的行数
        /// </summary>
        /// <param name="row">行数</param>
        void SetTt_row(int row);
        /// <summary>
        /// 获取显示的行数
        /// </summary>
        int GetTt_row();
        /// <summary>
        /// 设置使用的字体
        /// </summary>
        /// <param name="NowFont">字体族</param>
        void SetNowFont(FontFamily NowFont);
        /// <summary>
        /// 获取现在使用的字体
        /// </summary>
        FontFamily GetNowFont();

    }
}
