﻿using System.Collections.Generic;
using System.Windows.Media;
using InnerGame;
using System.Threading;
using InnerGame.Model;
using System.Threading.Tasks;

namespace UPassistant.ViewModels
{
    public class INGAMEShow : IOsd
    {
        private DanmuShow danmushow;
        private LinkedList<string> dm;
        private int row = 9;
        private bool IsOff;
        private ShareMem thsm;
        private Models.IGconfig config;
        private IFormater formater;

        public INGAMEShow(bool IsOff, DanmuShow danmushow)
        {
            dm = new LinkedList<string>();
            OsdHexColor = "#FFFFFFFF";
            this.IsOff = IsOff;
            this.danmushow = danmushow;       
            config = Models.IGconfig.getInstance();
            config.Load();
            //formater = new DefaultFormater();
            formater = new D2Formater();
            formater.setFont(config.igFontStyleMem, config.igFontSizeMem, config.igFontColorMem);
            formater.setPoint(config.igFontPointXMem,config.igFontPointYMem);
        }
        public double OsdTextOpt{get;set;}
        public int FadeTime{get;set;}
        public string OsdHexColor { get; set; }
        public int OsdFontSize { get; set; }
        public void AddDM(string nickname, string status, string v, bool IsNewNow)
        {
            if (dm.Count + 1 > GetTt_row())
            {
                dm.RemoveFirst();
            }
            dm.AddLast(v);
        }

        public void ClearDM()
        {
            dm.Clear();
            if (thsm.isStarted)
            {
                thsm.isStarted = false;
                thsm.clearLines();
            }
        }

        public int GetDMQCount()
        {
            if (dm != null)
            {
                return dm.Count;
            }
            else
            {
                return -1;
            }
        }

        public FontFamily GetNowFont()
        {
            return new FontFamily(config.igFontStyleMem);
        }

        public bool GetOFF()
        {
            return IsOff;
        }

        public int GetTt_row()
        {
            return row;
        }

        public void InitDM()
        {
            danmushow.TopTips.Opacity = 0;
            danmushow.OsdText.Opacity = 0;
            config.Load();
            thsm = new ShareMem();
            thsm.Init("ForObsAPPCSWrite", 2048);
            if (thsm.isStarted)
            {
                thsm.writeLines(formater, new string[] { "UP主助手-游戏内弹幕启动" });
                //thsm.writeLines(config._igfontstyle,config._igfontsize, new string[] { config._igfontcolor + "#UP主助手-游戏内弹幕启动" });
            }

        }

        public void SetNowFont(FontFamily NowFont)
        {
            //直接从配置器读取，因为是全屏弹幕是特殊的组件
            formater.setFont(config.igFontStyleMem, config.igFontSizeMem, config.igFontColorMem);
        }

        public void SetOFF(bool off)
        {
            this.IsOff = off;
        }

        public void SetTimerStart()
        {
            //throw new NotImplementedException();
        }

        public void SetTimerStop()
        {
            //throw new NotImplementedException();
        }

        public void SetTt_row(int row)
        {
            this.row = row;
        }

        public void ShowGroup()
        {
            if (thsm.isStarted)
            {
                string[] lsa = new string[dm.Count + 1];
                dm.CopyTo(lsa, 1);
                lsa[0] = "【UP主助手报告当前房间人数：" + App.B_eg.RoomPeopleCount + "】";
                thsm.writeLines(formater, lsa);
            }
        }

        public void ShowOne()
        {

        }

        public void TestAndApplication(string FontName,int FontSize, double _textopt)
        {
            //    throw new NotImplementedException();
        }

        public void WaitDMTW()
        {
            while (!GetOFF())
            {
                if (GetDMQCount() > 0)
                {
                    ShowGroup();
                }
                Thread.Sleep(500);
            }
        }
    }
}
