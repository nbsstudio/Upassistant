﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace UPassistant.ViewModels
{
    /// <summary>
    /// 封装默认的桌面弹幕显示器
    /// </summary>
    class OsdShow : IOsd
    {
        private bool AutoClear;
        private bool hasNew = false;
        private Queue<string> dmq = new Queue<string>();
        private int row_c = 0;//行计数
        private FontFamily NowFont;
        private System.Timers.Timer danrudanchu = new System.Timers.Timer();
        private bool off;
        private DanmuShow dmsf;
        private int tt_row;//弹幕行数
        public double OsdTextOpt { get; set; }
        public int FadeTime { get; set; }
        public string OsdHexColor{ get; set; }
        public int OsdFontSize { get; set; }

        public OsdShow(bool IsOff, DanmuShow dmsf)
        {
            off = IsOff;
            this.dmsf = dmsf;
            FadeTime = 5;
            tt_row = 9;
            NowFont = readInitFont();
        }
        
        public bool GetOFF()
        {
            return off;
        }
        public void SetOFF(bool off)
        {
            if (off)
            {
                ClearDM();
                SetTimerStop();
            }
            this.off = off;
        }
        public void AddDM(string nickname,string status,string v, bool IsNewNow)
        {
            if (off)
            {
                return;
            }
            //对文字做处理
            v = v.Replace("弹幕：", " 说: ");
            //加入到弹幕队列
            dmq.Enqueue(v);
            hasNew = IsNewNow;
        }
        public void InitDM()
        {
            
            danrudanchu.Elapsed += danrudanchu_Elapsed;
            SetNowFont(new FontFamily(App.uic.LoadFont()));
            AutoClear = App.uic.LoadAutoClearSetting();
            try//配置字体颜色
            {
                string colourtext = App.uic.LoadFontColour();
                if (!string.IsNullOrEmpty(colourtext))
                {
                    System.Drawing.Color ffc = System.Drawing.ColorTranslator.FromHtml(colourtext);
                    Color ffac = Color.FromArgb(ffc.A, ffc.R, ffc.G, ffc.B);
                    dmsf.FontColour = new SolidColorBrush(ffac);
                }
                else
                {
                    dmsf.FontColour = new SolidColorBrush(Colors.White);
                }
            }
            catch (Exception)
            {
            }
            OsdTextOpt = App.uic.TextOPT;
            string _dkbfont = null;
            if (_dkbfont == "")//设置默认字体
            {
                _dkbfont = "Mircosoft YaHei UI";
                App.uic.SaveFont(_dkbfont);
            }

            NowFont = GetNowFont();
            if (OsdFontSize < 5)
            {
                OsdFontSize = 18;
            }

            danrudanchu.Start();
            dmsf.bgt.Opacity = 0;
            //隐藏列表文字显示
            dmsf.TopTips.Opacity = 0;
            dmsf.OsdText.Opacity = 0;
        }
        public void WaitDMTW()
        {
            while (!GetOFF())
            {
                if (GetDMQCount() > 10)
                {
                    new Thread(ShowGroup).Start();
                }
                else if(GetDMQCount()>0)
                {
                    new Thread(ShowOne).Start();
                }                
                Thread.Sleep(300);
            }
        }
        public void ShowOne()
        {
            if (dmq.Count > 0)
            {
                string v = dmq.Dequeue();
                if (v == null)
                {
                    return;
                }
                try
                {
                    dmsf.Dispatcher.Invoke(new Action(() =>
                    {
                        dmsf.Topmost = false;
                        dmsf.Topmost = true;
                        dmsf.Width = 650;
                        int lineheigh = OsdFontSize + 6;
                        //隐藏歌单显示
                        int row_offset = row_c * lineheigh;
                        TextBlock tmp = new TextBlock();
                        tmp.Text = v;
                        Regex spdmmsg = new Regex("\\[道具娘\\]");
                        if (spdmmsg.IsMatch(v))
                        {
                            tmp.Style = (Style)App.Current.FindResource("SP_OSDText");
                        }
                        else
                        {
                            tmp.Style = (Style)App.Current.FindResource("OSDText");
                            tmp.Foreground = dmsf.FontColour;
                        }
                        tmp.FontSize = OsdFontSize;
                        tmp.FontFamily = NowFont;
                        tmp.Margin = new Thickness(0, row_offset, 0, 0);

                        dmsf.dmpanel.Children.Add(tmp);
                        dmsf.Height = tt_row * lineheigh;
                        if (row_offset > dmsf.Height - 10)
                        {

                            foreach (var item in dmsf.dmpanel.Children)
                            {
                                TextBlock rtmp = (TextBlock)item;
                                rtmp.Margin = new Thickness(0, rtmp.Margin.Top - lineheigh, 0, 0);
                            }
                            if (row_c > tt_row)
                            {
                                //用户把行数设置少了
                                dmsf.dmpanel.Children.RemoveRange(0, row_c - tt_row);
                                row_c = tt_row;
                            }
                            else
                            {
                                dmsf.dmpanel.Children.RemoveRange(0, 1);
                            }

                        }
                        else
                        {
                            row_c++;
                        }
                        if (dmsf.Opacity == 0)
                        {
                            DoubleAnimation daV = new DoubleAnimation(0, OsdTextOpt, new Duration(TimeSpan.FromSeconds(1)));
                            dmsf.BeginAnimation(UIElement.OpacityProperty, daV);
                        }
                        danrudanchu.Interval = FadeTime * 1000;
                        danrudanchu.Enabled = false;
                        danrudanchu.Enabled = true;
                    }));
                }
                catch (Exception)
                {

                }
            }
        }
        public void ShowGroup()
        {
            while (dmq.Count > 1)
            {
                ShowOne();
            }
        }
        public void ClearDM()
        {
            foreach (var item in dmsf.dmpanel.Children)
            {
                TextBlock tb = (TextBlock)item;
                if (tb.Name != "top_tips" || tb.Name != "osdText")
                {
                    tb.Opacity = 0;
                    tb.Text = "";
                }
            }

        }
        public int GetDMQCount()
        {
            return dmq.Count;
        }
        public void TestAndApplication(string FontName, int FontSize, double _textopt)
        {
            OsdTextOpt = _textopt / 100;
            SetNowFont(new FontFamily(FontName));
            AddDM("系统消息","T","这是一则测试(某些元素需要确定后重启软件生效)！", true);
            AddDM("系统消息", "T","This is a test！", true);
        }
        public void SetTimerStart()
        {
            danrudanchu.Start();
        }
        public void SetTimerStop()
        {
            danrudanchu.Stop();
        }
        public void SetNowFont(FontFamily NowFont)
        {
            this.NowFont = NowFont;
        }
        public void SetTt_row(int row)
        {
            if (row > 0)
            {
                tt_row = row;
            }
        }
        public int GetTt_row()
        {
            return tt_row;
        }
        public FontFamily GetNowFont()
        {
            return NowFont;
        }
        private void danrudanchu_Elapsed(object sender, ElapsedEventArgs e)
        {
            dmsf.Dispatcher.Invoke(new Action(() =>
            {
                DoubleAnimation daV = new DoubleAnimation(OsdTextOpt, 0, new Duration(TimeSpan.FromSeconds(0.3)));
                dmsf.BeginAnimation(UIElement.OpacityProperty, daV);
                if (AutoClear)
                {
                    dmsf.dmpanel.Children.Clear();
                }
            }));
            danrudanchu.Enabled = false;
        }
        private FontFamily readInitFont()
        {
            FontFamily bkfont = new FontFamily("黑体");//针对没有微软雅黑的系统
            foreach (FontFamily _f in Fonts.SystemFontFamilies)
            {
                LanguageSpecificStringDictionary _fontDic = _f.FamilyNames;
                if (_fontDic.ContainsKey(System.Windows.Markup.XmlLanguage.GetLanguage("zh-cn")))
                {
                    string _fontName = null;
                    if (_fontDic.TryGetValue(System.Windows.Markup.XmlLanguage.GetLanguage("zh-cn"), out _fontName))
                    {
                        if (_fontName == "Mircosoft YaHei UI")
                        {
                            bkfont = _f;
                        }
                    }
                }
            }
            return bkfont;
        }
    }
}
