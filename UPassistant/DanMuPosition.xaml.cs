﻿using System;
using System.Windows;
using System.Windows.Input;

namespace UPassistant
{
    /// <summary>
    /// DanMuPosition.xaml 的交互逻辑
    /// </summary>
    public partial class DanMuPosition : Window
    {
        private DanMuPosition()
        {
            InitializeComponent();
            
        }
        public DanMuPosition(int rowCount,int fontSize)
        {
            InitializeComponent();
            initSize(rowCount,fontSize);
        }

        private void initSize(int rowCount, int fontSize)
        {
            //阴影边占10px距离所以要加10
            Width = 650+20;
            Height = rowCount * (fontSize+6)+20;

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            App.osd.Top = this.Top;
            App.osd.Left = this.Left;
            App.uic.SaveMainWindowSetting();//引用此方法保存弹幕的位置
            //iio.WriteIniKeys("user", "dm_position_y", App.osd.Top.ToString(), AppDomain.CurrentDomain.BaseDirectory + "conf.ini");
            //iio.WriteIniKeys("user", "dm_position_x", App.osd.Left.ToString(), AppDomain.CurrentDomain.BaseDirectory + "conf.ini");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Top = App.osd.Top;
            this.Left = App.osd.Left;
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
