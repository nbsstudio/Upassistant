﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace UPassistant.UI
{
    class Manager
    {
        public BitmapImage SwitchStyle(bool on_off)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            if (on_off)
            {
                bi.UriSource = new Uri(@"Resources/switch_on.png", UriKind.RelativeOrAbsolute);
            }
            else
            {
                bi.UriSource = new Uri(@"Resources/switch_off.png", UriKind.RelativeOrAbsolute);
            }
            bi.EndInit();
            return bi;
        }
        public BitmapImage MainBackground(string uri="",bool defalut=true)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            if (defalut)//默认值
            {
                bi.UriSource = new Uri(@"pack://application:,,,/Resources/mainbg.jpg", UriKind.RelativeOrAbsolute);
            }
            else if(uri != "")
            {
                bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            }
            bi.EndInit();
            return bi;
        }
        public List<QuickStartItem> GetDynamicQuickStartBar()
        {
            List<QuickStartItem> quickstartitem = new List<QuickStartItem>();
            for (int i = 1; i <= 5; i++)
            {
                Addons.Addonitem NowSelected = App.addonsController.addonsn.Find((Addons.Addonitem s) => s.quickstart == i);
                if (NowSelected!=null)
                {
                    quickstartitem.Add(new QuickStartItem { Name = NowSelected.name, Tip = NowSelected.name + App.Current.FindResource("dbclick_to_open").ToString(), Icon = new Uri(NowSelected.icon_filename, UriKind.RelativeOrAbsolute) });
                }
            }
            return quickstartitem;
        }
    }
    public class QuickStartItem
    {
        public string Name { get; set; }
        public string Tip { get; set; }
        public Uri Icon{ get; set; }
    }
}
