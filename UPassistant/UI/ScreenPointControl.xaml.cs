﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UPassistant.UI
{
    /// <summary>
    /// ScreenPointControl.xaml 的交互逻辑
    /// </summary>
    public partial class ScreenPointControl : UserControl
    {
        public delegate void pointConfirm(double x,double y);
        public event pointConfirm PointConfirm;

        public ScreenPointControl()
        {
            InitializeComponent();
        }

        public void setPoint(double x,double y)
        {
            Canvas.SetLeft(thumb, x);
            Canvas.SetTop(thumb, y);
        }

        private void thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            double left = Canvas.GetLeft(thumb) + e.HorizontalChange;
            double top = Canvas.GetTop(thumb) + e.VerticalChange;
            if (top <= 0)
                top = 0;
            if (top >= (mainPanel.ActualHeight-thumb.Height))
                top = (mainPanel.ActualHeight-thumb.Height);
            if (left <= 0)
                left = 0;
            if (left >= (mainPanel.ActualWidth - thumb.Width))
                left = mainPanel.ActualWidth - thumb.Width;
            Canvas.SetLeft(thumb,left);
            Canvas.SetTop(thumb, top);
        }

        private void thumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            try
            {
                PointConfirm(Canvas.GetLeft(thumb), Canvas.GetTop(thumb));
            }
            catch (Exception ex)
            {
                Models.LogHelper.getLogger().Debug("设置定点错误", ex);
            } 
        }

        private void thumb_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {

        }
    }
}
