﻿using mshtml;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using UPassistant.Langs;

namespace UPassistant.UI
{
    /// <summary>
    /// UserLoginControl.xaml 的交互逻辑
    /// </summary>
    public partial class UserLoginControl : UserControl
    {
        private WebBrowser webBrowser;
        private HTMLDocumentClass hdc;
        private const string NACGAME_API_URL = "http://nacgame.com/ucenter/softwareapi";
        //private const string LOCALHOST_API_URL = "http://10.0.0.3:8080/ucenter/softwareapi";
        private const bool DEBUG_LOCAL_LOG_PAGE = false;
        public delegate void logined(string accessid,string roomID,bool autoLoginChecked);
        public delegate void roomidSetted(string roomID);
        public delegate void public_event();
        /// <summary>
        /// 当登录成功之后执行并传输accessid
        /// </summary>
        public event logined loginedEvent;
        /// <summary>
        /// 当登录页面加载完之后执行
        /// </summary>
        public event public_event loginPageLoaded;
        public event roomidSetted onlyRoomidSetted;
        public event public_event changeToLocalLogPage;
        public UserLoginControl()
        {
            InitializeComponent();
            Init();
        }
        private void Init()
        {
            try
            {
                string roomidCondition = "";
                if (App.egc.RoomID!=null && App.egc.RoomID.Trim()!="")
                {
                    roomidCondition = "?roomid=" + App.egc.RoomID.Trim();
                }
                loginFrame.Source = new Uri(NACGAME_API_URL + "/soft_login_2.html"+roomidCondition);
                Task.Run(()=>{
                    Thread.Sleep(1000);
                    if (loginFrame.CurrentSource == null)
                    {
                        if (!App.isLogin)
                        {
                            //使用本地页面
                            changeToLocalLogPage?.Invoke();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Models.LogHelper.WirteLog("登陆异常", ex);
            }
           

        }
        /// <summary>
        /// 自动登陆NCD
        /// </summary>
        public void AutoLogin()
        {
            if (!string.IsNullOrEmpty(App.nkc.AccessId))
            {
                string today = DateTime.Now.ToString("yyyy-MM-dd");
                string parkCode = CodyDanMu.Util.ParkEncryptUtil.encrypt(App.nkc.AccessId+","+today+",upassistant");
                loginFrame.Source = new Uri(NACGAME_API_URL + "/do_soft_login_auto.html?park="+parkCode);
                
                
            }
        }

        private void login_f_Navigated(object sender, NavigationEventArgs e)
        {
            try
            {
                loginPageLoaded?.Invoke();
                //login_f上下文加载完毕之后才获取浏览器对象
                webBrowser = (WebBrowser)loginFrame.Content;
                webBrowser.Navigated += WebBrowser_Navigated;
            }
            catch (Exception)
            {
                
            }
        }

        private void WebBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            hdc = (HTMLDocumentClass)webBrowser.Document;
            if (e.Uri.AbsoluteUri==( NACGAME_API_URL + "/do_soft_login.html"))
            {
                ParseLogResult();
            }
            else if (e.Uri.AbsoluteUri.IndexOf("/do_soft_login_auto.html")>0)
            {
                ParseLogResult();
            }
        }

        private void ParseLogResult()
        {
            IHTMLElement he = hdc.getElementById("sec_content");
            IHTMLElement autoLogCheckBox = hdc.getElementById("auto_log_check");
            IHTMLElement roomID = hdc.getElementById("room_id");
            //IHTMLElement at = hdc.getElementById("accept_content");
            if (he != null)
            {
                string sec = he.innerHTML;
                if (sec == "null")//用户只输入了房间ID
                {
                    onlyRoomidSetted?.Invoke(roomID.innerHTML);
                }
                else
                {
                    loginedEvent(sec, roomID.innerHTML, autoLogCheckBox.innerHTML == "true");
                }
                
            }else if(he == null)
            {
                MessageBox.Show(LangModel.L("LogFailed"));
            }
        }

        //private void loginBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    Netios.NetHTTP ntp = new Netios.NetHTTP();
        //    string post = String.Format("username={0}&password={1}",userName.Text,userPassword.Password);
        //    string result = ntp.pub_doPOST(NACGAME_API_URL + "/do_soft_login.html", post, Encoding.UTF8);
        //    if (result!=null && !result.Equals(""))
        //    {
        //        IHTMLDocument2 doc4 = new HTMLDocumentClass();
        //        doc4.write(result);  //html就是外面传进来的html代码
        //        hdc = (HTMLDocumentClass)doc4;
        //        parseLogResult();
        //    }
        //}
    }
}
