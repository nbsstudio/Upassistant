﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace UPassistant.UI
{
    /// <summary>
    /// ColorPicker.xaml 的交互逻辑
    /// </summary>
    public partial class ColorPicker : UserControl
    {
        public delegate void colorChanged(Color color);
        public event colorChanged ColorChanged;
        public ColorPicker()
        {
            InitializeComponent();
        }
        public void setFill(string hex_color)
        {
            System.Drawing.Color tempc = System.Drawing.ColorTranslator.FromHtml(hex_color);
            Color xtempc = Color.FromRgb(tempc.R, tempc.G, tempc.B);
            if (string.IsNullOrWhiteSpace(hex_color))
            {
                xtempc = Colors.White;
            }
            setColour(xtempc);
        }
        public void setColour(Color colourbrush)
        {
            SolidColorBrush scb = new SolidColorBrush();
            if (colourbrush.Equals(Colors.White))
            {
                scb.Color = Colors.Transparent;
            }
            else
            {
                scb.Color = colourbrush;
            }
            ColourPickerR.Fill = scb;
        }
        public Brush Fill
        {
            get { return ColourPickerR.Fill; }
            set { ColourPickerR.Fill = value; }
        }

        private void ColourPicker_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.ColorDialog colorDialog = new System.Windows.Forms.ColorDialog();
            colorDialog.AllowFullOpen = true;
            colorDialog.ShowDialog();
            SolidColorBrush scb = new SolidColorBrush();
            Color color = new Color();
            color.A = colorDialog.Color.A;
            color.B = colorDialog.Color.B;
            color.G = colorDialog.Color.G;
            color.R = colorDialog.Color.R;
            scb.Color = color;
            ColourPickerR.Fill = scb;
            ColorChanged?.Invoke(color);
        }
    }
}
