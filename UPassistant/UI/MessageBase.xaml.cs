﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using UPassistant.Models;
using UPassistant.ViewModels.Opt;

namespace UPassistant.UI
{
    /// <summary>
    /// MessageBase.xaml 的交互逻辑
    /// </summary>
    public partial class MessageBase : UserControl ,ICloneable
    {
        public MessageBase()
        {
            InitializeComponent();
            danMuText.Text = "";
            BaseColor = (SolidColorBrush)userAvatar.Fill;
            BaseOpt = this.Opacity;
            DanMuString = danMuText.Text;
            DefaultSet();
            BaseFontColor = (SolidColorBrush)danMuText.Foreground;
            try
            {
                LoadSystemSetting();
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("新弹幕载入失败", e);
            }
            
        }

        private void DefaultSet()
        {
            BaseTheme = new ThemeInfo();
            EnableBubble = true;
            TextLeftMargin = 10;
            TextTopMargin = 10;
            //BaseColor = new SolidColorBrush(new Color { R=0,G=0,B=0});
        }


        public void LoadSystemSetting() {
            BaseTheme =App.uic.NewDDMTheme;
            danMuText.Foreground = BaseFontColor;
            TextTopMargin = BaseTheme.TextTopMargin;
        }

        #region 属性
        private SolidColorBrush baseColor;
        private double SGWIDTH =-1;
        private double SGHEIGHT = 0;

        public SolidColorBrush BaseColor
        {
            get { return baseColor; }
            set {
                baseColor = value;
                userMessageBar.Fill = value;
                userAvatar.Fill = value;
            }
        }
        private int fontSize;

        public int DMFontSize
        {
            get { return fontSize; }
            set { fontSize = value; danMuText.FontSize = value; nickNameText.FontSize = value; }
        }

        private FontFamily font;

        public FontFamily DMFont
        {
            get { return font; }
            set {
                if (value!=null)
                {
                    font = danMuText.FontFamily = nickNameText.FontFamily = value;
                }
            }
        }
        private double baseOpt;

        public double BaseOpt
        {
            get { return baseOpt; }
            set {
                baseOpt = value;
                this.Opacity = value;
            }
        }

        public SolidColorBrush BaseFontColor
        {
            get { return new SolidColorBrush( BaseTheme.BaseFontColor); }
            set {
                BaseTheme.BaseFontColor = value.Color;
                danMuText.Foreground = value;
            }
        }
        private ImageBrush avatarBorder;

        public ImageBrush AvatarBorder
        {
            get { return avatarBorder; }
            set { avatarBorder = value;userAvatarL3.Fill = value; }
        }
        private ImageBrush avatarImage;

        public ImageBrush AvatarImage
        {
            get { return avatarImage; }
            set { avatarImage = value; userAvatar.Fill = value; }
        }

        private ImageBrush messageBorder;

        public ImageBrush MessageBorder
        {
            get { return messageBorder; }
            set { messageBorder = value; }
        }

        private string danMuString;

        public string DanMuString
        {
            get { return danMuString; }
            set { danMuString = value; danMuText.Text = value; if (nickName!=null && value != null) { AutoTransform(nickName.Length+value.Length); } }
        }

        private string nickName;
        public string NickName
        {
            get { return nickName; }
            set
            {
                nickName = value; nickNameText.Text = value;
                if (value!=null)
                {
                    AutoTransformNickname(value);
                }
            }
        }
        public double TextTopMargin
        {
            get { BaseTheme.TextTopMargin = (double)danMuText.GetValue(Canvas.TopProperty); return BaseTheme.TextTopMargin; }
            set {
                BaseTheme.TextTopMargin = value;
                danMuText.SetValue(Canvas.TopProperty, BaseTheme.TextTopMargin);
                nickNameText.SetValue(Canvas.TopProperty, BaseTheme.TextTopMargin);
            }
        }
        public double TextLeftMargin
        {
            get { BaseTheme.TextLeftMargin = (double)nickNameText.GetValue(Canvas.LeftProperty); return BaseTheme.TextLeftMargin; }
            set {
                BaseTheme.TextLeftMargin = value;
                danMuText.SetValue(Canvas.LeftProperty, BaseTheme.TextLeftMargin+55);
                nickNameText.SetValue(Canvas.LeftProperty, BaseTheme.TextLeftMargin);
            }
        }
        private bool enableBubble = false;

        public bool EnableBubble
        {
            get { return enableBubble; }
            set {
                enableBubble = value;
                if (value)
                {
                    SwitchToBubble();
                }
                else
                {
                    SwitchToNlm();
                }
            }
        }

        public ThemeInfo BaseTheme { get; set; }
        public double CHeight { get { return SGHEIGHT; } }
        #endregion

        /// <summary>
        /// 自动变换矩形
        /// </summary>
        /// <param name="dm"></param>
        private void AutoTransform(int dmLen)
        {
            try
            {
                
                if (enableBubble)
                {
                    double width = 0;
                    if (DMFontSize>=12)
                    {
                        width = DMFontSize * dmLen - 3;
                    }
                    else
                    {
                        width = DMFontSize * dmLen;
                    }
                    Width = SGWIDTH * 2 + width;
                    //将九宫格横向拉伸

                    for (int i = 0, j = 0; i < 9; i++)
                    {
                        if (mbStyleCanvas.Children[i].GetType() == typeof(Grid))
                        {
                            Grid mbb = (Grid)mbStyleCanvas.Children[i];
                            //中部
                            if (j == 1 || j == 4 || j == 7)
                            {
                                Canvas.SetLeft(mbb, SGWIDTH);
                                mbb.Width = width;
                            }
                            //右部
                            if (j == 2 || j == 5 || j == 8)
                            {
                                Grid pbb = (Grid)mbStyleCanvas.Children[i - 1];
                                double pwidth = Canvas.GetLeft(pbb) + pbb.Width;
                                Canvas.SetLeft(mbb, pwidth);
                            }
                            j++;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("计算界面错误",e);
            }
        }

        private void AutoTransformNickname(string nickname)
        {
            nickNameText.Width = DMFontSize * nickname.Length;
            danMuText.SetValue(Canvas.LeftProperty, CalcFontWidth(nickname));
        }
        /// <summary>
        /// 切换到九宫格模式
        /// </summary>
        private void SwitchToBubble()
        {
            userMessageBar.Visibility = userAvatar.Visibility = userAvatarL2.Visibility = userAvatarL3.Visibility = System.Windows.Visibility.Hidden;
            mbStyleCanvas.Visibility = System.Windows.Visibility.Visible;
            
            if (BaseTheme.MBStyle != null)
            {
                mbStyleCanvas.Children.Clear();
                int i=0;
                foreach (GridMB gridMB in BaseTheme.MBStyle)
                {
                    Grid image = new Grid();
                    if (SGWIDTH==-1)
                    {
                        //初始化单个图像的宽度常量
                        SGWIDTH = gridMB.W;
                    }
                    if (i==0||i==3||i==6)
                    {
                        SGHEIGHT += gridMB.H;
                    }
                    image.Width = image.MaxWidth = gridMB.W;
                    image.Height = image.MaxHeight = gridMB.H;
                    image.SetValue(Canvas.LeftProperty, gridMB.X);
                    image.SetValue(Canvas.TopProperty, gridMB.Y);
                    if (gridMB.BackgroundColor != null && gridMB.BackgroundColor != new Color { A = 0, R = 0, G = 0, B = 0, ScA = 0, ScR = 0, ScG = 0, ScB = 0 })
                    {
                        image.Background = new SolidColorBrush(gridMB.BackgroundColor);
                    }
                    else
                    {
                        ImageBrush imageBrush = new ImageBrush();
                        imageBrush.ImageSource = BitmapResourceBucket.TryTakeBitmapImage(gridMB.ImagePath);
                        imageBrush.Stretch = gridMB.ImageStretch;
                        image.Background = imageBrush;
                    }
                    //插入图像
                    mbStyleCanvas.Children.Add(image);
                    i++;
                }
            }
        }

        private double CalcFontWidth(string text)
        {
            try
            {
                System.Drawing.Font f = new System.Drawing.Font(nickNameText.FontFamily.ToString(), DMFontSize, System.Drawing.FontStyle.Regular);
                System.Drawing.Size sif = System.Windows.Forms.TextRenderer.MeasureText(text, f, new System.Drawing.Size(0, 0), System.Windows.Forms.TextFormatFlags.NoPadding);
                return sif.Width-10;
            }
            catch (Exception e)
            {
                LogHelper.WirteLog("计算字体长度错误",e);
                return 55;
            }
            
        }

        /// <summary>
        /// 切换至新模式
        /// </summary>
        private void SwitchToNlm()
        {
            userMessageBar.Visibility = userAvatar.Visibility = userAvatarL2.Visibility = userAvatarL3.Visibility = System.Windows.Visibility.Visible;
            mbStyleCanvas.Visibility = System.Windows.Visibility.Hidden;
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }
        public MessageBase Clone()
        {
            return (MessageBase)this.MemberwiseClone();
        }
    }

    [Serializable()]
    public class GridMB
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double W { get; set; }
        public double H { get; set; }
        public string ImagePath { get; set; }
        public Stretch ImageStretch { get; set; }
        public Color BackgroundColor { get; set; }
    }

    [Serializable()]
    public class ThemeInfo
    {
        public List<GridMB> MBStyle { get; set; }
        public double TextTopMargin { get; set; }
        public double TextLeftMargin { get; set; }
        public Color BaseFontColor { get; set; }
        public string Name { get; set; }
    }
}
