﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UPassistant.UI
{
    /// <summary>
    /// DanMuSortControl.xaml 的交互逻辑
    /// </summary>
    public partial class DanMuSortControl : UserControl
    {
        public DanMuSortControl()
        {
            InitializeComponent();
        }
        private string sortValue = "";
        private bool isLoaded = false;
        public string SortValue
        {
            get { return sortValue; }
            set {
                sortValue = value;
                if (value == "BA" && label.Margin.Left == 0 )//传进来的顺序和界面上的顺序相反，界面就要改过来
                {
                    correct();           
                }
            }
        }
        private void labe2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            changeSide();
        }

        private void changeSide()
        {
            Thickness A, B, C;
            double DA, DB;
            A = label.Margin;
            DA = label.ActualWidth;
            B = labe3.Margin;
            DB = labe3.ActualWidth;
            C = labe2.Margin;
            if (A.Left == 0)
            {
                C.Left = 0 + DB;
                B.Left = C.Left + labe2.ActualWidth + 2;
            }
            else
            {
                C.Left = 0 + DA;
                A.Left = C.Left + labe2.ActualWidth + 2;
            }
            labe3.Margin = A;
            label.Margin = B;
            labe2.Margin = C;
            sortValue = label.Margin.Left > 0 ? "BA" : "AB";
        }

        private void correct()
        {
            Thickness A, B, C;
            A = new Thickness { Left=100};
            B = new Thickness { Left = 70 };
            C = new Thickness { Left = 0 };
            label.Margin = A;
            labe2.Margin = B;
            labe3.Margin = C;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            isLoaded = true;
        }

        private void unmBtn_Click(object sender, RoutedEventArgs e)
        {
            new UserNameMapConfigWindow().ShowDialog();
        }
    }
}
