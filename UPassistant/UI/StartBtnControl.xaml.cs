﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace UPassistant.UI
{
    /// <summary>
    /// StartBtnControl.xaml 的交互逻辑
    /// </summary>
    public partial class StartBtnControl : UserControl
    {
        public StartBtnControl()
        {
            InitializeComponent();
            Enable = true;
            StartButton.Fill = new SolidColorBrush(Colors.White);
        }
        private bool enable;
        private int rpc;
        private double ans = 0.3;
        public int RPC
        {
            get { return rpc; }
            set { rpc = value;
                ShowRPC1.Visibility = Visibility.Visible;
                string rs = value.ToString();
                string show = rs;
                if (rs.Length>=4)
                {
                    show = rs.Substring(0, rs.Length - 3)+"千";
                }
                if (rs.Length >= 5)
                {
                    show = rs.Substring(0, rs.Length - 4) + "万";
                }
                ShowRPC1.Content = show + "人";
               
            }
        }

        public bool Enable
        {
            get { return enable; }
            set { enable = value;
                if (value)
                {
                    StartButton.Fill = new SolidColorBrush(Colors.White);
                }
                else
                {
                    StartButton.Fill = new SolidColorBrush(Colors.LightGray);
                }
            }
        }

        public void SetRoomID(string id)
        {
            ShowRid.Content = id;
            App.egc.RoomID = id;
        }
        public string GetRoomID()
        {
            return ShowRid.Content.ToString();
        }
        public void PressStart()
        {
            
            StartButton.Fill = new SolidColorBrush(Colors.Green);
            DoubleAnimation daV = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromSeconds(ans)));
            ShowRid.BeginAnimation(OpacityProperty, daV);
            StopButton.Visibility = Visibility.Visible;
        }
        public void LinkOkey()
        {
            StartButton.Fill = new SolidColorBrush(Colors.White);
            DoubleAnimation daV = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromSeconds(ans)));
            ShowRid.BeginAnimation(UIElement.OpacityProperty, daV);
            ShowRPC1.Visibility = Visibility.Hidden;
            StopButton.Visibility = Visibility.Hidden;
        }
        public void HideStartButtonMask()
        {
            DoubleAnimation dav = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromSeconds(0.5)));
            StartButtonMask.BeginAnimation(UIElement.OpacityProperty, dav);
        }
    }
}
