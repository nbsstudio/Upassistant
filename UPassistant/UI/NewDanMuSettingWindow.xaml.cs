﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using UPassistant.Models;

namespace UPassistant.UI
{
    /// <summary>
    /// NewDanMuSettingWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NewDanMuSettingWindow : Window
    {
        private bool loaded = false;
        public NewDanMuSettingWindow()
        {
            InitializeComponent();
            //baseColorSet.ColorChanged += BaseColorSet_ColorChanged;
            fontColorSet.ColorChanged += FontColorSet_ColorChanged;
            LoadStyle();
            loaded = true;
        }

        private void LoadStyle()
        {
            if (App.uic.DMStyle != null)
            {
                dmStyleComboBox.ItemsSource = App.uic.DMStyle;
                for (int i = 0; i < App.uic.DMStyle.Count; i++)
                {
                    if (App.uic.DMStyle[i].IsCurrentUsed)
                    {
                        dmStyleComboBox.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        private void FontColorSet_ColorChanged(Color color)
        {
            reviewBox.BaseFontColor = new SolidColorBrush(color);
        }

        private void BaseColorSet_ColorChanged(Color color)
        {
            reviewBox.BaseColor = new SolidColorBrush(color);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //baseColorSet.setColour(reviewBox.BaseColor.Color);
            fontColorSet.setColour(reviewBox.BaseFontColor.Color);
            reviewBox.EnableBubble = true;
            if (App.uic.NewDDMTheme != null)
            {
                vLayoutSetBox.Text = App.uic.NewDDMTheme.TextTopMargin.ToString();
                textBox.Text = App.uic.NewDDMTheme.TextLeftMargin.ToString();
            }
        }

        private void FontSizeSetBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int fontSize = 12;
            
            if (int.TryParse(fontSizeSetBox.Text.Trim(), out fontSize))
                reviewBox.DMFontSize = fontSize;
        }
        /// <summary>
        /// 垂直调节
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VLayoutSetBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            double vValue = 12;
            if (double.TryParse(vLayoutSetBox.Text.Trim(), out vValue))
                reviewBox.TextTopMargin = vValue;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveConfig();
        }

        private void SaveConfig()
        {
            App.uic.NewDDMTheme = reviewBox.BaseTheme;
            string json = new JsonHelper().SerializeObject(reviewBox.BaseTheme);
            File.WriteAllText(@".\"+App.uic.FindCurrentSkinDir()+@"\ndm.tjson", json);
        }
        /// <summary>
        /// 水平调节
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            double vValue = 12;
            if (double.TryParse(textBox.Text.Trim(), out vValue))
                reviewBox.TextLeftMargin = vValue;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveConfig();
        }

        private void dmStyleComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (loaded)
            {
                try
                {
                    Entities.DMStyleEntity dMStyleEntity = dmStyleComboBox.SelectedItem as Entities.DMStyleEntity;
                    foreach (var item in App.uic.DMStyle)
                    {
                        item.IsCurrentUsed = dMStyleEntity.ID == item.ID;
                    }
                    App.uic.SaveDMStyleSelection(dMStyleEntity.ID);
                }
                catch (System.Exception ex)
                {
                    LogHelper.WirteLog("修改弹幕样式异常",ex);
                }
            }
        }
    }
}
