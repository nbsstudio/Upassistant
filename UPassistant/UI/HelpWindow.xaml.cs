﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UPassistant.UI
{
    /// <summary>
    /// HelpWindow.xaml 的交互逻辑
    /// </summary>
    public partial class HelpWindow : Window
    {
        private static HelpWindow self;
        private HelpWindow()
        {
            InitializeComponent();
        }

        public static HelpWindow getInstance()
        {
            if (self ==null)
            {
                self = new HelpWindow();
            }
            return self;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            self = null;
        }

        private void label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://jingyan.baidu.com/article/9158e0003935ada254122801.html");
        }

        private void label1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://jingyan.baidu.com/article/af9f5a2d1c2b4743140a4501.html");
        }

        private void label3_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Window tips = new Window();
            tips.Style = (Style)App.Current.FindResource("AttentionWindow1");
            tips.Show();
        }

        private void ReportBtn_Click(object sender, RoutedEventArgs e)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Bug反馈器.exe";
            Process p = new Process();
            if (File.Exists(path))
            {
                p.StartInfo = new ProcessStartInfo(path, "-t nacgame");
                try
                {
                    p.Start();
                }
                catch (Exception ex)
                {
                    Models.LogHelper.WirteLog("启动反馈器错误", ex);
                }
            }
        }

        private void label4_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.bilibili.com/video/av20663956/");
        }
    }
}
