﻿using CodyDanMu;
using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media.Animation;

namespace UPassistant.UI
{
    /// <summary>
    /// WellcomBaord.xaml 的交互逻辑
    /// </summary>
    public partial class WellcomBaord : Window
    {
        private System.Timers.Timer timer;
        private bool exit = false;
        private DanmuInfo danmuInfo;
        public WellcomBaord(DanmuInfo danmuInfo)
        {
            InitializeComponent();
            Canvas.SetTop(MatherBoard, 150);
            this.danmuInfo = danmuInfo;
            int ScreenWidth = Screen.PrimaryScreen.WorkingArea.Width;
            int ScreenHeight = Screen.PrimaryScreen.WorkingArea.Height;
            //计算窗体显示的坐标值，可以根据需要微调几个像素
            double x = ScreenWidth - Width - 5;
            double y = ScreenHeight - Height - 5;
            this.Left = x;
            this.Top = y;
        }

        public void Poup()
        {
            Dispatcher.Invoke(new Action(() =>
            {
                Show();
                Who.Text = danmuInfo.nickname;
            }));
            MoveTo(new Point { X = 0, Y = 0 });
            timer = new System.Timers.Timer(5000);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            exit = true;
            MoveTo(new Point { X = 0, Y = 150 });
            timer.Stop();
        }

        private void MoveTo(Point deskPoint)
        {
            //Point p = e.GetPosition(body); 
            Dispatcher.Invoke(new Action(()=> {

                Point curPoint = new Point
                {
                    X = Canvas.GetLeft(MatherBoard),
                    Y = Canvas.GetTop(MatherBoard)
                };

                //double _s = System.Math.Sqrt(Math.Pow((deskPoint.X - curPoint.X), 2) + Math.Pow((deskPoint.Y - curPoint.Y), 2));

                //double _secNumber = (_s / 1000) * 500;
                double _secNumber = 1 * 1000;
                Storyboard storyboard = new Storyboard();

                //创建X轴方向动画 
                DoubleAnimation doubleAnimation = new DoubleAnimation(
                  Canvas.GetLeft(MatherBoard),
                  deskPoint.X,
                  new Duration(TimeSpan.FromMilliseconds(_secNumber))

                );
                Storyboard.SetTarget(doubleAnimation, MatherBoard);
                Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath("(Canvas.Left)"));
                storyboard.Children.Add(doubleAnimation);

                //创建Y轴方向动画 

                doubleAnimation = new DoubleAnimation(
                  Canvas.GetTop(MatherBoard),
                  deskPoint.Y,
                  new Duration(TimeSpan.FromMilliseconds(_secNumber))
                );
                Storyboard.SetTarget(doubleAnimation, MatherBoard);
                Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath("(Canvas.Top)"));
                storyboard.Children.Add(doubleAnimation);
                //动画播放 
                storyboard.Begin();
                storyboard.Completed += Storyboard_Completed;

            }));
            
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (exit)
            {
                Close();
            }
        }

        private void WellcomBoard_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
