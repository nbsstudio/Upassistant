﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace LiveUpdate
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{

        public static bool Is64Bit
        {
            get;set;
        }

        /// <summary>
        /// Program entry point.
        /// </summary>
        [STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
            Is64Bit = Environment.Is64BitOperatingSystem;
			Application.Run(new MainForm());
		}
		
	}
}
