﻿/*
 * 由SharpDevelop创建。
 * 用户： notebook
 * 日期: 2015/5/14
 * 时间: 11:51
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using Netios;
using System.Text.RegularExpressions;
using System.IO.Compression;
using LiveUpdate.BaseClase;
using Microsoft.Win32;

namespace LiveUpdate
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class MainForm : Form
    {
        private string path = Application.StartupPath;
        private const string local = "http://localhost:8080";
        private const string backupZipFilePath = @".\更新前备份.zip";
        private bool cpModel = false;

        BaseClase.NewListUpdateClass nluc = new BaseClase.NewListUpdateClass();
        public MainForm()
        {
            InitializeComponent();
            UiInt();
            GetRegeditForType();
            GetCpModel();
        }

        private static void GetRegeditForType()
        {
            try
            {
                //读取注册表信息
                using (RegistryKey currentKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{4224AA82-6D20-44CA-925D-F2AB71E4DDD2}_is1", false))
                {
                    if (currentKey != null)
                    {
                        string keyValue = currentKey.GetValue("Inno Setup: Setup Type").ToString();
                        if (keyValue == "x64")
                        {
                            Program.Is64Bit = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                //加入程序没法在注册表找到用户安装的UP助手是否64位，那么在LoadMath()方法中调用的GetNowVersion()方法也可以获取得到
            }
        }

        private void GetCpModel()
        {
            try
            {
                UpdateClass updateClass = new UpdateClass();
                updateClass.LoadMath();
                if (int.Parse(updateClass.NewEdition) - int.Parse(updateClass.OldEdition) > 1)
                {
                    cpModel = true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WirteLog("跳级更新查询失败", ex);
            }

        }

        /// <summary>        
        /// c#.net 下载文件        
        /// </summary>        
        /// <param name="URL">下载文件地址</param>       
        /// 
        /// <param name="Filename">下载后的存放地址</param>        
        /// <param name="Prog">用于显示的进度条</param>        
        /// 
        public void DownloadFile(string URL, string filename, ProgressBar prog, Label label1)
        {
            float percent = 0;
            try
            {
                System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
                System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
                long totalBytes = myrp.ContentLength;
                if (prog != null)
                {
                    prog.Maximum = (int)totalBytes;
                }
                System.IO.Stream st = myrp.GetResponseStream();
                System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
                long totalDownloadedByte = 0;
                byte[] by = new byte[1024];
                int osize = st.Read(by, 0, (int)by.Length);
                while (osize > 0)
                {
                    totalDownloadedByte = osize + totalDownloadedByte;
                    System.Windows.Forms.Application.DoEvents();
                    so.Write(by, 0, osize);
                    if (prog != null)
                    {
                        prog.Value = (int)totalDownloadedByte;
                    }
                    osize = st.Read(by, 0, (int)by.Length);

                    percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                    label1.Text = "下载进度" + percent.ToString() + "%";
                    Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
                }
                label1.Text = "下载完成。安装中... ...";
                so.Flush();//将缓冲区内在写入到基础流中
                st.Flush();//将缓冲区内在写入到基础流中
                so.Close();
                st.Close();
            }
            catch (System.Exception)
            {

            }
        }
        /// <summary>
        /// 关闭原有的应用程序 
        /// </summary>
        private void KillProess()
        {
            base.Invoke(new Action(() =>
            {
                label1.Text = "正在关闭程序....";

            }));

            Process[] proc = Process.GetProcessesByName("UPassistant");
            //关闭原有应用程序的所有进程
            foreach (Process pro in proc)
            {
                pro.Kill();
            }
        }
        /// <summary>
        /// 判断是否可以下载
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private bool CanDownload(string path)
        {
            string[] forbiddenlist = new string[] { "Update.exe", "Newtonsoft.Json.dll", "ICSharpCode.SharpZipLib.dll" };
            if (!cpModel && Regex.IsMatch(path, "su.sp", RegexOptions.IgnoreCase))
            {
                return false;//如果非跳级更新，则不下载su.sp包
            }
            foreach (string item in forbiddenlist)
            {
                if (Regex.IsMatch(path, item, RegexOptions.IgnoreCase))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 开始更新
        /// </summary>
        /// <param name="localtest"></param>
        private void UpdateStart(object localtest)
        {
            LogHelper.WirteLog("开始更新...");
            string zipFile = path + @"\update_pack.zip";
            bool IsLocalTest = (bool)localtest;
            //关闭原有的应用程序  
            KillProess();
            MultiServerClass msc = new BaseClase.MultiServerClass();
            string ser = msc.GetAvailableDOMAIN();
            if (IsLocalTest)
            {
                ser = local;
            }
            if (nluc.CheckList(ser))
            {
                //如果版本更新涉及跳级更新，就直接解压su.sp包来更新。
                Invoke(new Action(() =>
                {
                    progressBar1.Maximum = nluc.TodoDwnFile.Count;
                    nluc.TodoDwnFile.ForEach(new Action<BaseClase.Dltype>((BaseClase.Dltype s) =>
                    {
                        curnlabel4.Text = s.Path;
                        if (CanDownload(s.Path))
                        {
                            DownloadFile(s.Url, AppDomain.CurrentDomain.BaseDirectory + s.Path, progressBar2, label1);
                        }
                        progressBar1.Value++;
                    }));
                }));
            }
            else
            {
                MessageBox.Show("更新失败，服务器返回错误信息。请登陆官网查看更新是否可用或了解最新版本的更新计划！", "警告");
            }
            if (cpModel)
            {
                //解压全局更新包
                Decompress(zipFile);
            }
            LogHelper.WirteLog("[LogEnd]");
            Exec();
        }

        private void Decompress(string zipFile)
        {
            //解压
            try
            {
                LogHelper.WirteLog("解压文件:" + zipFile);
                //关闭原有的应用程序  
                KillProess();
                //BaseClase.Zip.UnZip(zipFile, path, "", true, true);
                try
                {
                    if (File.Exists(backupZipFilePath))
                    {
                        File.Delete(backupZipFilePath);
                    }
                    ZipFile.CreateFromDirectory(".", backupZipFilePath);
                }
                catch (Exception)
                {

                }
                try
                {
                    using (FileStream zipFileToOpen = new FileStream(zipFile, FileMode.Open))
                    using (ZipArchive archive = new ZipArchive(zipFileToOpen, ZipArchiveMode.Read))
                    {
                        foreach (var zipArchiveEntry in archive.Entries)
                        {
                            if (zipArchiveEntry.FullName.IndexOf("/") > 0)
                            {
                                if (Directory.Exists(zipArchiveEntry.FullName))
                                {
                                    Directory.Delete(zipArchiveEntry.FullName, true);
                                }
                            }
                            else
                            {
                                if (File.Exists(zipArchiveEntry.FullName))
                                {
                                    File.Delete(zipArchiveEntry.FullName);
                                }
                            }

                        }
                    }
                    ZipFile.ExtractToDirectory(zipFile, "./");

                    File.Delete("SelfUpdate.fff");
                }
                catch (Exception e)
                {
                    LogHelper.WirteLog("错误：" + e.Message);
                    LogHelper.WirteLog("\n");
                    LogHelper.WirteLog("OMG!更新遇到问题了！你可以在软件的目录里面找到备份文件！解压覆盖即可还原哦！\n");
                    LogHelper.WirteLog("按任意键退出！");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("更新失败，请稍后重试！\n\n" + ex.Message, "更新错误");
                LogHelper.WirteLog("下载错误：", ex.Message);
                ChangeToNormal();
            }

        }

        private void Exec()
        {
            if (MessageBox.Show("升级完成！请重新登陆！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            {
                FileInfo file = new FileInfo(path + @"\UPassistant.exe");//文件地址
                if (file.Exists)
                {
                    Process.Start(path + @"\UPassistant.exe");
                }
                Application.Exit();
            }
        }

        void BtnDownClick(object sender, EventArgs e)
        {

            ChangeToUpdateProc();
            Thread th = new Thread(UpdateStart);
            th.Start(false);
        }
        void Button2Click(object sender, EventArgs e)
        {

        }
        private const string oldp = "LiveUpdate.exe";
        /// <summary>
        /// 清理旧的更新程序
        /// </summary>
        private void CleanOldP()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + oldp;
            if (System.IO.File.Exists(path))
            {
                File.Delete(path);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (Program.Is64Bit)
            {
                Text = Text + "(x64)";
            }
            CleanOldP();
            NetHTTP be = new NetHTTP();
            be.PubDoResponse(new PostItem { GOP = "GET", postUrl = "" }, System.Text.Encoding.UTF8);

        }
        private void UiInt()
        {
            tabControl1.SelectedTab = tabPage1;
            tabControl1.Size = new System.Drawing.Size(this.Width + 4, this.Height + 4);
        }
        private void ChangeToUpdateProc()
        {
            tabControl1.SelectedTab = tabPage2;
            this.Height = 300;
        }

        private void ChangeToNormal()
        {
            base.Invoke(new Action(() =>
            {
                tabControl1.SelectedTab = tabPage1;
                label1.Text = "更新软件：";
                btnDown.Enabled = true;
                this.Height = 541;
            }));
        }
        private void MainForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Z)
            {
                ChangeToUpdateProc();
                UpdateStart(true);
            }
        }

        private void TabControl1_KeyDown(object sender, KeyEventArgs e)
        {
            MainForm_KeyUp(sender, e);
        }
    }
}
