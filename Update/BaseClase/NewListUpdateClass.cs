﻿using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LiveUpdate.BaseClase
{
    public class NewListUpdateClass
    {
        /*
        *json格式
        *
        *{
        *   "result": 200,
        *   "status": "update",
        *   "data": [{"url": "",path: ""}]
        *}
        */

        public List<Dltype> TodoDwnFile;
        Netios.UpdateClass uc = new Netios.UpdateClass();
        public bool CheckList(string server)
        {
            bool canupdateonlist = false;
            try
            {
                Netios.NetHTTP be = new Netios.NetHTTP();
                string nowVer = uc.GetNewVerCode(server);

                //对64位系统的特殊处理
                string url = server + "/version/index/getupdatelist2/soft/RSDS/id/" + nowVer;
                if (Program.Is64Bit)
                {
                    //DialogResult rs = MessageBox.Show("检测到你正在使用64位的操作系统，需要下载对应的更新吗？","警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //if (rs ==  DialogResult.Yes)
                    //{
                    //2018.04.17开始，自动检查注册表中的卸载标志。
                    url = server + "/version/index/getupdatelist2/soft/RSDS_x64/id/" + nowVer;
                    //}
                }

                string jsonp = be.PubDoGET(url, Encoding.UTF8);
                JObject acd = (JObject)JsonConvert.DeserializeObject(jsonp);
                if (acd["result"].ToString()=="200")
                {
                    if (acd["status"].ToString() == "update" || acd["status"].ToString() == "patch")
                    {
                        canupdateonlist = true;
                        TodoDwnFile = new List<Dltype>();
                        foreach (JObject item in acd["data"])
                        {
                            Dltype dlitem  = JsonConvert.DeserializeObject<Dltype>(item.ToString());
                            dlitem.Url = server + dlitem.Url;//自动追加域名
                            //dltype dlitem = item.ToObject<dltype>();
                            TodoDwnFile.Add(dlitem);
                        }
                    }
                }
            }
            catch (System.Exception)
            {
                canupdateonlist = false;
            }
            return canupdateonlist;
        }
    }
    public class Dltype
    {
        public string Url { get; set; }
        public string Path { get; set; }
    }
}
