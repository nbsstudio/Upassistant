﻿using System.Net.Sockets;
using System.Text;

namespace LiveUpdate.BaseClase
{
    /// <summary>
    /// 封装多服务器地址获取
    /// </summary>
    public class MultiServerClass
    {
        private const string HTTPH = "http://";
        private const string HTTPSH = "https://";
        private const string NACGAME = "www.nacgame.com";
        private const string BACKNACGAME = "nacgame.iask.in";
        private const string JPIP = "45.32.32.6";
        public MultiServerClass()
        {

        }
        /// <summary>
        /// 检查和返回一个可用的更新服务器网址
        /// </summary>
        /// <returns></returns>
        public string GetAvailableDOMAIN()
        {
            string temp = HTTPH+"localhost";
            try
            {
                Netios.NetHTTP nhttp = new Netios.NetHTTP();
                string text = string.Empty;
                for (int i = 0; i < 3; i++)
                {
                    switch (i)
                    {
                        case 0:
                            temp = GetNACGAME();
                            text = nhttp.PubDoGET(temp, Encoding.UTF8);
                            if (!string.IsNullOrEmpty(text))
                            {
                                return temp;
                            }
                            break;
                        case 1:
                            temp = GetBACKNACGAME();
                            text = nhttp.PubDoGET(temp, Encoding.UTF8);
                            if (!string.IsNullOrEmpty(text))
                            {
                                return temp;
                            }
                            break;
                        case 2:
                            temp = GetJPIP();
                            text = nhttp.PubDoGET(temp, Encoding.UTF8);
                            if (!string.IsNullOrEmpty(text))
                            {
                                return temp;
                            }
                            break;
                    }
                }
            }
            catch (System.Exception)
            {
                temp = HTTPH + "localhost";
            }
            return temp;
        }
        public string GetNACGAME()
        {
            return HTTPH+NACGAME;
        }
        public string GetBACKNACGAME()
        {
            return HTTPH + BACKNACGAME;
        }
        public string GetJPIP()
        {
            return HTTPH + JPIP;
        }
    }
}
