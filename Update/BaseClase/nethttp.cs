﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Netios
{
    public class NetHTTP
    {
        public string PubDoGET(string url, Encoding _encode)
        {

            string result = "";
            if (url != null || url != string.Empty)
            {
                try
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] data = encoding.GetBytes("");
                    HttpWebRequest myRequest =
                    (HttpWebRequest)WebRequest.Create(url);
                    myRequest.Method = "GET";
                    myRequest.ContentType = "application/x-www-form-urlencoded";
                    myRequest.ContentLength = data.Length;
                    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                    StreamReader reader = new StreamReader(myResponse.GetResponseStream(), _encode);
                    result = reader.ReadToEnd();
                    result = result.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    int status = (int)myResponse.StatusCode;
                    reader.Close();

                }
                catch (Exception)
                {

                }
            }
            return result;
        }
        public string PubDoPOST(string url, string POdata, Encoding _encode)
        {

            string result = "";
            if (url != null || url != string.Empty)
            {
                try
                {
                    //ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] data = _encode.GetBytes(POdata);
                    HttpWebRequest myRequest =
                    (HttpWebRequest)WebRequest.Create(url);
                    myRequest.Method = "POST";
                    myRequest.ContentType = "application/x-www-form-urlencoded";
                    myRequest.ContentLength = data.Length;
                    Stream newStream = myRequest.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                    StreamReader reader = new StreamReader(myResponse.GetResponseStream(), _encode);
                    result = reader.ReadToEnd();
                    result = result.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    int status = (int)myResponse.StatusCode;
                    reader.Close();

                }
                catch (Exception)
                {

                }
            }
            return result;
        }
        public string PubDoResponse(PostItem POdata, Encoding _encode)
        {

            string result = "";
            if (POdata.postUrl != null || POdata.postUrl != string.Empty)
            {
                try
                {
                    //ASCIIEncoding encoding = new ASCIIEncoding();
                    
                    byte[] data = _encode.GetBytes(POdata.postData);
                    if (POdata.GOP == "GET")
                    {
                        data = _encode.GetBytes("");
                    }
                    HttpWebRequest myRequest =
                    (HttpWebRequest)WebRequest.Create(POdata.postUrl);
                    myRequest.Method = POdata.GOP;
                    myRequest.ContentType = POdata.Datatype;
                    myRequest.ContentLength = data.Length;
                    if (!string.IsNullOrEmpty(POdata.Refer))
                    {
                        myRequest.Referer = POdata.Refer;
                    }
                    Stream newStream = myRequest.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();
                    HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                    StreamReader reader = new StreamReader(myResponse.GetResponseStream(), _encode);
                    result = reader.ReadToEnd();
                    result = result.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    int status = (int)myResponse.StatusCode;
                    reader.Close();

                }
                catch (Exception)
                {

                }
            }
            return result;
        }
    }
    public struct PostItem
    {
        public string postData;
        public string postUrl;
        private string _GOP;
        public string GOP {
            get {
                return this._GOP;
            }
            set {
                if (value=="GET"||value=="POST")
                {
                    this._GOP = value;
                }
                else
                {
                    this._GOP = "GET";
                }
            }
        }
        public string Refer;
        private string datatype;
        public string Datatype
        {
            get
            {
                if (string.IsNullOrEmpty(datatype))
                {
                    datatype = "application/x-www-form-urlencoded";
                }
                return this.datatype;
            }
            set
            {
                if (value.IndexOf("application/") == 0)
                {
                    this.datatype = value;
                }
                else
                {
                    this.datatype = "application/x-www-form-urlencoded";
                }
            }
        }
    }
}
