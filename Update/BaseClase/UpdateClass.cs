﻿using LiveUpdate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Netios
{
    public class UpdateClass
    {
        public string OldEdition { get; set; }
        public string NewEdition { get; set; }
        private const string NowUpdateProgram = "Update.exe";
        public UpdateClass()
        {
            OldEdition = "1001";
            NewEdition = "1002";
        }
        public string GetNowVersion()
        {
            //获取系统中xml里面存储的版本号               
            String fileName = AppDomain.CurrentDomain.BaseDirectory + "XMLEdition.xml";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(fileName);
            XmlNode xn = xmlDoc.SelectSingleNode("/Content/Project");
            XmlElement xe = (XmlElement)xn;
            if (xe.GetAttribute("id") == "RSDS")
            {
                OldEdition = xe.GetAttribute("Edition");//动态数组
                if (xe.HasAttribute("Is64BitProcess"))
                {
                    Program.Is64Bit = xe.GetAttribute("Is64BitProcess")== "True";
                }
                //XmlElement nxe = xe;
                //nxe.SetAttribute("Edition", NewEdition);
                //xmlDoc.SelectSingleNode("/Content").ReplaceChild(nxe, xe);
                //xmlDoc.Save(fileName);
            }
            return OldEdition;
        }
        public string GetNewVerCode(string server)
        {
            string NewEdition = this.OldEdition;//初始化返回的版本号为现在的版本号，以免在网上找不到新的版本号产生错误。
            try
            {
                NetHTTP bes = new NetHTTP();
                string pageHtml = bes.PubDoGET(server+"/index.php?s=/Version/index/versionDetail/swn/rsds", Encoding.UTF8);
                String fileName = AppDomain.CurrentDomain.BaseDirectory + @"WebGetEdition.xml";
                StreamWriter sw = new StreamWriter(fileName);
                sw.Write(pageHtml);
                sw.Close();
                //获取系统中xml里面存储的版本号 
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(fileName);
                XmlNode xn = xmlDoc.SelectSingleNode("/Content/Project");
                XmlElement xe = (XmlElement)xn;
                if (xe.GetAttribute("id") == "RSDS")
                {
                    NewEdition = xe.GetAttribute("Edition");//动态数组
                    this.NewEdition = NewEdition;
                }
            }
            catch (WebException webEx)
            {
                MessageBox.Show(webEx.Message.ToString());
            }
            return NewEdition;
        }
        public void LoadMath()
        {
            //服务器上的版本号
            string NewEdition = "1002";
            //应用程序中的版本号

                //本地版本号
                this.GetNowVersion();
                //服务器上的版本号
                //NewEdition = GetNewVerCode();
                double newE = double.Parse(NewEdition);
                double oldE = double.Parse(OldEdition);
                ////比较两个版本号，判断应用程序是否要更新
                //if (newE > oldE)
                //{
                //    //更新程序¨

                //    //打开下载窗口
                //    // Application.Run(new DownUpdate());

                //    //启动安装程序                        
                //    System.Diagnostics.Process.Start(AppDomain.CurrentDomain.BaseDirectory + NowUpdateProgram);
                //    //Environment.Exit(0);//强制退出
                //}
        }
    }
}
