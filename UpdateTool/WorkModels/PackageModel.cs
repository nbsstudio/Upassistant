﻿using Ionic.Zip;
using log4net;
using System;
using System.IO;
using System.Text;

namespace UpdateTool.WorkModels
{
    /// <summary>
    /// 打包模块
    /// </summary>
    public class PackageModel
    {
        private static ILog log = LogManager.GetLogger(typeof(PackageModel));
        public bool FullPack(Entity.MainConfig config)
        {
            try
            {
                for (int z = 0; z < 2; z++)
                {
                    //读取所有文件
                    using (ZipFile zip = new ZipFile())
                    {
                        string currentDir = z==0?config.Bin32Path:config.Bin64Path;
                        DirectoryInfo di = new DirectoryInfo(currentDir);
                        FileInfo[] fis = di.GetFiles();
                        DirectoryInfo[] dis = di.GetDirectories();
                        //组装zip
                        for (int i = 0; i < fis.Length; i++)
                        {
                            string useDirName = "";
                            for (int j = 0; j < dis.Length; j++)
                            {
                                string dirName = dis[j].FullName.Replace(currentDir, "");
                                if (fis[i].FullName.Contains(dirName))
                                {
                                    useDirName = dirName;
                                    break;
                                }
                            }
                            if (fis[i].FullName.Contains("deploy.zip") || fis[i].FullName.Contains("su.sp"))
                            {
                                continue;
                            }
                            zip.AddFile(fis[i].FullName, useDirName);
                        }
                        //写入zip
                        zip.AlternateEncodingUsage = ZipOption.Always;
                        zip.AlternateEncoding = Encoding.UTF8;
                        zip.Save(currentDir + "\\su.sp");
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                log.Warn(e);
                return false;
            }
            
        }

        /// <summary>
        /// 打包可以发布更新的包
        /// </summary>
        /// <param name="config"></param>
        /// <param name="paths"></param>
        /// <returns></returns>
        public bool DeployPack(Entity.MainConfig config,string[] paths)
        {
            try
            {
                for (int z = 0; z < 2; z++)
                {
                    //读取所有文件
                    using (ZipFile zip = new ZipFile())
                    {
                        string currentDir = z == 0 ? config.Bin32Path : config.Bin64Path;
                        DirectoryInfo di = new DirectoryInfo(currentDir);
                        FileInfo[] fis = di.GetFiles();
                        DirectoryInfo[] dis = di.GetDirectories();
                        for (int i = 0; i < fis.Length; i++)
                        {
                            string useDirName = "";
                            for (int j = 0; j < dis.Length; j++)
                            {
                                string dirName = dis[j].FullName.Replace(currentDir, "");
                                if (fis[i].FullName.Contains(dirName))
                                {
                                    useDirName = dirName;
                                    break;
                                }
                            }
                            bool accept = false;
                            foreach (string item in paths)
                            {
                                if (item!=null && fis[i].FullName.Contains(item))
                                {
                                    accept = true;
                                    break;
                                }
                            }
                            if (accept)
                            {
                                zip.AddFile(fis[i].FullName, useDirName);
                            }
                            
                        }

                        zip.AlternateEncodingUsage = ZipOption.Always;
                        zip.AlternateEncoding = Encoding.UTF8;
                        zip.Save(currentDir + "\\deploy.zip");
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                log.Warn(e);
                return false;
            }
        }
    }

}
