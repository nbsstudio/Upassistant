﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UpdateTool.Entity;

namespace UpdateTool.WorkModels
{
    /// <summary>
    /// 工作流
    /// </summary>
    public class WorkFlow
    {
        private bool processing = false;
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(WorkFlow));
        public int Step { get; set; }

        internal void Start(MainConfig mainConfig, List<FileItem> list,Entity.WorkMode workMode)
        {
            if (processing)
            {
                return;
            }
            processing = true;
            //先全量打包
            log.Info("全量打包开始...");
            Step = 1;
            bool res = new PackageModel().FullPack(mainConfig);
            Step = 2;
            //在将全量包和本次增量一起再打包
            string[] d = new string[list.Count];
            if (mainConfig.AutoCheckDiff)
            {
                //自动配置，对比出文件的指纹，以辨别哪些需要加入到zip包
            }
            else
            {
                //手工配置，从界面选中需要加入的文件路径
                int j = 0;
                //筛选出需要保留的文件
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Checked)
                    {
                        string mPath = list[i].Path.Replace(mainConfig.Bin32Path,"");//去掉发布目录部分
                        d[j] = mPath;
                        j++;
                    }
                }
                Array.Resize<string>(ref d, j);
            }
           
            bool res2 = new PackageModel().DeployPack(mainConfig,d);
            if (workMode == WorkMode.PACK_AND_UPLOAD)
            {
                Step = 3;
                //TODO: 通过接口上传服务器
            }
            Step = 4;
            
            processing = false;
        }
    }
}
