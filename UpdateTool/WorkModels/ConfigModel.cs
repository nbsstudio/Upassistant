﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using log4net;

namespace UpdateTool.WorkModels
{
    /// <summary>
    /// 配置读写模块
    /// </summary>
    public class ConfigModel
    {
        private static ILog log = LogManager.GetLogger(typeof(ConfigModel));
        public static Entity.MainConfig ReadFile()
        {
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory + @"config.json";
                string content = File.ReadAllText(path);
                Entity.MainConfig mc = JsonConvert.DeserializeObject<Entity.MainConfig>(content);
                return mc;
            }
            catch (Exception e)
            {
                log.Error("读取配置错误",e);
                return null;
            }
            
        }
    }
}
