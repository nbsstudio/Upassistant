﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateTool.Entity
{
    public class MainConfig
    {
        /// <summary>
        /// 32位程序发布目录
        /// </summary>
        public string Bin32Path { get; set; }
        /// <summary>
        /// 64位程序发布目录
        /// </summary>
        public string Bin64Path { get; set; }
        /// <summary>
        /// 是否自动检查不同项
        /// </summary>
        public bool AutoCheckDiff { get; set; }
        /// <summary>
        /// 不对64位进行部署
        /// </summary>
        public bool DontDoAnyThinkFor64 { get; set; }

    }
}
