﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UpdateTool.WorkModels;

namespace UpdateTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Init();
        }
        private ObservableCollection<Entity.FileItem> observableCollection = new ObservableCollection<Entity.FileItem>();
        public void Init()
        {
            Entity.MainConfig mainConfig = ConfigModel.ReadFile();
            ScanFiles(mainConfig);
            PDataGrid.ItemsSource = observableCollection;
        }

        private void ScanFiles(Entity.MainConfig mainConfig)
        {
            string path = mainConfig.Bin32Path;
            DirectoryInfo Dir = new DirectoryInfo(path);
            DirectoryInfo[] DirSub = Dir.GetDirectories();
            if (DirSub.Length <= 0)
            {
                foreach (FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly)) //查找文件
                {
                    observableCollection.Add(new Entity.FileItem { Path = Dir + @"\" + f.ToString(), Checked = false });
                }

            }


            int t = 1;

            foreach (DirectoryInfo d in DirSub)//查找子目录 
            {
                if (t == 1)
                {

                    foreach (FileInfo f in Dir.GetFiles("*.*", SearchOption.TopDirectoryOnly)) //查找文件
                    {
                        observableCollection.Add(new Entity.FileItem { Path = Dir + @"\" + f.ToString(), Checked = false });

                    }

                    t = t + 1;

                }

            }
        }

        private void PkBtn_Click(object sender, RoutedEventArgs e)
        {
            GoWork(Entity.WorkMode.PACK);
        }

        private void GoWork(Entity.WorkMode workMode)
        {
            WorkFlow wf = new WorkFlow();
            Entity.MainConfig mainConfig = ConfigModel.ReadFile();
            if (mainConfig != null)
            {
                List<Entity.FileItem> list = observableCollection.ToList<Entity.FileItem>();
                list.Add(new Entity.FileItem { Checked = true, Path = "su.sp" });//全量包默认允许放入deploy包
                wf.Start(mainConfig, list, workMode);
                MessageBox.Show("打包完成", "");
            }
            else
            {
                MessageBox.Show("配置无法读取", "致命错误");
            }
        }

        private void PkUBtn_Click(object sender, RoutedEventArgs e)
        {
            GoWork(Entity.WorkMode.PACK_AND_UPLOAD);
        }
    }
}
