﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;

namespace RQM
{
    public class PRDL
    {
        #region 属性
        private int dlot = 20;//下载超时时间
        private Timer timer1 = new Timer();
        private int runningtime = 0;
        private long C_id = 0;
        private string mID;
        private string musicname;
        private PublicLib plb = new PublicLib();
        private bool IsDelegate;
        private string CloudAddress;
        #endregion
        public PRDL(long id, string mid, string name, int _dlot,bool IsDelegate = false,string CloudAddress="")
        {
            C_id = id;
            mID = mid;
            musicname = name;
            timer1.Elapsed += Timer1_Tick;
            timer1.Interval = 1000;
            this.IsDelegate = IsDelegate;
            this.CloudAddress = CloudAddress;
            if (_dlot < 2)
            {
                dlot = 5;
            }
            else
            {
                dlot = _dlot;
            }
            timer1.Start();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            runningtime++;
            if (runningtime >= dlot)
            {
                closethis(musicname + ":音乐下载超时，超出设定的" + dlot + "秒自动放弃！");
            }
        }

        private void closethis(string logmsg = "")
        {
            if (logmsg != "")
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"cache\" + this.mID + ".log"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + @"cache\" + this.mID + ".log");
                }
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + @"cache\" + this.mID + ".log", logmsg);
            }
            PublicLib.Stop(C_id);
            PublicLib.inPorcess = false;
            this.timer1.Stop();
        }
        /// <summary>
        /// 获取酷我实际地址并下载
        /// </summary>
        public void KuWoPR()
        {
            string REUrl = string.Empty;
            REUrl = plb.pub_doGET(string.Format("http://antiserver.kuwo.cn/anti.s?type=convert%5Furl&format=aac%7Cmp3&rid=MUSIC%5F{0}&response=url", mID), Encoding.UTF8);
            download(C_id, REUrl, mID);
        }
        /// <summary>
        /// 获取酷狗实际地址并下载
        /// </summary>
        public void KuGoPR(string url)
        {
            download(C_id, url, mID);
        }
        public void NetEasy_oldapi(string REUrl)
        {
            try
            {
                if (REUrl == "null")
                {
                    closethis("解析下载地址失败，可能是版权问题");
                    return;
                }
                download(C_id, REUrl, mID);
            }
            catch (Exception)
            {
                closethis("解析下载地址失败，可能是版权问题");
            }
        }
        /// <summary>
        /// 第三方获取5Sing实际地址
        /// </summary>
        public void VSing()
        {
            string REUrl = string.Empty;
            REUrl = plb.pub_doGET(string.Format("http://5sing.uyy.so/fanchang/{0}.html", mID), Encoding.UTF8);
            Regex rel = new Regex("http://([^<>\"']*).mp3");
            REUrl = rel.Match(REUrl).Value;
            download(C_id, REUrl, mID);
        }
        /// <summary>
        /// 云服务器下载
        /// </summary>
        private void download(long id, string URLAddress, string musicID)
        {
            if (IsDelegate)
            {
                URLAddress = CloudAddress + URLAddress;
            }
            download(URLAddress, musicID);
        }
        /// <summary>
        /// 普通下载
        /// </summary>
        /// <param name="URLAddress"></param>
        /// <param name="musicID"></param>
        private void download(string URLAddress, string musicID)
        {
            if (musicID == string.Empty || musicID == "")
            {
                closethis("Sorry,no MusicID that can't download the music in your storge!");
            }
            if (URLAddress == "" || URLAddress == string.Empty)
            {
                closethis(musicname + ":无效的下载地址");
            }
            else
            {

                string fileName = AppDomain.CurrentDomain.BaseDirectory + @"cache\" + musicID + ".ms";//客户端保存的文件名
                //client.DownloadFile(URLAddress, fileName);
                float percent = 0;

                try
                {
                    HttpWebRequest Myrq = (HttpWebRequest)WebRequest.Create(URLAddress);
                    HttpWebResponse myrp = (HttpWebResponse)Myrq.GetResponse();
                    long totalBytes = myrp.ContentLength;
                    Stream st = myrp.GetResponseStream();
                    Stream so = new FileStream(fileName, FileMode.Create);
                    long totalDownloadedByte = 0;
                    byte[] by = new byte[1024];
                    int osize = st.Read(by, 0, (int)by.Length);
                    while (osize > 0)
                    {
                        totalDownloadedByte = osize + totalDownloadedByte;
                        so.Write(by, 0, osize);
                        osize = st.Read(by, 0, (int)by.Length);
                        percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                    }
                    so.Close();
                    st.Close();

                    FileStream fw = File.Open(fileName + ".ok", FileMode.OpenOrCreate);
                    fw.Seek(0, SeekOrigin.Begin);
                    fw.SetLength(0);
                    fw.Close();
                    FileStream afw = File.Open(fileName + ".ok", FileMode.OpenOrCreate);
                    StreamWriter sw = new StreamWriter(afw, Encoding.UTF8);
                    sw.Write("ok");
                    sw.Flush();
                    sw.Close();
                    closethis();
                }
                catch (Exception)
                {

                }

            }
        }
    }
}
