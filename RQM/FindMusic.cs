﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Web;

namespace RQM
{
    public class FindMusic
    {
        public MatchIDs DefaultRes = new MatchIDs { name = "未找到", mid = "0", custom_link = "null", mode = "null", artis = "" };
        public bool[] Switchs = new bool[] { true, true, true, true, true };//163,kuwo,xiami,5sing
        private PublicLib pl = new PublicLib();
        public FindMusic(bool neteasy, bool kuwo, bool kugo, bool Vsing)
        {
            Switchs[0] = neteasy;
            Switchs[1] = kuwo;
            Switchs[2] = kugo;
            Switchs[3] = Vsing;

        }
        #region 同步搜库技术
        delegate MatchIDs[] SearchDLG(string musicname);
        public MatchIDs SyncSearch(string musicname)
        {
            //支持精准点歌
            List<MatchIDs> MutiRestul = new List<MatchIDs>();
            MatchIDs restul = DefaultRes;
            try
            {
                string mohu_musicname = musicname;
                //歌手名过滤
                string[] sp1 = musicname.Split(' ');
                string[] sp2 = musicname.Split('-');
                if (sp1.Length > 1)
                    mohu_musicname = sp1[0];
                if (sp2.Length > 1)
                    mohu_musicname = sp2[0];
                //每个引擎都有一个开关
                if (Switchs[0])
                {
                    MutiRestul.Add(NetEasySearch(musicname));
                }
                if (Switchs[1])
                {
                    MutiRestul.AddRange(KuwooSearch(musicname));
                }
                if (Switchs[2])
                {
                    MutiRestul.AddRange(XiaMiSearch(musicname));
                }
                if (Switchs[3])
                {
                    MutiRestul.AddRange(VsingSearch(musicname));
                }
                if (Switchs[4])
                {
                    MutiRestul.AddRange(KugoSearch(musicname));
                }
                foreach (MatchIDs item in MutiRestul)
                {
                    //System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "mse.log", item.name+":"+item.mode+"\n");
                    Regex acc = new Regex("(" + musicname + "*)", RegexOptions.IgnoreCase);
                    Regex mohu = new Regex("(" + mohu_musicname + "*)", RegexOptions.IgnoreCase);
                    Regex ForID = new Regex("^[0-9]\\d*");
                    if (acc.IsMatch(item.name))
                    {
                        restul = item;
                        break;
                    }
                    else if (mohu.IsMatch(item.name))
                    {
                        restul = item;
                        break;
                    }
                    else if (ForID.IsMatch(musicname))
                    {
                        restul = item;
                        break;
                    }
                }
            }
            catch (Exception)
            {

            }
            return restul;
        }
        public MatchIDs VSingSearchOnly(string musicname)
        {
            //不支持精准点歌
            List<MatchIDs> MutiRestul = new List<MatchIDs>();
            MatchIDs restul = DefaultRes;
            try
            {
                MutiRestul.AddRange(VsingSearch(musicname));
                if (MutiRestul.Count > 0)
                {
                    restul = MutiRestul[0];
                }
            }
            catch (Exception)
            {

            }
            return restul;
        }
        #endregion

        #region 搜索引擎
        public MatchIDs NetEasySearch(string musicname)
        {
            MatchIDs restul = DefaultRes;
            if (!Switchs[0])
            {
                return restul;
            }
            Regex ForID = new Regex("^[0-9]\\d*");
            NetEasyOldApi neo = new NetEasyOldApi();
            if (ForID.IsMatch(musicname))
            {
                restul.mid = musicname.Trim();
                NEResult ner = neo.GetDTLFromID(musicname);
                restul.name = ner.name;
                restul.custom_link = ner.custom_link;
                restul.artis = ner.artist;
            }
            else
            {
                NEResult ner = neo.GetDTLFromName(musicname);
                restul.mid = ner.mid;
                restul.name = ner.name;
                restul.custom_link = ner.custom_link;
                restul.artis = ner.artist;
            }
            restul.mode = "163music";
            return restul;
        }

        public MatchIDs[] XiaMiSearch(string musicname)
        {
            string html = pl.pub_doGET(string.Format("http://www.xiami.com/search?key={0}&pos=1", musicname), System.Text.Encoding.UTF8);
            List<MatchIDs> IDs = new List<MatchIDs>();
            IDs.Add(DefaultRes);
            if (html != "")
            {
                try
                {
                    Regex htmlp = new Regex("href=\"http://www.xiami.com/song/([^<>\"'/]*)\" title=\"([^<>\"']*)\"", RegexOptions.Multiline);
                    MatchCollection hmmp = htmlp.Matches(html);
                    if (hmmp.Count > 0)
                    {
                        IDs.Clear();
                    }
                    foreach (Match item in hmmp)
                    {
                        MatchIDs tmp = new MatchIDs();
                        string rid = item.Groups[1].Value.Substring(0, 10);
                        tmp.mid = rid;
                        tmp.name = item.Groups[2].Value.Replace("&nbsp;", " ");
                        tmp.custom_link = string.Format("http://www.xiami.com/widget/0_{0},_235_346_FF8719_494949_1/multiPlayer.swf", tmp.mid);
                        tmp.mode = "xiamimusic";
                        //json 示例：
                        //TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                        //string timestamp = Convert.ToInt64(ts.TotalSeconds).ToString();
                        //string.Format("http://www.xiami.com/song/playlist/id/{0}/object_name/default/object_id/0/cat/json?_ksTS={1}&callback=jsonp", tmp.mid, timestamp);
                        IDs.Add(tmp);
                    }
                }
                catch (Exception)
                {
                    return IDs.ToArray();
                }
            }
            return IDs.ToArray();
        }

        public MatchIDs[] KuwooSearch(string musicname)
        {
            string html = pl.pub_doGET("http://sou.kuwo.cn/ws/NSearch?type=all&key=" + musicname, System.Text.Encoding.UTF8);
            List<MatchIDs> IDs = new List<MatchIDs>();
            IDs.Add(DefaultRes);
            if (html != "")
            {
                try
                {
                    Regex htmlp = new Regex("href=\"http://www.kuwo.cn/yinyue/([^<>\"']*)\" title=\"([^<>\"']*)\"", RegexOptions.Multiline);
                    Regex artistp = new Regex("href=\"http://www.kuwo.cn/mingxing/([^<>\"']*)\" target=\"_blank\" title=\"([^<>\"']*)\"", RegexOptions.Multiline);
                    MatchCollection hmmp = htmlp.Matches(html);
                    if (hmmp.Count > 0)
                    {
                        IDs.Clear();
                    }
                    foreach (Match item in hmmp)
                    {
                        MatchIDs tmp = new MatchIDs();
                        string rid = item.Groups[1].Value;
                        tmp.mid = rid.TrimEnd('/');
                        tmp.name = item.Groups[2].Value.Replace("&nbsp;", " ");
                        tmp.artis = artistp.Match(html).Groups[2].Value.Replace("&nbsp;", " ");
                        tmp.custom_link = string.Format("http://www.kuwo.cn/yinyue/{0}", tmp.mid);
                        tmp.mode = "kuwoomusic";
                        IDs.Add(tmp);
                    }
                    string link = string.Format("http://www.kuwo.cn/yinyue/{0}/", IDs.ToArray()[0]);
                    //Process.Start(link);
                }
                catch (Exception)
                {
                    return IDs.ToArray();
                }
            }
            return IDs.ToArray();
        }

        public MatchIDs[] KugoSearch(string musicname)
        {
            string keyword = HttpUtility.UrlEncode(musicname, System.Text.Encoding.UTF8).Replace("+", "%20");
            string json = pl.pub_doGET("http://mobilecdn.kugou.com/api/v3/search/song?format=json&keyword=" + keyword + "page=1&pagesize=20&showtype=1", System.Text.Encoding.UTF8);
            JObject jObject = JObject.Parse(json);
            JToken data = jObject["data"]["info"];
            List<MatchIDs> IDs = new List<MatchIDs>();
            if (data.HasValues)
            {
                JArray songs = data.ToObject<JArray>();
                foreach (var item in songs)
                {
                    MatchIDs tmp = new MatchIDs();
                    string rid = item["hash"].ToString();
                    if (item["price_320"].ToString() == "0")
                    {
                        rid = item["320hash"].ToString();
                    }

                    tmp.mid = rid.Trim();
                    tmp.name = item["songname_original"].ToString();
                    tmp.artis = item["singername"].ToString();
                    try
                    {
                        string json2 = pl.pub_doGET("http://m.kugou.com/app/i/getSongInfo.php?cmd=playInfo&hash=" + rid.Trim(), System.Text.Encoding.UTF8);
                        JObject jObject2 = JObject.Parse(json2);
                        tmp.custom_link = jObject2["url"].ToString();
                    }
                    catch (Exception)
                    {

                    }
                    tmp.mode = "kugomusic";
                    IDs.Add(tmp);
                }
            }
            else
            {
                IDs.Add(DefaultRes);
            }
            return IDs.ToArray();
        }
        public MatchIDs[] VsingSearch(string musicname)
        {
            string html = pl.pub_doGET("http://search.5sing.kugou.com/?keyword=" + musicname, System.Text.Encoding.UTF8);
            List<MatchIDs> IDs = new List<MatchIDs>();
            IDs.Add(DefaultRes);
            if (html != "")
            {
                try
                {
                    Regex htmlp = new Regex("var.dataList.=.'([^']*)'");
                    Match hmmp = htmlp.Match(html);
                    string json = hmmp.Groups[1].Value;
                    //开始对json进行过滤
                    Regex json_ff = new Regex(@"\\(?!u)");//删除除了“\u”之外的所有“\”
                    json = json_ff.Replace(json, "");
                    Regex tag = new Regex("<em class=\"([^\"]*)\">([^<>]*)</em>");
                    json = tag.Replace(json, "tag_name");
                    //结束过滤
                    JArray ja = (JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                    IDs.Clear();
                    MatchIDs tmp = new MatchIDs();
                    tmp.name = ja[0]["originalName"].ToString();
                    tmp.mid = ja[0]["songId"].ToString();
                    tmp.custom_link = ja[0]["songurl"].ToString();
                    tmp.mode = "5sing";
                    IDs.Add(tmp);
                    //Process.Start(link);
                }
                catch (Exception)
                {
                    return IDs.ToArray();
                }
            }
            return IDs.ToArray();
        }
        #endregion

    }
    public struct MatchIDs
    {
        public string mid;
        public string name;
        public string artis;//歌手
        public string custom_link;//有些引擎在解析之后会返回一个自定义的连接
        public string mode;
    }
}
