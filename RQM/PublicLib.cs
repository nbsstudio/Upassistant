﻿using Netios;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace RQM
{
    public class PublicLib : NetHTTP
    {
        #region 公共广播事件
        public static bool inPorcess = false;
        public delegate void EndDownLoad(long taskid);
        public static event EndDownLoad EndDownLoadEvent;
        /// <summary>
        /// 结束下载时转发事件到事件广播
        /// </summary>
        public static void Stop(long _taskid)
        {
            EndDownLoadEvent(_taskid);
        }
        #endregion
    }
    /// <summary>
    /// 公共：网易云音乐搜索结果
    /// </summary>
    public struct NEResult
    {
        public string mid;
        public string name;
        public string custom_link;
        public string artist;
    }
}
