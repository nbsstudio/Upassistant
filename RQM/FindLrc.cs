﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace RQM
{
    public class FindLrc
    {
        public void FindAndDownload(string musicname, string mid)
        {
            PublicLib pl = new PublicLib();
            string html = pl.pub_doGET(string.Format("http://music.baidu.com/search/lrc?key={0}", musicname), Encoding.UTF8);
            Regex dlb = new Regex(@"\{ 'href':'([^\{\}<>']*)' \}");
            Match vsb = dlb.Match(html);
            string link = vsb.Groups[1].Value;
            link = "http://music.baidu.com/" + link;
            //下载
            try
            {
                System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(link);
                System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
                long totalBytes = myrp.ContentLength;
                System.IO.Stream st = myrp.GetResponseStream();
                System.IO.Stream so = new System.IO.FileStream(AppDomain.CurrentDomain.BaseDirectory + "cache\\" + mid + ".lrc", System.IO.FileMode.Create);
                byte[] by = new byte[1024];
                int osize = st.Read(by, 0, (int)by.Length);
                while (osize > 0)
                {
                    so.Write(by, 0, osize);
                    osize = st.Read(by, 0, (int)by.Length);
                    //percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                }
                so.Close();
                st.Close();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// 搜网易云音乐歌词
        /// </summary>
        /// <param name="musicname"></param>
        /// <param name="artis"></param>
        /// <param name="mid"></param>
        /// <param name="outmode">歌词输出模式</param>
        public void FindAndDownload(string musicname, string artis, string mid,int outmode)
        {
            string title = musicname.Substring(0, musicname.LastIndexOf("feat.") == -1 ? musicname.Length - 1 : musicname.LastIndexOf("feat.")).TrimEnd(new char[] { '(', '[' });
            string artist = string.IsNullOrEmpty(artis) ? null : artis.Substring(0, artis.LastIndexOf("feat.") == -1 ? artis.Length - 1 : artis.LastIndexOf("feat.")).TrimEnd(new char[] { '(', '[' });
            string s = string.IsNullOrEmpty(artist) ? title : title + "-" + artist;
            //string searchURL = "http://music.163.com/api/search/get/web?csrf_token=";备用
            string searchURL = "http://music.163.com/api/search/get/";
            string postData = "hlpretag=<span class=\"s-fc7\">&hlposttag=</span>&s=" + System.Net.WebUtility.UrlEncode(s) + "&type=1&offset=0&total=true&limit=5";

            try
            {
                byte[] data = Encoding.UTF8.GetBytes(postData);
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(searchURL);
                myRequest.Method = "POST";
                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = data.Length;
                myRequest.Host = "music.163.com";
                myRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36";
                myRequest.Referer = "http://music.163.com/search/";
                Stream newStream = myRequest.GetRequestStream();
                newStream.Write(data, 0, data.Length);
                newStream.Close();
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                string result = reader.ReadToEnd();
                bool status = (int)myResponse.StatusCode == 200;
                reader.Close();
                MusicJson musicJson = Newtonsoft.Json.JsonConvert.DeserializeObject<MusicJson>(result);
                if (status)
                {
                    if (musicJson.code != 200 || musicJson.result.songCount == null)
                        FindAndDownload(musicname, mid);

                    var song = musicJson.result.songs;
                    int[] outInt = new int[] { 0, 0 };
                    int b = 0;
                    int c = 0;
                    foreach (var k in song)
                    {
                        var ncm_name = k.name;
                        foreach (var a_k in k.artists)
                        {
                            string ncm_artist = a_k.name;
                            int p0 = Compare(title, ncm_name);
                            int p1 = Compare(artist, ncm_artist);
                            if (p0 == 100 && p1 == 100)
                            {
                                b = song.FindIndex(m => m == k);
                                c = k.artists.FindIndex(m => m == a_k);
                                outInt[0] = p0;
                                outInt[1] = p1;
                                break;
                            }
                            if (p0 > outInt[0])
                            {
                                b = song.FindIndex(m => m == k);
                                c = k.artists.FindIndex(m => m == a_k);
                                outInt[0] = p0;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(artist) && (p0 == outInt[0] && p1 > outInt[1]))
                                {
                                    b = song.FindIndex(m => m == k);
                                    c = k.artists.FindIndex(m => m == a_k);
                                    outInt[1] = p1;
                                }
                            }
                        }
                    }
                    //获取歌词
                    var res_id = song[b].id;
                    var res_name = song[b].name;
                    var res_artist = song[b].artists[c].name;
                    string lyricURL = "http://music.163.com/api/song/lyric?os=pc&id=" + res_id + "&lv=-1&kv=-1&tv=-1";
                    HttpWebRequest lrcRequest = (HttpWebRequest)WebRequest.Create(lyricURL);
                    lrcRequest.Method = "GET";
                    lrcRequest.Referer = "http://music.163.com/";
                    lrcRequest.Headers.Add(HttpRequestHeader.Cookie, "appver=1.5.0.75771");
                    HttpWebResponse lrcResponse = (HttpWebResponse)lrcRequest.GetResponse();
                    StreamReader lrcReader = new StreamReader(lrcResponse.GetResponseStream(), Encoding.UTF8);
                    string lrcResult = lrcReader.ReadToEnd();
                    lrcReader.Close();
                    if ((int)lrcResponse.StatusCode == 200)
                    {
                        LrcClass lrcJson = Newtonsoft.Json.JsonConvert.DeserializeObject<LrcClass>(lrcResult);
                        string tlyric = "";
                        string lrc = "";
                        if (lrcJson.lrc == null)
                            FindAndDownload(musicname, mid);

                        if (lrcJson.tlyric != null && !string.IsNullOrEmpty(lrcJson.tlyric.lyric))
                        {
                            string pattern = "〔|〕|〈|〉|《|》|「|」|『|』|〖|〗|【|】|{|}|/";
                            Regex regex = new Regex(pattern);
                            tlyric = regex.Replace(lrcJson.tlyric.lyric, "");
                        }

                        StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "cache\\" + mid + ".lrc", false, Encoding.UTF8);
                        if (lrcJson.lrc != null && !string.IsNullOrEmpty(lrcJson.lrc.lyric))
                        {
                            lrc = lrcJson.lrc.lyric;
                        }
                        else
                        {
                            lrc = "[00:01.00]未找到歌词，可能该歌曲是纯音乐";
                        }
                        switch (outmode)
                        {
                            case 1://原歌词优先
                                {
                                    sw.Write(lrc);
                                    break;
                                }
                            case 2://翻译优先
                                {
                                    if (!string.IsNullOrEmpty(tlyric))
                                        sw.Write(tlyric);
                                    else
                                        sw.Write(lrc);
                                    break;
                                }
                            case 3://结合版
                                {
                                    if (!string.IsNullOrEmpty(tlyric))
                                        sw.Write(LrcMerge(lrc, tlyric));
                                    else
                                        sw.Write(lrc);
                                    break;
                                }
                        }
                        sw.Flush();
                        sw.Close();
                    }
                }
            }
            catch (Exception)
            {
                FindAndDownload(musicname, mid);
            }

        }

        private int Compare(string x, string y)
        {
            List<char> X = new List<char>();
            List<char> Y = new List<char>();
            for (int i = 0; i < x.Length; i++)
            {
                X.Add(x[i]);
            }
            for (int i = 0; i < y.Length; i++)
            {
                X.Add(y[i]);
            }
            decimal z = 0;
            decimal s = X.Count + Y.Count;
            X.Sort();
            Y.Sort();
            while (X.Count != 0 && Y.Count != 0)
            {
                if (X[0] == Y[0])
                {
                    z++;
                    X.RemoveAt(0);
                    Y.RemoveAt(0);
                }
                else if (X[0] < Y[0])
                {
                    X.RemoveAt(0);
                }
                else if (X[0] > Y[0])
                {
                    Y.RemoveAt(0);
                }
            }

            return (int)(z / s * 200);
        }

        private string LrcMerge(string olrc, string tlrc)
        {
            string[] bracket = new string[] { "「", "」" };
            string[] Olrc = olrc.Split('\n');
            string[] Tlrc = tlrc.Split('\n');
            var o_f = Olrc[0].IndexOf("[by:");
            if (o_f == 0)
            {
                var o_b = Olrc[0].IndexOf("]");
                var o = (o_f != -1 && o_b != -1) ? Olrc[0].Substring(4, o_b - 4) : "";

                var t_f = Tlrc[0].IndexOf("[by:");
                var t_b = Tlrc[0].IndexOf("]");
                var t = (t_f != -1 && t_b != -1) ? Olrc[0].Substring(4, o_b - 4) : "";
                Olrc[0] = "[by:" + o + "/译:" + t + "]";
            }
            var set = Olrc[5].IndexOf("]");
            set = (set == -1) ? 9 : set;
            var i = 0;
            var l = Tlrc.Length;
            var lrc = new System.Collections.ArrayList();
            foreach (var k in Olrc)
            {
                lrc.Add("");
                int K = Array.IndexOf(Olrc, k);
                string a = "";
                try
                {
                    a = k.Substring(1, set - 1);
                }
                catch (Exception)
                {
                    continue;
                }
                //var a = k.Substring(1, set - 1);
                while (i < l)
                {
                    var j = 0;
                    var tf = 0;
                    while (j < 5)
                    {
                        if (i + j >= l) break;
                        var b = Tlrc[i + j].Substring(1, set - 1);
                        if (a == b)
                        {
                            tf = 1;
                            i += j;
                            break;
                        }
                        j++;
                    }
                    if (tf == 0)
                    {
                        lrc[K] = Olrc[K];
                        break;
                    }
                    var c = Tlrc[i].Substring(set + 1);
                    if (!string.IsNullOrEmpty(c))
                    {
                        lrc[K] = Olrc[K] + bracket[0] + Tlrc[i].Substring(set + 1) + bracket[1];
                        i++;
                        break;
                    }
                    else
                    {
                        lrc[K] = Olrc[K];
                        break;
                    }
                }
            }
            return string.Join('\n'.ToString(), lrc.ToArray());
        }
    }
}
