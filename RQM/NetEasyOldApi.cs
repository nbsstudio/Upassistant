﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace RQM
{
    public class NetEasyOldApi
    {
        public NEResult GetDTLFromName(string musicname)
        {
            NEResult res = new NEResult();
            try
            {
                PublicLib pl = new PublicLib();
                string json = pl.pub_doResponse(new Netios.postItem { GOP = "POST", postData = string.Format("s={0}&offset=0&limit=1&type=1", musicname), postUrl = "http://music.163.com/api/search/pc", Refer = "http://music.163.com" }, Encoding.UTF8);
                //Regex _songs = new Regex("\"name\":\"([^\\\"]*)\",\"id\":([0-9]\\d*)");
                //Regex _songs = new Regex("\"duration\":([0-9]\\d*),\"name\":\"([^\\\"]*)\",\"id\":([0-9]\\d*)");
                Newtonsoft.Json.Linq.JObject jo1 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                string json2 = jo1["result"].ToString();
                Newtonsoft.Json.Linq.JObject jo2 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(json2);
                string json3 = jo2["songs"][0].ToString();
                Newtonsoft.Json.Linq.JObject jo3 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(json3);
                string mid = jo3["id"].ToString();
                //string mid = _songs.Match(json).Groups[3].Value;
                //System.IO.File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "mid.log", mid);
                res = GetDTLFromID(mid);
                res.mid = mid;
            }
            catch (Exception)
            {
                res.name = "未找到";
                res.custom_link = "null";
            }
            return res;
        }
        public NEResult GetDTLFromID(string id)
        {
            NEResult res = new NEResult();
            try
            {
                PublicLib pl = new PublicLib();
                string json = pl.pub_doGET(string.Format("http://music.163.com/api/song/detail?ids=[{0}]", id), Encoding.UTF8);
                Newtonsoft.Json.Linq.JObject jo1 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                Newtonsoft.Json.Linq.JObject jo2 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(jo1["songs"][0].ToString());
                Newtonsoft.Json.Linq.JObject jo3 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(jo2["artists"][0].ToString());
                //新下载API
                string json2 = pl.pub_doGET(string.Format("http://music.163.com/api/song/enhance/download/url?br=128000&id={0}", id), Encoding.UTF8);
                Newtonsoft.Json.Linq.JObject jo4 = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(json2);
                res.custom_link = jo4["data"]["url"].ToString();
                //res.custom_link = "http://p2"+jo2["mp3Url"].ToString().Remove(0,9);
                res.name = jo2["name"].ToString();
                res.artist = jo3["name"].ToString();
            }
            catch (Exception)
            {
                res.name = "未找到";
                res.custom_link = "null";
            }
            return res;
        }
    }
    
}
