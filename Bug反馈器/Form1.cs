﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.IO;
using Netios;

namespace Bug反馈器
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private const string COMMENT = "请输入程序崩溃前你的操作或当时的状态...";
        private void Form1_Load(object sender, EventArgs e)
        {
            categoryComboBox.SelectedIndex = 0;
            commentBox.Text = COMMENT;
            uploadLogFileCheckBox1.Checked = true;
            
        }

        private void commentBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (commentBox.Text== COMMENT)
            {
                commentBox.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            button1.Text = "正在发送...";
            Thread th = new Thread(Submit);
            th.Start();
        }

        private void Submit()
        {
            try
            {
                NetHTTP http = new NetHTTP();
                string cat = "";
                string comm = "";
                Invoke(new Action(() => {
                    cat = categoryComboBox.Text;
                    comm = "comment:" + commentBox.Text;
                }));
                string logContent = "log:null";
                if (uploadLogFileCheckBox1.Checked)
                {
                    try
                    {
                        logContent = "log:"+File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory+"错误.log");
                        if (logContent.Length>1500)
                        {
                            logContent = logContent.Substring(logContent.Length-1500);//取右边1500个文字
                        }
                    }
                    catch (Exception)
                    {
                        
                    }
                }
                string parm = "type="+cat + "&content=" + comm + logContent+"&mcode=" + MachineCode.HashString(MachineCode.GetMachineCodeString());
                string result = http.pub_doPOST("http://www.nacgame.com/ucenter/softwareapi/bugreporter", parm, Encoding.UTF8);
                //string result = http.pub_doPOST("http://localhost:8080/website2/ucenter/softwareapi/bugreporter", parm, Encoding.UTF8);
                if (result.Equals("200.ok"))
                {
                    Invoke(new Action(() => {
                        button1.Text = "发送成功...";
                    }));
                }
                else
                {
                    Invoke(new Action(() => {
                        button1.Text = "发送失败...";
                    }));
                }
               
            }
            catch (Exception e)
            {
                Invoke(new Action(()=> {
                    button1.Text="网络错误...";
                }));
            }
            finally
            {
                Invoke(new Action(() => {
                    button1.Enabled = true;
                }));
                
            }
        }
    }
}
