﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace Bug反馈器
{
    static class Program
    {
        private static bool isCanCreateNew;
        private static bool MainCall = false;

        public static bool mainCall
        {
            get { return MainCall; }
        }

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //多开检查
            Mutex mutex = new Mutex(true, "BUGReporter", out isCanCreateNew);
            if (!isCanCreateNew)
            {
                MessageBox.Show("反馈鸡不允许多开哦","肾透支");
                Environment.Exit(0);
            }
            //参数检查
            List<string> argList = new List<string>(args);
            int point = -1;
            point = argList.FindIndex((s) => s.Equals("-t"));
            if (point>-1 && argList.FindIndex((s)=>s.Equals("nacgame")) == point+1)
            {
                MainCall = true;
            }
            else
            {
                MessageBox.Show("如需主动提交BUG，您也可以直接登陆软件官网反馈。\n网址：http://software.nacgame.com","提交", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
